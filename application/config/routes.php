<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'general';

$route['faq'] = 'general/faq';
$route['privacy'] = 'general/privacy';

$route['home'] = 'general/dashboard';

$route['campaign'] = 'campaigns/index';
$route['campaign/create'] = 'campaigns/create_campaign';
$route['campaign/detail/(:any)'] = 'campaigns/campaign_detail';

$route['signin'] = 'user/signin';
$route['signup'] = 'user/signup';
$route['signout'] = 'user/signout';
$route['profil'] = 'user/profil';
$route['notification'] = 'user/notification';
$route['message'] = 'user/message';
$route['profil/campaign'] = 'user/campaign';
$route['profil/event'] = 'user/events';
$route['profil/password'] = 'user/password';
$route['profil/edit'] = 'user/editprofil';

$route['events'] = 'events/index';
$route['events/create'] = 'events/create';
$route['events/detail/(:any)'] = 'events/detail_events';

$route['pendonor'] = 'donor/pendonor';
$route['pendonor/detail'] = 'donor/detail';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;