<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sipatuo - Urgent</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
	<?php
		$multiple_css = array('all.css','bootstrap.min.css','offcanvas.css');
		echo assets_css($multiple_css);
	?>
</head>
<body class="bg-light">
	<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <a style="font-size:1.55em;" class="navbar-brand ml-lg-3 mr-lg-3" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/Guard-Red.png" class="sip-logo"> <span class="">SIPATUO</span></a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
        <!-- <i style="color:#f2f2f2;margin-right:7px;" class="fas fa-ellipsis-v"></i> -->
      </button>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>"><i class="fas fa-smile-beam"></i> <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>campaign">Campaign</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>pendonor">Pendonor</a>
          </li>
          <li class="nav-item active mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>events">Kegiatan</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>signup"><i class="fas fa-user-plus"></i></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a alt="Login" title="Login" class="nav-link" href="<?php echo base_url(); ?>signin"><i class="fas fa-door-open"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="container-fluid">
      <div style="min-height:600px;" class="row justify-content-center bg-white">
        <div class="col-lg-9">
          <div class="row justify-content-center pt-4">
            <div style="text-align: center;" class="col-lg-12 mt-5 mb-4 d-none d-md-block d-lg-block">
              <h1 style="font-size:2.5em;color:black;" class="display-4"><strong>#kegiatan</strong>Donor</h1>
            </div>
            <div style="text-align: center;" class="col-lg-12 pb-5 d-none d-md-block d-lg-block">
              <button style="border-radius:0px;border:solid 1px #dddddd;box-shadow: 3px 3px 2px #dddddd;" type="button" class="btn btn-outline-secondary mr-2"><i class="fas fa-search"></i></button>
              <a href="<?php echo base_url(); ?>events/create" style="border-radius:0px;border:solid 1px #dddddd;box-shadow: 3px 3px 2px #dddddd;" class="btn btn-outline-secondary"><i class="fas fa-plus-circle"></i></a>
            </div>
            <div class="col-lg-12 d-lg-none d-md-none">
              <div class="float-right"><a style="font-size:1.15em;" href="#" class="text-success mr-1"><i class="fas fa-plus-circle"></i></a> <a style="font-size:1.15em;" href="#" class="text-dark"><i class="fas fa-search"></i></a></div>
              <h6 style="" class="border-bottom border-gray pb-3 mb-0">Daftar Kegiatan Donor</h6>
            </div>
            <div class="col-lg-4 col-md-4 mb-lg-5">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img style="border:solid 1px #efefef" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-2.jpg" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark"><a href="<?php echo base_url(); ?>events/detail/ev123nt">Tema : Card title that wraps to a new line latihan melakukan</a></strong>
                  <span style="color:#ff9e9e" class="d-block mb-2">Penyelenggara : BPJS Kesehatan</span>
                  This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Alamat : Jalan Perintis Kemerdekaan 10, Rumah sakit Universitas Hasanuddin (Tampilkan dalam maps)</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu : 24 Juli 2018</span>
                </p>
              </div>
              <div style="box-shadow: 3px 4px 9px #515151;" class="card d-none d-md-block d-lg-block">
                <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/Donor-Darah-2.jpg" alt="Card image cap">
                <div class="card-body">
                  <h5 style="font-size:1.25em;font-weight: 400;" class="card-title display-4"><a href="<?php echo base_url(); ?>events/detail/ev123nt">Card title that wraps to a new line</a></h5>
                  <p style="font-size:0.9em;" class="card-text lead mb-3">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <span class="btn btn-outline-dark btn-sm urgent-info mb-2"><i class="fas fa-map-marker-alt mr-1"></i> Jawa Barat</span>
                  <span class="btn btn-outline-dark btn-sm urgent-info mb-2"><i class="far fa-clock mr-1"></i> 01 Okt 2018</span>
                  <p style="font-size:0.9em;" class="card-text lead"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-lg-5">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img style="border:solid 1px #efefef" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-5.png" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Tema : Card title that wraps to a new line latihan melakukan</strong>
                  <span style="color:#ff9e9e" class="d-block mb-2">Penyelenggara : BPJS Kesehatan</span>
                  -
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Alamat : Jalan Perintis Kemerdekaan 10, Rumah sakit Universitas Hasanuddin (Tampilkan dalam maps)</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu : 24 Juli 2018</span>
                </p>
              </div>
              <div style="box-shadow: 3px 4px 9px #515151;" class="card d-none d-md-block d-lg-block">
                <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/Donor-Darah-2.jpg" alt="Card image cap">
                <div class="card-body">
                  <h5 style="font-size:1.25em;font-weight: 400;" class="card-title display-4"><a href="<?php echo base_url(); ?>events/detail/ev123nt">Card title that wraps to a new line</a></h5>
                  <p style="font-size:0.9em;" class="card-text lead mb-3">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <span class="btn btn-outline-dark btn-sm urgent-info mb-2"><i class="fas fa-map-marker-alt mr-1"></i> Jawa Barat</span>
                  <span class="btn btn-outline-dark btn-sm urgent-info mb-2"><i class="far fa-clock mr-1"></i> 01 Okt 2018</span>
                  <p style="font-size:0.9em;" class="card-text lead"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-lg-5">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img style="border:solid 1px #efefef" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-2.jpg" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Tema : Card title that wraps to a new line latihan melakukan</strong>
                  <span style="color:#ff9e9e" class="d-block mb-2">Penyelenggara : BPJS Kesehatan</span>
                  This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Alamat : Jalan Perintis Kemerdekaan 10, Rumah sakit Universitas Hasanuddin (Tampilkan dalam maps)</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu : 24 Juli 2018</span>
                </p>
              </div>
              <div style="box-shadow: 3px 4px 9px #515151;" class="card d-none d-md-block d-lg-block">
                <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/Donor-Darah-2.jpg" alt="Card image cap">
                <div class="card-body">
                  <h5 style="font-size:1.25em;font-weight: 400;" class="card-title display-4"><a href="<?php echo base_url(); ?>events/detail/ev123nt">Card title that wraps to a new line</a></h5>
                  <p style="font-size:0.9em;" class="card-text lead mb-3">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <span class="btn btn-outline-dark btn-sm urgent-info mb-2"><i class="fas fa-map-marker-alt mr-1"></i> Jawa Barat</span>
                  <span class="btn btn-outline-dark btn-sm urgent-info mb-2"><i class="far fa-clock mr-1"></i> 01 Okt 2018</span>
                  <p style="font-size:0.9em;" class="card-text lead"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-lg-5">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img style="border:solid 1px #efefef" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-2.jpg" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Tema : Card title that wraps to a new line latihan melakukan</strong>
                  <span style="color:#ff9e9e" class="d-block mb-2">Penyelenggara : BPJS Kesehatan</span>
                  This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Alamat : Jalan Perintis Kemerdekaan 10, Rumah sakit Universitas Hasanuddin (Tampilkan dalam maps)</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu : 24 Juli 2018</span>
                </p>
              </div>
              <div style="box-shadow: 3px 4px 9px #515151;" class="card d-none d-md-block d-lg-block">
                <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/Donor-Darah-2.jpg" alt="Card image cap">
                <div class="card-body">
                  <h5 style="font-size:1.25em;font-weight: 400;" class="card-title display-4"><a href="<?php echo base_url(); ?>events/detail/ev123nt">Card title that wraps to a new line</a></h5>
                  <p style="font-size:0.9em;" class="card-text lead mb-3">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <span class="btn btn-outline-dark btn-sm urgent-info mb-2"><i class="fas fa-map-marker-alt mr-1"></i> Jawa Barat</span>
                  <span class="btn btn-outline-dark btn-sm urgent-info mb-2"><i class="far fa-clock mr-1"></i> 01 Okt 2018</span>
                  <p style="font-size:0.9em;" class="card-text lead"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-lg-5">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img style="border:solid 1px #efefef" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-1.png" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Tema : Card title that wraps to a new line latihan melakukan</strong>
                  <span style="color:#ff9e9e" class="d-block mb-2">Penyelenggara : BPJS Kesehatan</span>
                  This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Alamat : Jalan Perintis Kemerdekaan 10, Rumah sakit Universitas Hasanuddin (Tampilkan dalam maps)</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu : 24 Juli 2018</span>
                </p>
              </div>
              <div style="box-shadow: 3px 4px 9px #515151;" class="card d-none d-md-block d-lg-block">
                <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/Donor-Darah-2.jpg" alt="Card image cap">
                <div class="card-body">
                  <h5 style="font-size:1.25em;font-weight: 400;" class="card-title display-4"><a href="<?php echo base_url(); ?>events/detail/ev123nt">Card title that wraps to a new line</a></h5>
                  <p style="font-size:0.9em;" class="card-text lead mb-3">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <span class="btn btn-outline-dark btn-sm urgent-info mb-2"><i class="fas fa-map-marker-alt mr-1"></i> Jawa Barat</span>
                  <span class="btn btn-outline-dark btn-sm urgent-info mb-2"><i class="far fa-clock mr-1"></i> 01 Okt 2018</span>
                  <p style="font-size:0.9em;" class="card-text lead"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-lg-5">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img style="border:solid 1px #efefef" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-5.png" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Tema : Card title that wraps to a new line latihan melakukan</strong>
                  <span style="color:#ff9e9e" class="d-block mb-2">Penyelenggara : BPJS Kesehatan</span>
                  This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Alamat : Jalan Perintis Kemerdekaan 10, Rumah sakit Universitas Hasanuddin (Tampilkan dalam maps)</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu : 24 Juli 2018</span>
                </p>
              </div>
              <div style="box-shadow: 3px 4px 9px #515151;" class="card d-none d-md-block d-lg-block">
                <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/Donor-Darah-2.jpg" alt="Card image cap">
                <div class="card-body">
                  <h5 style="font-size:1.25em;font-weight: 400;" class="card-title display-4"><a href="<?php echo base_url(); ?>events/detail/ev123nt">Card title that wraps to a new line</a></h5>
                  <p style="font-size:0.9em;" class="card-text lead mb-3">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <span class="btn btn-outline-dark btn-sm urgent-info mb-2"><i class="fas fa-map-marker-alt mr-1"></i> Jawa Barat</span>
                  <span class="btn btn-outline-dark btn-sm urgent-info mb-2"><i class="far fa-clock mr-1"></i> 01 Okt 2018</span>
                  <p style="font-size:0.9em;" class="card-text lead"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-lg-5">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img style="border:solid 1px #efefef" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-3.png" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Tema : Card title that wraps to a new line latihan melakukan</strong>
                  <span style="color:#ff9e9e" class="d-block mb-2">Penyelenggara : BPJS Kesehatan</span>
                  This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Alamat : Jalan Perintis Kemerdekaan 10, Rumah sakit Universitas Hasanuddin (Tampilkan dalam maps)</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu : 24 Juli 2018</span>
                </p>
              </div>
              <div style="box-shadow: 3px 4px 9px #515151;" class="card d-none d-md-block d-lg-block">
                <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/Donor-Darah-2.jpg" alt="Card image cap">
                <div class="card-body">
                  <h5 style="font-size:1.25em;font-weight: 400;" class="card-title display-4"><a href="<?php echo base_url(); ?>events/detail/ev123nt">Card title that wraps to a new line</a></h5>
                  <p style="font-size:0.9em;" class="card-text lead mb-3">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <span class="btn btn-outline-dark btn-sm urgent-info mb-2"><i class="fas fa-map-marker-alt mr-1"></i> Jawa Barat</span>
                  <span class="btn btn-outline-dark btn-sm urgent-info mb-2"><i class="far fa-clock mr-1"></i> 01 Okt 2018</span>
                  <p style="font-size:0.9em;" class="card-text lead"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
              </div>
            </div>
            <div style="text-align: center;" class="col-lg-12 mb-5 mt-4">
              <button style="border-radius: 20px;" type="button" class="btn btn-sm btn-danger px-4"><i style="color:white;" class="fas fa-expand mr-2"></i> Tampilkan Lainnya</button>
            </div>
          </div>
          
        </div>
      </div>
    </main>

    <footer style="margin-top:50px;padding-top: 11px;" class="text-muted">
      <div class="container">
        <img class="float-right" width="50px" src="<?php echo base_url(); ?>assets/img/Guard-Dark.png">
        <!-- <p class="float-right">
          <a href="#"><i class="fas fa-tint"></i></a>
        </p> -->
        <a href="#"><i style="font-size: 1.55em;color:#174a9b;" class="fab fa-facebook-square mr-2"></i></a>
        <a href="#"><i style="font-size: 1.55em;" class="fab fa-twitter mr-2"></i></a>
        <a href="#"><i style="font-size: 1.55em;color:red;" class="fab fa-youtube mr-2"></i></a>
        <p>Banyak hal diluar sana yang perlu di explore. Berkelilinglah, rasakan keagungan Tuhan.</p>
        <p>2018 &copy; Sipatuo Team, <i class="fas fa-heart"></i> <i class="fas fa-smile"></i></p>
      </div>
    </footer>
<?php
	$multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
	echo assets_js($multiple_js);
?>
</body>
</html>