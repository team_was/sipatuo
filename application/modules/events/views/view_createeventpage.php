<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sipatuo - Detail Urgent</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
	<?php
		$multiple_css = array('all.css','bootstrap.min.css','offcanvas.css','smart_wizard.css', 'smart_wizard_theme_dots.css');
		echo assets_css($multiple_css);
	?>
</head>
<body class="bg-light">
	<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <?php $this->load->view('common/navbar_title_view'); ?>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>"><i class="fas fa-smile-beam"></i> <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>campaign">Campaign</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>pendonor">Pendonor</a>
          </li>
          <li class="nav-item active mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>events">Kegiatan</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>signup"><i class="fas fa-user-plus"></i></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a alt="Login" title="Login" class="nav-link" href="<?php echo base_url(); ?>signin"><i class="fas fa-door-open"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="container-fluid">

      <div style="min-height:500px;" class="row">
        <div class="col-lg-8 bg-light">
          <div class="col-lg-12 pt-5">
            <p style="color:#c80d0d;text-shadow: 2px 2px 12px white;font-size: 2.33em;" class="display-4 mb-4 mt-0"><strong>#create</strong>Event</p>
          </div>
          <div class="col-lg-12 pb-5">
            <form action="<?php echo base_url() ?>events/create/save" id="myForm" role="form" data-toggle="validator" method="post" accept-charset="utf-8">
            <div class="row justify-content-center">

              <div class="col-lg-10">
                <!-- <form>
                  <div class="form-group">
                    <label class="mb-0" for="fullname">Nama Lengkap</label>
                    <input type="text" class="form-control" id="fullname" aria-describedby="nameHelp" placeholder="Masukkan nama lengkap anda">
                    <small id="nameHelp" class="form-text text-muted">Masukkan nama anda yang membuat campaign ini.</small>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label class="mb-0" for="email">Email</label>
                      <input type="email" class="form-control" id="fullname" aria-describedby="emailHelp" placeholder="Masukkan email anda">
                      <small id="emailHelp" class="form-text text-muted">Masukkan nama anda yang membuat campaign ini.</small>
                    </div>
                    <div class="form-group col-md-6">
                      <label class="mb-0" for="email">Phone</label>
                      <input type="email" class="form-control" id="fullname" aria-describedby="emailHelp" placeholder="Masukkan nomor hp anda">
                      <small id="emailHelp" class="form-text text-muted">Masukkan nama anda yang membuat campaign ini.</small>
                    </div>
                  </div>
                  <hr/>
                  <div class="form-group">
                    <label class="mb-0" for="pasienname">Nama Pasien</label>
                    <input type="text" class="form-control" id="pasienname" aria-describedby="pasienHelp" placeholder="Masukkan nama pasien">
                    <small id="pasienHelp" class="form-text text-muted">Masukkan nama pasien yang membutuhkan bantuan darah.</small>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label class="mb-0" for="inputPassword4">Goldar</label>
                      <select class="form-control">
                        <option selected>Goldar</option>
                        <option>A</option>
                        <option>B</option>
                        <option>O</option>
                        <option>AB</option>
                      </select>
                      <small id="nameHelp" class="form-text text-muted">Pilih golongan darah yang anda butuhkan.</small>
                    </div>
                    <div class="form-group col-md-6">
                      <label class="mb-0" for="bag">Kantong</label>
                      <input type="number" class="form-control" id="bag" placeholder="Jumlah Kantong">
                      <small id="nameHelp" class="form-text text-muted">Masukkan jumlah kantong darah yang anda butuhkan.</small>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="mb-0" for="desc">Deskripsi kebutuhan</label>
                    <textarea class="form-control" id="desc" rows="3"></textarea>
                  </div>
                  <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </form> -->

                <div id="smartwizard">
                  <ul style="">
                      <li><a href="#step-1">Langkah 1<br /><small>Informasi Umum</small></a></li>
                      <li><a href="#step-2">Langkah 2<br /><small>Informasi Kegiatan</small></a></li>
                      <li><a href="#step-3">Langkah 3<br /><small>Waktu Kegiatan dan Lokasi</small></a></li>
                  </ul>

            <div>
                <div id="step-1" class="">
                    <h3 style="font-weight:400;" class="border-bottom border-gray lead pb-2 pt-2">Informasi Umum</h3>
                    <div class="col-lg-12">
                        <div class="row pt-3">
                          <div class="col-lg-6">
                            <div id="form-step-0" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label class="mb-0" style="font-size:0.88em;" for="fullname">Nama Anda <strong style="color:red;">*</strong></label>
                                  <input required name="fullname" required type="text" class="form-control form-control-sm" id="fullname" placeholder="Masukkan nama lengkap anda">
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div id="form-step-1" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label class="mb-0" style="font-size:0.88em;" for="hp">Telepon <strong style="color:red;">*</strong></label>
                                  <input required name="hp" required type="text" class="form-control form-control-sm" id="hp" placeholder="Masukkan nama telepon">
                                  <!-- <small style="font-size:0.75em;" id="emailHelp" class="form-text text-muted ml-2">Masukkan nomor telepon yang akan dihubungi oleh pendonor jika ingin memberi bantuan kepada anda.</small> -->
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-12 mb-3">
                            <div id="form-step-2" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label class="mb-0" style="font-size:0.88em;" for="email">Email Address <strong style="color:red;">*</strong></label>
                                  <div class="input-group">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text"> <i class="far fa-credit-card"></i> </span>
                                      </div>
                                      <input required name="email" type="email" class="form-control form-control-sm" id="email" placeholder="Masukkan Alamat Email">
                                  </div>
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div id="step-2" class="">
                    <h3 style="font-weight:400;" class="border-bottom border-gray lead pb-2 pt-2">Informasi Kegiatan</h3>
                    <div class="col-lg-12">
                      <div class="row">
                        <div class="col-lg-12">
                          <div id="form-step-3" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label class="mb-0" style="font-size:0.88em;" for="eventtheme">Tema Kegiatan <strong style="color:red;">*</strong></label>
                                  <textarea name="eventtheme" style="font-size:0.81em;" class="form-control" id="eventtheme" rows="2"></textarea>
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-12">
                          <div id="form-step-4" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label class="mb-0" style="font-size:0.88em;" for="descevent">Deskripsi Kegiatan <strong style="color:red;">*</strong></label>
                                  <textarea name="descevent" style="font-size:0.81em;" class="form-control" id="descevent" rows="3"></textarea>
                                  <small id="descHelp" class="form-text text-muted">Jelaskan deskripsi kegiatan anda.</small>
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-12">
                          <div id="form-step-5" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label class="mb-0" style="font-size:0.88em;" for="organizedby">Organized by <strong style="color:red;">*</strong></label>
                                  <textarea name="organizedby" style="font-size:0.81em;" class="form-control" id="organizedby" rows="3"></textarea>
                                  <small id="organizedbyHelp" class="form-text text-muted">Masukkan penyelenggara kegiatan ini.</small>
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-12">
                          <div id="form-step-6" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label class="mb-0" style="font-size:0.88em;" for="supportedby">Supported by <strong style="color:red;">*</strong></label>
                                  <textarea name="supportedby" style="font-size:0.81em;" class="form-control" id="supportedby" rows="3"></textarea>
                                  <small id="supportedbyHelp" class="form-text text-muted">Masukkan pendukung-pendukung dari kegiatan anda.</small>
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-12">
                          <div id="" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label class="mb-0" style="font-size:0.88em;" for="fullname">Gambar/Poster Kegiatan <strong style="color:red;"></strong></label>
                                  <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div id="step-3" class="">
                    <h3 style="font-weight:400;" class="border-bottom border-gray lead pb-2 pt-2">Waktu Kegiatan dan Lokasi</h3>
                    <div class="col-lg-12">
                      <div class="row">
                        <div class="col-lg-6">
                          <div id="form-step-7" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label class="mb-0" style="font-size:0.88em;" for="startevent">Tanggal mulai kegiatan <strong style="color:red;">*</strong></label>
                                  <input required name="startevent" required type="number" class="form-control form-control-sm" id="startevent" placeholder="Masukkan tanggal awal">
                                  <!-- <small id="nameHelp" class="form-text text-muted">Deadline berisi durasi anda membutuhkan darah.</small> -->
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div id="form-step-8" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label class="mb-0" style="font-size:0.88em;" for="fullname">Tanggal akhir kegiatan <strong style="color:red;">*</strong></label>
                                  <input required name="endevent" required type="number" class="form-control form-control-sm" id="endevent" placeholder="Masukkan tanggal akhir">
                                  <!-- <small id="nameHelp" class="form-text text-muted">Masukkan jumlah kantong darah yang anda butuhkan.</small> -->
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-12">
                          <div id="form-step-9" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label class="mb-0" style="font-size:0.88em;" for="fullname">Waktu kegiatan <strong style="color:red;">*</strong></label>
                                  <input required name="timeevent" required type="text" class="form-control form-control-sm" id="timeevent" placeholder="Masukkan waktu kegiatan">
                                  <!-- <small id="nameHelp" class="form-text text-muted">Masukkan jumlah kantong darah yang anda butuhkan.</small> -->
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-4">
                          <div id="form-step-10" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label class="mb-0" style="font-size:0.88em;" for="negara">Negara <strong style="color:red;">*</strong></label>
                                  <input required name="negara" required type="text" class="form-control form-control-sm" id="negara" placeholder="Pilih Negara">
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-4">
                          <div id="form-step-11" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label class="mb-0" style="font-size:0.88em;" for="provinsi">Provinsi <strong style="color:red;">*</strong></label>
                                  <input required name="provinsi" required type="text" class="form-control form-control-sm" id="provinsi" placeholder="Pilih Provinsi">
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-4">
                          <div id="form-step-12" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label class="mb-0" style="font-size:0.88em;" for="kab">Kabupaten/Kota <strong style="color:red;">*</strong></label>
                                  <input required name="kab" required type="text" class="form-control form-control-sm" id="kab" placeholder="Pilih Kabupaten/Kota">
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-12">
                          <div id="form-step-13" role="form" data-toggle="validator">
                              <div class="form-group">
                                  <label class="mb-0" style="font-size:0.88em;" for="addevent">Alamat lengkap kegiatan <strong style="color:red;">*</strong></label>
                                  <textarea name="addevent" style="font-size:0.81em;" class="form-control" id="addevent" rows="3"></textarea>
                                  <small id="addeventHelp" class="form-text text-muted">Masukkan lokasi kegiatan donor darah anda dilakukan.</small>
                                  <div style="color:red;padding-top:5px;font-size:9pt;" class="help-block with-errors"></div>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            </div>
          


              </div>    
            </div>
          </form>
          </div>
        </div>
        <div class="col-lg-4 bg-secondary">
          <div style="text-align: center;" class="col-lg-12 d-none d-md-block d-lg-block">
            <!-- <p style="color:white;text-shadow: 2px 2px 12px #c80d0d;font-size: 1.55em;" class="display-4 mb-5 mt-5 pt-3"><strong>#kebutuhan</strong>Lainnya</p> -->
          </div>
          <div class="my-3 p-3 bg-secondary rounded mt-5">
            <h6 class="border-bottom border-gray pb-2 mb-0">Kegiatan donor lainnya</h6>
            <div class="media text-white pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-white">Bersama membangun bangsa yang peduli sesama.</strong>
                <span style="color:#e8e8e8;" class="d-block mb-2">Organized by : BPJS Kesehatan ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu kegiatan : 24 Juli 2018</span>
              </p>
            </div>
            <div class="media text-white pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=0ccadb&fg=0ccadb&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-white">Bakti sosial donor darah</strong>
                <span style="color:#e8e8e8;" class="d-block mb-2">Organized by : PLN ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                Donec id elit non mi porta gravida at eget metus. 
                
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Jakarta Selatan</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu kegiatan : 24 Agustus 2018</span>
              </p>
            </div>
            <div class="media text-white pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=6f42c1&fg=6f42c1&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-white">Bakti sosial donor darah</strong>
                <span style="color:#e8e8e8;" class="d-block mb-2">Organized by : PLN ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                Donec id elit non mi porta gravida at eget metus. 
                
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Jakarta Selatan</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu kegiatan : 24 Agustus 2018</span>
              </p>
            </div>
            <div class="media text-white pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=dbbb0c&fg=dbbb0c&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-white">Bakti sosial donor darah</strong>
                <span style="color:#e8e8e8;" class="d-block mb-2">Organized by : Pertamina ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                Donec id elit non mi porta gravida at eget metus. 
                
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Jakarta Selatan</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu kegiatan : 24 Agustus 2018</span>
              </p>
            </div>
            <small class="d-block text-right mt-3 mb-3">
              <a class="text-white" href="<?php echo base_url(); ?>events">Lihat Semua Kegiatan</a>
            </small>
          </div>
        </div>
      </div>
      <div class="row justify-content-center box-gabung-sipper">
        <div class="col-lg-12 mt-5 text-gabung-donor">
          <h1 style="font-size:2em;color:black;" class="display-4"><strong>Ayo </strong>Bergabung bersama kami</h1>
          <h1 style="font-size:1em;color:#153b77;" class="display-4">Gabung, temukan, minta bantuan dan bantu teman yang membutuhkan bantuan anda.</h1>
        </div>
        <div style="text-align: center;" class="col-lg-6">
          <button type="button" class="btn btn-warning btn-lg-lg">Daftar</button> <span>atau</span>
          <button type="button" class="btn btn-info btn-lg-lg">Login</button>
        </div>
      </div>
    </main>

    <?php $this->load->view('common/footer_view'); ?>
    
<?php
	$multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js','jquery.smartWizard.min.js');
	echo assets_js($multiple_js);
?>
<script type="text/javascript">
        $(document).ready(function(){

            // Step show event
            $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
               //alert("You are on step "+stepNumber+" now");
               if(stepPosition === 'first'){
                   $("#prev-btn").addClass('disabled');
               }else if(stepPosition === 'final'){
                   $("#next-btn").addClass('disabled');
               }else{
                   $("#prev-btn").removeClass('disabled');
                   $("#next-btn").removeClass('disabled');
               }

               if($('button.sw-btn-next').hasClass('disabled')){
                    $('.sw-btn-group-extra').show(); // show the button extra only in the last page
                }else{
                    $('.sw-btn-group-extra').hide();                
                }
            });

            // Toolbar extra buttons
            var btnFinish = $('<button></button>').text('Selesai')
                                             .addClass('btn btn-info')
                                             .on('click', function(){
                                                    if( !$(this).hasClass('disabled')){
                                                        var elmForm = $("#myForm");
                                                        if(elmForm){
                                                            elmForm.validator('validate');
                                                            var elmErr = elmForm.find('.has-error');
                                                            if(elmErr && elmErr.length > 0){
                                                                alert('Oops we still have error in the form');
                                                                return false;
                                                            }else{
                                                                //alert('Great! we are ready to submit form');
                                                                elmForm.submit();
                                                                return false;
                                                            }
                                                        }
                                                    }
                                                });
            var btnCancel = $('<button></button>').text('Cancel')
                                             .addClass('btn btn-danger')
                                             .on('click', function(){
                                                    $('#smartwizard').smartWizard("reset");
                                                    $('#myForm').find("input, textarea").val("");
                                                });
            // Smart Wizard
            $('#smartwizard').smartWizard({
                    selected: 0,
                    theme: 'default',
                    transitionEffect:'fade',
                    showStepURLhash: true,
                    // enableFinishButton: false,
                    // labelFinish:'Done',
                    toolbarSettings: {toolbarPosition: 'bottom',
                                      toolbarButtonPosition: 'end',
                                      toolbarExtraButtons: [btnFinish, btnCancel]
                                    },
                    anchorSettings: {
                                markDoneStep: true, // add done css
                                markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                                removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                                enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                            }
            });

            // $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
            //     //var elmForm = $("#form-step-" + stepNumber);
            //     //var uniq_no = Math.floor((Math.random() * 1000) + 1);
            //     //$("#nominal").text('Rp.'+uniq_no);
            //     //$("#nominal").mask('000.000.000.000.000,00', {reverse: true}).trigger('input');
            //     var elmForm0 = $("#form-step-0");
            //     var elmForm1 = $("#form-step-1");
            //     var elmForm2 = $("#form-step-2");
            //     var elmForm3 = $("#form-step-3");
            //     // stepDirection === 'forward' :- this condition allows to do the form validation
            //     // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
            //     if(stepDirection === 'forward' && elmForm0 || elmForm1 || elmForm2 || elmForm3){
            //         elmForm0.validator('validate');
            //         elmForm1.validator('validate');
            //         elmForm2.validator('validate');
            //         elmForm3.validator('validate');
            //         var elmErr0 = elmForm0.children('.has-error');
            //         if(elmErr0 && elmErr0.length > 0){
            //             // Form validation failed
            //             return false;
            //         }
            //         var elmErr1 = elmForm1.children('.has-error');
            //         if(elmErr1 && elmErr1.length > 0){
            //             // Form validation failed
            //             return false;
            //         }
            //         var elmErr2 = elmForm2.children('.has-error');
            //         if(elmErr2 && elmErr2.length > 0){
            //             // Form validation failed
            //             return false;
            //         }
            //         var elmErr3 = elmForm3.children('.has-error');
            //         if(elmErr3 && elmErr3.length > 0){
            //             // Form validation failed
            //             return false;
            //         }
            //     }
            //     return true;
            // });

            // $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
            //     // Enable finish button only on last step
            

            //     if($('button.sw-btn-next').hasClass('disabled')){
            //         $('.sw-btn-group-extra').show(); // show the button extra only in the last page
            //     }else{
            //         $('.sw-btn-group-extra').hide();                
            //     }

            // });


            // External Button Events
            $("#reset-btn").on("click", function() {
                // Reset wizard
                $('#smartwizard').smartWizard("reset");
                return true;
            });

            $("#prev-btn").on("click", function() {
                // Navigate previous
                $('#smartwizard').smartWizard("prev");
                return true;
            });

            $("#next-btn").on("click", function() {
                // Navigate next
                //
                $('#smartwizard').smartWizard("next");

                return true;
            });

            // $("#theme_selector").on("change", function() {
            //     // Change theme
            //     $('#smartwizard').smartWizard("theme", $(this).val());
            //     return true;
            // });

            // // Set selected theme on page refresh
            // $("#theme_selector").change();
        });
    </script>

</body>
</html>