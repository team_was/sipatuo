<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sipatuo - Detail Urgent</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
	<?php
		$multiple_css = array('all.css','bootstrap.min.css','offcanvas.css');
		echo assets_css($multiple_css);
	?>
</head>
<body class="bg-light">
	<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <?php $this->load->view('common/navbar_title_view'); ?>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>"><i class="fas fa-smile-beam"></i> <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>campaign">Campaign</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>pendonor">Pendonor</a>
          </li>
          <li class="nav-item active mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>events">Kegiatan</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>signup"><i class="fas fa-user-plus"></i></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a alt="Login" title="Login" class="nav-link" href="<?php echo base_url(); ?>signin"><i class="fas fa-door-open"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="container-fluid">

      <div style="min-height:800px;" class="row">
        <div class="col-lg-8 pb-3 bg-light">
          <div class="col-lg-12 pt-5 text-center">
            <p style="color:#c80d0d;text-shadow: 2px 2px 12px white;font-size: 2.33em;" class="title-event display-4 mb-5 mt-0"><strong>#detail</strong>Kegiatan</p>
          </div>
          <div class="col-lg-12">
            <div class="row justify-content-center">
              
              <div class="col-lg-6 mr-2 pb-3">
                <div class="row">
                  <div style="min-height:200px;" class="col-lg-12 p-2 bg-white">
                    <!-- <span style="font-size: 0.88em;position:absolute;top:5px;left:10px;color:black;">Gambar :</span> -->
                    <img class="img-fluid" src="<?php echo base_url(); ?>assets/img/Menara.jpg">
                  </div>
                  <div class="col-lg-12 mt-3 pl-2">
                    <span class="btn btn-outline-danger btn-sm mb-2">Viewer : 0</span>
                    <span class="btn btn-outline-info btn-sm mb-2">Join : 0</span>
                  </div>
                  <div class="col-lg-12 pl-2 pt-3">
                    <span style="border-radius: 25px;" class="btn btn-outline-dark btn-sm mb-2">#donordarah</span>
                    <span style="border-radius: 25px;" class="btn btn-outline-dark btn-sm mb-2">#kegiatandonor</span>
                    <span style="border-radius: 25px;" class="btn btn-outline-dark btn-sm mb-2">#BPJSKesehatan</span>
                    <span style="border-radius: 25px;" class="btn btn-outline-dark btn-sm mb-2">#DinasKependidikan</span>
                    <span style="border-radius: 25px;" class="btn btn-outline-dark btn-sm mb-2">#donordarah</span>
                    <span style="border-radius: 25px;" class="btn btn-outline-dark btn-sm mb-2">#kegiatandonor</span>
                  </div>
                  <div class="col-lg-12">
                    
                  </div>
                </div>
              </div>
              <div style="min-height:500px;" class="col-lg-5 pb-3">
                <div class="row">
                  <div style="border-radius: 10px;" class="col-lg-12 bg-white mb-1">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:black;">Tema Kegiatan :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod. </p>
                  </div>
                  <div style="border-radius: 10px;" class="col-lg-12 bg-warning mb-1">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Deskripsi :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                  </div>
                  <div style="border-radius: 10px;" class="col-lg-12 bg-info mb-1">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Alamat :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead text-light p-2 pb-3">Jalan Perintis Kemerdekaan 10, Rumah sakit Universitas Hasanuddin (Tampilkan dalam maps)</p>
                  </div>
                  <div style="border-radius: 10px;" class="col-lg-12 bg-danger mb-1">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Waktu Kegiatan :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead text-light p-2 pb-3">02 Agustus 2018, Jam 8 Pagi - Selesai</p>
                  </div>
                  <div style="border-radius: 10px;" class="col-lg-12 bg-secondary mb-1">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Organized by :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead text-light p-2 pb-3">BPJS Kesehatan</p>
                  </div>
                  <div style="border-radius: 10px;" class="col-lg-12 bg-primary mb-1">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Supported by :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead text-light p-2 pb-3">Kalla Group, Dinas Kesehatan Makassar, Dinas Pendidikan</p>
                  </div>                
                </div>
              </div>

              <div style="border-radius: 15px;" class="col-lg-11 bg-white mt-5 p-3 mb-5">
                <label style="font-size:2em;" class="display-4 mb-0">Komentar</label> <span style="font-size: 1.2em;" class="badge badge-dark">2</span>
                <hr/>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="media text-dark mb-2">
                      <img style="width:40px;height:40px;" src="<?php echo base_url(); ?>assets/img/user-3.png" alt="" class="mr-2 rounded">
                      <p class="media-body pb-3 mb-0 small lh-125 border-gray">
                        <strong class="d-block text-dark">Ahmad Imron</strong>
                        <span style="color:#afafaf;font-size:0.9em;" class="d-block mb-2"><i class="far fa-clock mr-2"></i>24 September 2018 , 08:00</span>
                        Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                      </p>
                    </div>
                    <div class="media text-dark mb-2">
                      <img style="width:40px;height:40px;" src="<?php echo base_url(); ?>assets/img/user-4.png" alt="" class="mr-2 rounded">
                      <p class="media-body pb-3 mb-0 small lh-125 border-gray">
                        <strong class="d-block text-dark">Suryadarmadi</strong>
                        <span style="color:#afafaf;font-size:0.9em;" class="d-block mb-2"><i class="far fa-clock mr-2"></i>24 September 2018 , 08:00</span>
                        Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                      </p>
                    </div>
                  </div>

                  <div class="col-lg-12 mb-3 px-4">
                    <form>
                      <hr/>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-lg-5 mb-2">
                            <!-- <label class="lbl" for="username"><i style="font-size: 0.8em;" class="fab fa-facebook-square mr-2"></i>Facebook</label> -->
                            <input style="border-radius: 20px;font-size:0.8em;" type="text" class="form-control form-control-sm px-3 py-2" id="nama" placeholder="Masukkan nama anda">
                          </div>
                          <div class="col-lg-7 mb-2">
                            <!-- <label class="lbl" for="username"><i style="font-size: 0.8em;" class="fab fa-line mr-2"></i>Line</label> -->
                            <input style="border-radius: 20px;font-size:0.8em;" type="email" class="form-control form-control-sm px-3 py-2" id="email" placeholder="Masukkan email anda">
                          </div>
                        </div>                   
                      </div>
                      <div class="form-group mb-2">
                        <textarea placeholder="Masukkan Komentar Anda" rows="4" style="border-radius: 20px;font-size:0.8em;" class="form-control"></textarea>
                      </div>
                      <div class="col-lg-12 text-right">
                        <button style="border-radius: 25px" type="submit" class="btn btn-sm btn-warning mb-2 px-4"><i class="far fa-comment-dots mr-1"></i>Simpan Komentar</button>
                      </div>
                    </form>
                  </div>  
                </div>
              </div>


            </div>
          </div>
        </div>
        <div class="col-lg-4 bg-white">
          <div class="my-3 p-lg-3 bg-white rounded mt-5">
            <a style="font-size:0.75em;" href="#" class="float-right">Lihat Semua</a>
            <h6 class="border-bottom border-gray pb-2 mb-0">Kegiatan donor lainnya</h6>
            <div class="media text-dark pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-dark">Bersama membangun bangsa yang peduli sesama.</strong>
                <span style="color:#113775;" class="d-block mb-2">Organized by : BPJS Kesehatan </span>
                Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu kegiatan : 24 Juli 2018</span>
                <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>events/detail/ev123nt" class="btn btn-sm btn-success ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
              </p>
            </div>
            <div class="media text-dark pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=0ccadb&fg=0ccadb&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-dark">Bakti sosial donor darah</strong>
                <span style="color:#113775;" class="d-block mb-2">Organized by : PLN </span>
                Donec id elit non mi porta gravida at eget metus. 
                
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Jakarta Selatan</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu kegiatan : 24 Agustus 2018</span>
                <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>events/detail/ev123nt" class="btn btn-sm btn-success ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
              </p>
            </div>
            <div class="media text-dark pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=6f42c1&fg=6f42c1&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-dark">Bakti sosial donor darah</strong>
                <span style="color:#113775;" class="d-block mb-2">Organized by : PLN </span>
                Donec id elit non mi porta gravida at eget metus. 
                
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Jakarta Selatan</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu kegiatan : 24 Agustus 2018</span>
                <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>events/detail/ev123nt" class="btn btn-sm btn-success ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
              </p>
            </div>
            <div class="media text-dark pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=dbbb0c&fg=dbbb0c&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-dark">Bakti sosial donor darah</strong>
                <span style="color:#113775;" class="d-block mb-2">Organized by : Pertamina </span>
                Donec id elit non mi porta gravida at eget metus. 
                
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Jakarta Selatan</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu kegiatan : 24 Agustus 2018</span>
                <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>events/detail/ev123nt" class="btn btn-sm btn-success ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
              </p>
            </div>
            <div class="media text-dark pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=aad005&fg=aad005&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-dark">Bakti sosial donor darah</strong>
                <span style="color:#113775;" class="d-block mb-2">Organized by : Pertamina </span>
                Donec id elit non mi porta gravida at eget metus. 
                
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Jakarta Selatan</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu kegiatan : 24 Agustus 2018</span>
                <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>events/detail/ev123nt" class="btn btn-sm btn-success ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
              </p>
            </div>
            <small class="d-block text-right mt-3 mb-3">
              <a class="text-dark" href="<?php echo base_url(); ?>events">Lihat Semua Kegiatan</a>
            </small>
          </div>
        </div>
      </div>
      <div class="row justify-content-center box-gabung-sipper">
        <div class="col-lg-12 mt-5 text-gabung-donor">
          <h1 style="font-size:2em;color:black;" class="display-4"><strong>Ayo </strong>Bergabung bersama kami</h1>
          <h1 style="font-size:1em;color:#153b77;" class="display-4">Gabung, temukan, minta bantuan dan bantu teman yang membutuhkan bantuan anda.</h1>
        </div>
        <div style="text-align: center;" class="col-lg-6">
          <a style="border-radius:20px;" href="<?php echo base_url(); ?>signup" class="btn btn-outline-warning px-lg-5 btn-lg-lg">Daftar</a> <span class="px-lg-2">atau</span>
          <a style="border-radius:20px;" href="<?php echo base_url(); ?>signin" class="btn btn-outline-info px-lg-5 btn-lg-lg">Login</a>
        </div>
      </div>
    </main>

    <?php $this->load->view('common/footer_view'); ?>
    
<?php
	$multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
	echo assets_js($multiple_js);
?>
</body>
</html>