<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sipatuo - Urgent</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
	<?php
		$multiple_css = array('all.css','bootstrap.min.css','offcanvas.css');
		echo assets_css($multiple_css);
	?>

  <style type='text/css'>
    .fade-btm {
      -webkit-mask-image:-webkit-gradient(linear, left top, left bottom, from(rgba(0,0,0,1)), to(rgba(0,0,0,0)));
      mask-image: linear-gradient(to bottom, rgba(0,0,0,1), rgba(0,0,0,0));
    }
  </style>
</head>
<body class="bg-light">
	<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <?php $this->load->view('common/navbar_title_view'); ?>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>"><i class="fas fa-smile-beam"></i> <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>campaign">Campaign</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>pendonor">Pendonor</a>
          </li>
          <li class="nav-item active mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>events">Kegiatan</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>signup"><i class="fas fa-user-plus"></i></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a alt="Login" title="Login" class="nav-link" href="<?php echo base_url(); ?>signin"><i class="fas fa-door-open"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <?php $dataevent = 1; ?>
    <main role="main" class="container-fluid">
      <div style="min-height:600px;" class="row justify-content-center bg-white">
        <div class="col-lg-10">
          <div class="row justify-content-center pt-4">
            <?php if($dataevent > 0){ ?>
            <div style="text-align: center;" class="col-lg-12 mt-5 mb-4 d-none d-md-block d-lg-block">
              <h1 style="font-size:2.5em;color:black;" class="display-4"><strong>#kegiatan</strong>Donor</h1>
            </div>
            <div style="text-align: center;" class="col-lg-12 pb-5 d-none d-md-block d-lg-block">
              <a class="text-dark" href="#"><i style="font-size:1.3em;" class="fas fa-search mr-2"></i></a>
              <a class="text-dark" href="#"><i style="font-size:1.3em;" class="fas fa-plus-circle"></i></a>
            </div>
            <div class="col-lg-12 d-lg-none d-md-none">
              <div class="float-right"><a style="font-size:1.15em;" href="#" class="text-success mr-1"><i class="fas fa-plus-circle"></i></a> <a style="font-size:1.15em;" href="#" class="text-dark"><i class="fas fa-search"></i></a></div>
              <h6 style="" class="border-bottom border-gray pb-3 mb-0">Daftar Kegiatan Donor</h6>
            </div>
            <div class="col-lg-3 col-md-4 mb-lg-5">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img style="border:solid 1px #e5e5e5" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-2.jpg" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block"><a class="text-dark" href="<?php echo base_url(); ?>events/detail/ev123nt">Tema : Card title that wraps to a new line latihan melakukan</a></strong>
                  <span style="color:#ff9e9e" class="d-block mb-2">Penyelenggara : BPJS Kesehatan</span>
                  This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                  <span class="d-block text-gray-dark pt-2">Alamat : Jalan Perintis Kemerdekaan 10, Rumah sakit Universitas Hasanuddin (Tampilkan dalam maps)</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu : 24 Juli 2018</span>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>events/detail/ev123nt" class="btn btn-sm btn-success ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-comments"></i> 0</a>
                </p>
              </div>
              <div style="border:solid 1px #e2e2e2" class="card d-none d-md-block d-lg-block">
                <img class="card-img-top fade-btm" src="<?php echo base_url(); ?>assets/img/Donor-Darah.jpg" alt="Card image cap">
                <div class="card-body">
                  <h5 style="font-size:1.20em;font-weight: 400;margin-top:-20px;" class="card-title display-4"><a class="text-dark" href="<?php echo base_url(); ?>events/detail/ev123nt">Card title that wraps to a new line</a></h5>
                  <div class="mb-1">
                    <i class="far fa-clock mr-1 text-info"></i>
                    <i class="fas fa-map-marker-alt mr-1 text-danger"></i>
                    <a href="#"><i class="far fa-comment-dots text-dark"></i><small> 155</small></a>
                  </div>
                  <p style="font-size:0.9em;" class="card-text lead mb-3">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
                  <a style="" href="<?php echo base_url(); ?>events/detail/ev123nt" class="btn btn-dark btn-sm urgent-info px-3 mb-2"><i class="fas fa-info-circle mr-1"></i> Detail Kegiatan</a>
                  <span style="position: absolute;top:5px;right:5px;" class="btn btn-success btn-sm urgent-info mb-2">Open</span>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 mb-lg-5">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img style="border:solid 1px #efefef" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-5.png" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block"><a class="text-dark" href="<?php echo base_url(); ?>events/detail/ev123nt">Tema : Membangun Palu dan Donggala</a></strong>
                  <span style="color:#ff9e9e" class="d-block mb-2">Penyelenggara : BPJS Kesehatan</span>
                  -
                  <span class="d-block text-gray-dark pt-2">Alamat : Jalan Perintis Kemerdekaan 10, Rumah sakit Universitas Hasanuddin (Tampilkan dalam maps)</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu : 24 Juli 2018</span>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>events/detail/ev123nt" class="btn btn-sm btn-success ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-comments"></i> 0</a>
                </p>
              </div>
              <div style="border:solid 1px #e2e2e2" class="card d-none d-md-block d-lg-block">
                <img class="card-img-top fade-btm" src="<?php echo base_url(); ?>assets/img/Donor-Darah-2.jpg" alt="Card image cap">
                <div class="card-body">
                  <h5 style="font-size:1.20em;font-weight: 400;margin-top:-20px;" class="card-title display-4"><a class="text-dark" href="<?php echo base_url(); ?>events/detail/ev123nt">Card title that wraps to a new line</a></h5>
                  <div class="mb-1">
                    <i class="far fa-clock mr-1 text-info"></i>
                    <i class="fas fa-map-marker-alt mr-1 text-danger"></i>
                    <a href="#"><i class="far fa-comment-dots text-dark"></i><small> 0</small></a>
                  </div>
                  <p style="font-size:0.9em;" class="card-text lead mb-3">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
                  <a style="" href="<?php echo base_url(); ?>events/detail/ev123nt" class="btn btn-dark btn-sm urgent-info px-3 mb-2"><i class="fas fa-info-circle mr-1"></i> Detail Kegiatan</a>
                  <span style="position: absolute;top:5px;right:5px;" class="btn btn-warning btn-sm urgent-info py-0 mb-2">Completed</span>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 mb-lg-5">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img style="border:solid 1px #efefef" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-2.jpg" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Tema : Card title that wraps to a new line latihan melakukan</strong>
                  <span style="color:#ff9e9e" class="d-block mb-2">Penyelenggara : BPJS Kesehatan</span>
                  This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                  <span class="d-block text-gray-dark pt-2">Alamat : Jalan Perintis Kemerdekaan 10, Rumah sakit Universitas Hasanuddin (Tampilkan dalam maps)</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu : 24 Juli 2018</span>
                </p>
              </div>
              <div style="border:solid 1px #e2e2e2" class="card d-none d-md-block d-lg-block">
                <img class="card-img-top fade-btm" src="<?php echo base_url(); ?>assets/img/Donor-Darah-2.jpg" alt="Card image cap">
                <div class="card-body">
                  <h5 style="font-size:1.20em;font-weight: 400;margin-top:-20px;" class="card-title display-4"><a class="text-dark" href="<?php echo base_url(); ?>events/detail/ev123nt">Card title that wraps to a new line</a></h5>
                  <div class="mb-1">
                    <i class="far fa-clock mr-1 text-info"></i>
                    <i class="fas fa-map-marker-alt mr-1 text-danger"></i>
                    <a href="#"><i class="far fa-comment-dots text-dark"></i><small> 0</small></a>
                  </div>
                  <p style="font-size:0.9em;" class="card-text lead mb-3">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
                  <a style="" href="<?php echo base_url(); ?>events/detail/ev123nt" class="btn btn-dark btn-sm urgent-info px-3 mb-2"><i class="fas fa-info-circle mr-1"></i> Detail Kegiatan</a>
                  <span style="position: absolute;top:5px;right:5px;" class="btn btn-warning btn-sm urgent-info py-0 mb-2">Completed</span>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 mb-lg-5">
              <div style="height:387px;background: #efefef;border:none;" class="card d-none d-md-block d-lg-block">
              </div>
            </div>
            <div style="text-align: center;" class="col-lg-12 mb-5 mt-4">
              <button style="border-radius: 20px;" type="button" class="btn btn-sm btn-danger px-4"><i style="color:white;" class="fas fa-globe-asia mr-2"></i> Tampilkan Lainnya</button>
            </div>
            <?php } else { ?>
              <div class="col-lg-6 mt-5 mb-5">
                <div class="jumbotron text-center pt-5">
                    <img width="120px" src="<?php echo base_url(); ?>assets/img/event.png" alt="">
                    <h1 style="font-size:1.7em;" class="display-4 mb-4">Belum ada <strong style="color:#ce0000;">kegiatan</strong> donor.</h1>
                    <p style="font-size:1.1em;" class="lead mb-4">Kegiatan donor dilaksanakan untuk memenuhi kebutuhan darah Palang Merah Indonesia dan Institusi terkait lainnya.</p>
                    <hr class="my-4">
                    <a style="border-radius: 20px;" href="<?php echo base_url(); ?>event" class="btn btn-sm btn-danger mb-2 px-4"><i style="color:white;" class="fas fa-plus-circle mr-2"></i> Buat Kegiatan</a><br/>
                </div>
              </div>
            <?php } ?>
          </div>
          
        </div>
      </div>
    </main>

    <?php $this->load->view('common/footer_view'); ?>
    
<?php
	$multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
	echo assets_js($multiple_js);
?>
</body>
</html>