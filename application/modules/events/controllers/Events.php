<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends MX_Controller {
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->load->view('view_listeventpage');
	}

	function detail_events()
	{
		$this->load->view('view_detaileventpage');
	}

	function create()
	{
		$this->load->view('view_createeventpage');
	}
}