<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sipatuo - Hadir membantu anda.</title>
    <link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
    <?php
      $multiple_css = array('all.css','bootstrap.min.css','startpage.css');
      echo assets_css($multiple_css);
    ?>
  </head>
  <body class="bg-light">
    <nav style="" class="navbar shadow-sm navbar-expand-lg fixed-top navbar-dark bg-light">
      <?php $this->load->view('common/navbar_title_admin_view'); ?>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>home"><i class="fas fa-home"></i></a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;" class="nav-link dropdown-toggle" id="userdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url() ?>profil"><i class="far fa-list-alt"></i></a>
            <div style="margin-top:-17px;margin-right:130px;" class="dropdown-menu dropdown-menu-right" aria-labelledby="userdropdown">
              <h6 class="dropdown-header">General</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>campaign"><i class="fas fa-bullhorn mr-2"></i>Semua Campaign</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>events"><i class="far fa-calendar-alt mr-2"></i>Semua Kegiatan</a></span>
              <div class="dropdown-divider"></div>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>pendonor"><i class="fas fa-users mr-2"></i>Semua Pendonor</a></span>
            </div>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>notification"><i class="far fa-bell"></i>
              <label class="bg-danger count-new-notif-1"><strong>4</strong></label>
            </a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>message"><i class="far fa-envelope"></i>
              <label class="bg-danger count-new-msg-1"><strong>9</strong></label>
            </a>
          </li>
          <li class="nav-item active mr-lg-2">
            <a style="font-size:1.3em;color:#636363;" class="nav-link dropdown-toggle" id="userdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url() ?>profil"><i class="far fa-user-circle"></i></a>
            <div style="margin-top:-17px;margin-right:65px;" class="dropdown-menu dropdown-menu-right" aria-labelledby="userdropdown">
              <h6 class="dropdown-header">Personal</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/campaign"><i class="fas fa-bullhorn mr-2"></i>Campaign Anda</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/event"><i class="far fa-calendar-alt mr-2"></i>Kegiatan Anda</a></span>
              <div class="dropdown-divider"></div>
              <h6 class="dropdown-header">Profil</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/password"><i class="fas fa-fingerprint mr-2"></i>Ganti Password</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil"><i class="far fa-user mr-2"></i>Profil</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>signout"><i class="fas fa-power-off mr-2"></i>Keluar</a></span>
            </div>
          </li>
          <li class="nav-item">
            <a style="font-size:1.3em;color:#636363;" class="nav-link" href="<?php echo base_url(); ?>signout"><i class="fas fa-power-off"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    

    <main role="main" class="container-fluid">
      
      <div style="min-height:800px;" class="row">

        <div class="col-lg-4 bg-white">
          <div style="text-align: center;" class="col-lg-12 d-none d-md-block d-lg-block">
            <!-- <p style="color:white;text-shadow: 2px 2px 12px #c80d0d;font-size: 1.55em;" class="display-4 mb-5 mt-5 pt-3"><strong>#kebutuhan</strong>Lainnya</p> -->
          </div>

          <div class="my-3 p-3 rounded mt-5 bg-white">
            <div class="tabs pl-0 pr-0">
              <div class="tab-button-outer">
                <ul id="tab-button">
                  <li><a href="#tab01">Campaign Anda</a></li>
                  <li><a href="#tab02">Kegiatan Anda</a></li>
                  <!-- <li><a href="#tab03">Tab 3</a></li>
                  <li><a href="#tab04">Tab 4</a></li>
                  <li><a href="#tab05">Tab 5</a></li> -->
                </ul>
              </div>
              <div class="tab-select-outer">
                <select id="tab-select">
                  <li><a href="#tab01">Campaign Anda</a></li>
                  <li><a href="#tab02">Kegiatan Anda</a></li>
                  <!-- <option value="#tab03">Tab 3</option>
                  <option value="#tab04">Tab 4</option>
                  <option value="#tab05">Tab 5</option> -->
                </select>
              </div>

              <div id="tab01" class="tab-contents pt-4 p-0">
                <!-- <h2>Campaign Anda</h2> -->
                
                <a style="font-size:0.75em;" href="<?php echo base_url(); ?>profil/campaign" class="float-right">Lihat Semua Campaign Anda</a>
                <h6 class="border-bottom border-gray pb-2 mb-0">Campaign Anda</h6>

                <div class="media text-dark pt-3">
                  <div style="width: 42px;height:32px;background:#ffd6d6;color:#8e8e8e;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>A+</strong></div>
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Butuh : A+ (3 Kantong)</strong>
                    <span style="color:#960000;" class="d-block mb-2">Request by : Yunita Sinegar</span>
                    Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                    
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-info ml-0 mt-2"><i class="fas fa-pen mr-1"></i> Edit</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-trash mr-1"></i> Delete</a>
                  </p>
                </div>
                <div class="media text-dark pt-3">
                  <div style="width: 42px;height:32px;background:#ffd6d6;color:#8e8e8e;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>Tr</strong></div>
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Butuh : Trombosit (10 Kantong)</strong>
                    <span style="color:#960000;" class="d-block mb-2">Request by : Ringki datas</span>
                    Donec id elit non mi porta gravida at eget metus.               
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-info ml-0 mt-2"><i class="fas fa-pen mr-1"></i> Edit</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-trash mr-1"></i> Delete</a>
                  </p>
                </div>
                <div class="media text-dark pt-3">
                  <div style="width: 42px;height:32px;background:#ffd6d6;color:#8e8e8e;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>AB+</strong></div>
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Butuh : AB+ (10 Kantong)</strong>
                    <span style="color:#960000;" class="d-block mb-2">Request by : Ananda Riswa </span>
                    Donec id elit non mi porta gravida at eget metus.
                    
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-info ml-0 mt-2"><i class="fas fa-pen mr-1"></i> Edit</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-trash mr-1"></i> Delete</a>
                  </p>
                </div>
                <div class="media text-dark pt-3">
                  <div style="width: 42px;height:32px;background:#ffd6d6;color:#8e8e8e;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>B+</strong></div>
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Butuh : B+ (20 Kantong)</strong>
                    <span style="color:#960000;" class="d-block mb-2">Request by : Indrawan Silha</span>
                    Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                    
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-info ml-0 mt-2"><i class="fas fa-pen mr-1"></i> Edit</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-trash mr-1"></i> Delete</a>
                  </p>
                </div>
                <small class="d-block text-right mt-3 mb-3">
                  <a class="text-danger" href="<?php echo base_url(); ?>profil/campaign">Lihat Semua Campaign Anda</a>
                </small>


              </div>
              <div id="tab02" class="tab-contents pt-4 p-0">
                <!-- <h2>Kegiatan Anda</h2> -->
                <a style="font-size:0.75em;" href="<?php echo base_url(); ?>profil/event" class="float-right">Lihat Semua Kegiatan Anda</a>
                <h6 class="border-bottom border-gray pb-2 mb-0">Kegiatan Anda</h6>

                <div class="media text-dark pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=#018715&fg=#018715&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Butuh : A+ (3 Kantong)</strong>
                    <span style="color:#960000;" class="d-block mb-2">Request by : Yunita Sinegar</span>
                    Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                    
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-info ml-0 mt-2"><i class="fas fa-pen mr-1"></i> Edit</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-trash mr-1"></i> Delete</a>
                  </p>
                </div>
                <div class="media text-dark pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=#018715&fg=#018715&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Butuh : Trombosit (10 Kantong)</strong>
                    <span style="color:#960000;" class="d-block mb-2">Request by : Ringki datas</span>
                    Donec id elit non mi porta gravida at eget metus.               
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-info ml-0 mt-2"><i class="fas fa-pen mr-1"></i> Edit</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-trash mr-1"></i> Delete</a>
                  </p>
                </div>
                <div class="media text-dark pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=#018715&fg=#018715&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Butuh : AB+ (10 Kantong)</strong>
                    <span style="color:#960000;" class="d-block mb-2">Request by : Ananda Riswa </span>
                    Donec id elit non mi porta gravida at eget metus.
                    
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-info ml-0 mt-2"><i class="fas fa-pen mr-1"></i> Edit</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-trash mr-1"></i> Delete</a>
                  </p>
                </div>
                <div class="media text-dark pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=#018715&fg=#018715&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Butuh : B+ (20 Kantong)</strong>
                    <span style="color:#960000;" class="d-block mb-2">Request by : Indrawan Silha</span>
                    Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                    
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-info ml-0 mt-2"><i class="fas fa-pen mr-1"></i> Edit</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-trash mr-1"></i> Delete</a>
                  </p>
                </div>
                <small class="d-block text-right mt-3 mb-3">
                  <a class="text-white" href="<?php echo base_url(); ?>profil/event">Lihat Semua Kegiatan Anda</a>
                </small>
              </div>
              
            </div>



            
          </div>
        </div>

        <div class="col-lg-8 pb-3 bg-light">
          <div class="col-lg-12 pt-5 text-center">
            <p style="color:#c80d0d;text-shadow: 2px 2px 12px white;font-size: 2.33em;" class="display-4 mb-5 mt-0"><strong>#profil</strong>Anda</p>
          </div>
          <div class="col-lg-12">
            <div class="row justify-content-center">
              
              <div class="col-lg-4 pb-3 mr-2">
                <div class="row">
                  <div style="min-height:200px;" class="col-lg-12 p-2 bg-white">
                    <label style="position: absolute;top:7;left:7;font-size: 0.60em;" class="btn btn-dark"> <i class="fas fa-camera mr-1"></i> Change Photo<input type="file" style="display: none;"></label>
                    <img class="img-fluid" src="<?php echo base_url(); ?>assets/img/user-3.png">
                  </div>
                  <div style="word-wrap: break-word;" class="col-lg-12 bg-primary">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Bio / Motto :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">Bahagia itu sederhana, sesederhana perasaan ini yang mudah jatuh karenamu.
                  </p>
                  </div>
                  
                  <!-- <div class="col-lg-12 pl-2 pt-3">
                    <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-line text-success"></i> line_id</span>
                    <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-facebook-square text-primary"></i> facebook_id</span>
                    <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-blackberry text-danger"></i> BBM_id</span>
                    <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-whatsapp"></i> whatsapp_no</span>
                    <span class="btn btn-outline-dark btn-sm mb-2">#donordarah</span>
                    <span class="btn btn-outline-dark btn-sm mb-2">#kegiatandonor</span>
                  </div> -->
                </div>
              </div>
              <div style="min-height:500px;" class="col-lg-5 pb-3">
                <div class="row">
                  <div class="col-lg-12 bg-white">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:black;">Nama :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3"><strong>Suryadaramadi Sadar.</strong></p>
                  </div>
                  <div class="col-lg-12 bg-warning">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Golongan Darah :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">A Rhesus + (Positif)</p>
                  </div>
                  <div class="col-lg-12 bg-info">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Lokasi :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">Indonesia, Sulawesi Selatan, Makassar</p>
                  </div>
                  <div class="col-lg-12 bg-danger">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Email :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">suryadarmadi@gmail.com</p>
                  </div>
                  <div class="col-lg-12 bg-secondary">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Phone :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">081 111 111 111</p>
                  </div>
                  <div style="background: #ffb5b5;" class="col-lg-12">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Status :</span>
                    <p style="font-size: 0.88em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">
                      Bergabung pada 12 Juli 2018. <br/>
                      Terakhir online 3 Menit yang lalu.
                    </p>
                  </div>
                  <div style="" class="col-lg-12 bg-white">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:black;">Sosial Media :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">
                      <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-line text-success"></i> line_id</span>
                      <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-facebook-square text-primary"></i> facebook_id</span>
                      <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-twitter text-info"></i> twitter_id</span>
                      <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-blackberry text-danger"></i> BBM_id</span>
                      <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-whatsapp"></i> whatsapp_no</span>
                      <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-telegram"></i> telegram_id</span>
                    </p>
                  </div>
                  <div class="col-lg-12 pt-3 pl-0">
                    <a href="<?php echo base_url(); ?>profil/edit" class="btn btn-warning btn-sm pl-3 pr-3 p-2"><i class="fas fa-pencil-alt mr-2"></i> Edit Profil</a>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
        
      </div>
      
      <?php $this->load->view('common/invitation_view'); ?>
      
    </main>

    <?php $this->load->view('common/footer_view'); ?>

    <?php
      $multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
      echo assets_js($multiple_js);
    ?>
  </body>
</html>