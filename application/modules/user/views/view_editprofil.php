<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sipatuo - Hadir membantu anda.</title>
    <link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
    <?php
      $multiple_css = array('all.css','bootstrap.min.css','startpage.css');
      echo assets_css($multiple_css);
    ?>
  </head>
  <body class="bg-light">
    <nav style="" class="navbar shadow-sm navbar-expand-lg fixed-top navbar-dark bg-light">
      <?php $this->load->view('common/navbar_title_admin_view'); ?>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>home"><i class="fas fa-home"></i></a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;" class="nav-link dropdown-toggle" id="userdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url() ?>profil"><i class="far fa-list-alt"></i></a>
            <div style="margin-top:-17px;margin-right:130px;" class="dropdown-menu dropdown-menu-right" aria-labelledby="userdropdown">
              <h6 class="dropdown-header">General</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>campaign"><i class="fas fa-bullhorn mr-2"></i>Semua Campaign</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>events"><i class="far fa-calendar-alt mr-2"></i>Semua Kegiatan</a></span>
              <div class="dropdown-divider"></div>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>pendonor"><i class="fas fa-users mr-2"></i>Semua Pendonor</a></span>
            </div>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>notification"><i class="far fa-bell"></i>
              <label class="bg-danger count-new-notif-1"><strong>4</strong></label>
            </a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>message"><i class="far fa-envelope"></i>
              <label class="bg-danger count-new-msg-1"><strong>9</strong></label>
            </a>
          </li>
          <li class="nav-item active mr-lg-2">
            <a style="font-size:1.3em;color:#636363;" class="nav-link dropdown-toggle" id="userdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url() ?>profil"><i class="far fa-user-circle"></i></a>
            <div style="margin-top:-17px;margin-right:65px;" class="dropdown-menu dropdown-menu-right" aria-labelledby="userdropdown">
              <h6 class="dropdown-header">Personal</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/campaign"><i class="fas fa-bullhorn mr-2"></i>Campaign Anda</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/event"><i class="far fa-calendar-alt mr-2"></i>Kegiatan Anda</a></span>
              <div class="dropdown-divider"></div>
              <h6 class="dropdown-header">Profil</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/password"><i class="fas fa-fingerprint mr-2"></i>Ganti Password</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil"><i class="far fa-user mr-2"></i>Profil</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>signout"><i class="fas fa-power-off mr-2"></i>Keluar</a></span>
            </div>
          </li>
          <li class="nav-item">
            <a style="font-size:1.3em;color:#636363;" class="nav-link" href="<?php echo base_url(); ?>signout"><i class="fas fa-power-off"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    

    <main role="main" class="container-fluid">
      
      <div style="min-height:550px;" class="row justify-content-center">

        <div class="col-lg-7">
          <div class="row justify-content-center pt-4">
            <div style="text-align: center;" class="col-lg-12 d-none d-md-block d-lg-block">
              <p style="color:#c80d0d;text-shadow: 2px 2px 12px white;font-size:2.5em;" class="display-4 mb-3 mt-3"><strong>#edit</strong>Profil</p>
            </div>
            <div class="col-lg-12 mb-5 pb-5">
              <div style="border-radius: 15px;" class="my-3 p-5 bg-white shadow-sm">
                <form>
                  <div class="form-group">
                    <label class="lbl" for="username"><i style="font-size: 0.8em;" class="far fa-address-card mr-2"></i>Nama Lengkap <strong class="text-danger">*</strong></label>
                    <input style="border-radius: 20px;font-size:0.8em;" type="text" class="form-control form-control-sm px-3 py-2" id="username" placeholder="Masukkan nama lengkap anda">
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-lg-6">
                        <label class="lbl" for="password"><i style="font-size: 0.8em;" class="far fa-heart mr-2"></i></i>Golongan Darah <strong class="text-danger">*</strong></label>
                        <select style="border-radius: 20px;height: 38px;font-size:0.8em;" class="form-control form-control-sm py-2 px-3">
                          <option selected>Pilih Goldar Anda</option>
                          <option>A</option>
                          <option>B</option>
                          <option>O</option>
                          <option>AB</option>
                        </select>
                      </div>
                      <div class="col-lg-6">
                        <label class="lbl" for="password"><i style="font-size: 0.8em;" class="fab fa-nintendo-switch mr-2"></i>Rhesus <strong class="text-danger">*</strong></label>
                        <select style="border-radius: 20px;height: 38px;font-size:0.8em;" class="form-control form-control-sm py-2 px-3">
                          <option selected>Pilih Rhesus Goldar Anda</option>
                          <option>Positif</option>
                          <option>Negatif</option>
                          <option>Belum Tahu</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="lbl" for="password"><i style="font-size: 0.8em;" class="far fa-envelope mr-2"></i>Email <strong class="text-danger">*</strong></label>
                    <input style="border-radius: 20px;font-size:0.8em;" type="email" class="form-control form-control-sm px-3 py-2" id="password" placeholder="Masukkan email anda">
                    <!-- <small style="font-size:0.75em;" id="nameHelp" class="form-text text-muted">Masukkan password lama anda untuk memastikan bahwa anda benar-benar pemilik akun ini.</small> -->
                  </div>
                  <div class="form-group">
                    <label class="lbl" for="username"><i style="font-size: 0.8em;" class="fas fa-mobile-alt mr-2"></i>Telepon <strong class="text-danger">*</strong></label>
                    <input style="border-radius: 20px;font-size:0.8em;" type="text" class="form-control form-control-sm px-3 py-2" id="telepon" placeholder="Masukkan nomor telepon anda">
                  </div>
                  <div class="form-group mb-4">
                    <label class="lbl" for="username"><i style="font-size: 0.8em;" class="fas fa-award mr-2"></i>Motto / Bio</label>
                    <textarea rows="5" style="border-radius: 20px;font-size:0.8em;" class="form-control"></textarea>
                  </div>
                  <hr/>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-lg-6">
                        <label class="lbl" for="username"><i style="font-size: 0.8em;" class="fab fa-facebook-square mr-2"></i>Facebook</label>
                        <input style="border-radius: 20px;font-size:0.8em;" type="text" class="form-control form-control-sm px-3 py-2" id="fb" placeholder="Masukkan alamat facebook anda">
                      </div>
                      <div class="col-lg-6">
                        <label class="lbl" for="username"><i style="font-size: 0.8em;" class="fab fa-line mr-2"></i>Line</label>
                        <input style="border-radius: 20px;font-size:0.8em;" type="text" class="form-control form-control-sm px-3 py-2" id="line" placeholder="Masukkan id Line anda">
                      </div>
                    </div>                   
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-lg-6">
                        <label class="lbl" for="username"><i style="font-size: 0.8em;" class="fab fa-telegram-plane mr-2"></i>Telegram</label>
                        <input style="border-radius: 20px;font-size:0.8em;" type="text" class="form-control form-control-sm px-3 py-2" id="telegram" placeholder="Masukkan id telegram anda">
                      </div>
                      <div class="col-lg-6">
                        <label class="lbl" for="username"><i style="font-size: 0.8em;" class="fab fa-blackberry mr-2"></i>BBM </label>
                        <input style="border-radius: 20px;font-size:0.8em;" type="text" class="form-control form-control-sm px-3 py-2" id="bbm" placeholder="Masukkan id BBM anda">
                      </div>
                    </div>                   
                  </div>
                  <div class="col-lg-12 text-center">
                    <button style="border-radius: 25px" type="submit" class="btn btn-sm btn-warning mt-4 mb-2 px-4">Simpan Perubahan</button>
                  </div>
                    
                </form>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      
      <?php $this->load->view('common/invitation_view'); ?>
    </main>

    <?php $this->load->view('common/footer_view'); ?>

    <?php
      $multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
      echo assets_js($multiple_js);
    ?>
  </body>
</html>