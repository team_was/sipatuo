<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sipatuo - Login User</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
	<?php
		$multiple_css = array('all.css','bootstrap.min.css','offcanvas.css');
		echo assets_css($multiple_css);
	?>
</head>
<body class="bg-white">
	<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <a style="font-size:1.55em;" class="navbar-brand ml-lg-3 mr-lg-3" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/Guard-Red.png" class="sip-logo"> <span class="">SIPATUO</span></a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
        <!-- <i style="color:#f2f2f2;margin-right:7px;" class="fas fa-ellipsis-v"></i> -->
      </button>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>"><i class="fas fa-smile-beam"></i> <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>campaign">Campaign</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>pendonor">Pendonor</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>events">Kegiatan</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>signup"><i class="fas fa-user-plus"></i></a>
          </li>
          <li class="nav-item active mr-lg-4">
            <a alt="Login" title="Login" class="nav-link" href="<?php echo base_url(); ?>signin"><i class="fas fa-door-open"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="container-fluid">

      <div style="border-bottom: solid 0px #e8e8e8;" class="row justify-content-center">

        <div style="min-height:600px;" class="col-lg-5 bg-warning pt-5 order-sm-2">
          <div class="col-lg-12 pt-4 txt-sm-center">
              <p style="color:white;font-size: 2.33em;" class="display-4 mb-4 mt-0"><strong>#login</strong>Form</p>
              <?php  ?>
            </div>
          <div class="row justify-content-center">
            
            <div class="col-lg-9 txt-sm-center">
              <button style="background: #3b5998;color:white;border-radius:20px;" type="submit" class="btn btn-sm px-3 mb-2"><i class="fab fa-facebook mr-2"></i>Facebook</button>
              <button style="background: #f35656;color:white;border-radius:20px;" type="submit" class="btn btn-sm px-3 mb-2"><i class="fab fa-google mr-2"></i>Google</button>
              <hr/>
              <form>
                <div class="form-group">
                  <label class="lbl" for="username">Email or Username</label>
                  <input  style="border-radius: 20px;border:none;" type="text" class="form-control form-control-sm px-3 py-2" id="username" placeholder="Masukkan email atau username anda">
                </div>
                <div class="form-group">
                  <label class="lbl" for="password">Password</label>
                  <input  style="border-radius: 20px;border:none;" type="text" class="form-control form-control-sm px-3 py-2" id="password" placeholder="Masukkan password anda">
                </div>
                <button style="border-radius: 20px;" type="submit" class="btn btn-sm btn-success pl-5 pr-5">Sign In</button>
              </form>
              <p style="font-weight: 400;color:white;font-size:1em;" class="lead pt-4">Ingin mencari atau menjadi pendonor, anda harus mempunyai akun, silahkan <a href="<?php echo base_url(); ?>signup" class="text-danger">klik disini</a>.</p>
            </div>
          </div>
        </div>

        <div style="min-height:600px;" class="col-lg-7 bg-light order-sm-1">
          <div class="row justify-content-center pt-lg-5 mt-5 mb-5">

            <div id="myCarousel" class="carousel slide" data-ride="carousel">
              <!-- Carousel indicators -->
              <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
              </ol>   
              <!-- Wrapper for carousel items -->
              <div class="carousel-inner">
                <div class="item carousel-item active">
                  <div class="img-box"><img src="<?php echo base_url(); ?>assets/img/help-2.png" alt=""></div>
                  <p class="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
                  <p class="overview"><b>Help Other</b>, Sipatuo Team</p>
                </div>
                <div class="item carousel-item">
                  <div class="img-box"><img src="<?php echo base_url(); ?>assets/img/campaign-2.png" alt=""></div>
                  <p class="testimonial">Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget nisi a mi suscipit tincidunt. Utmtc tempus dictum risus. Pellentesque viverra sagittis quam at mattis. Suspendisse potenti. Aliquam sit amet gravida nibh, facilisis gravida odio.</p>
                  <p class="overview"><b>Campaign Blood Needs</b>, Sipatuo Team</p>
                </div>
                <div class="item carousel-item">
                  <div class="img-box"><img src="<?php echo base_url(); ?>assets/img/calendar-1.png" alt=""></div>
                  <p class="testimonial">Phasellus vitae suscipit justo. Mauris pharetra feugiat ante id lacinia. Etiam faucibus mauris id tempor egestas. Duis luctus turpis at accumsan tincidunt. Phasellus risus risus, volutpat vel tellus ac, tincidunt fringilla massa. Etiam hendrerit dolor eget rutrum.</p>
                  <p class="overview"><b>Organize Events</b>, Sipatuo Team</p>
                </div>
              </div>
              <!-- Carousel controls -->
              <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
                <i class="fa fa-angle-left"></i>
              </a>
              <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
                <i class="fa fa-angle-right"></i>
              </a>
            </div>

          </div>
        </div>
        <!-- <div class="col-lg-3 bg-danger">
          
        </div> -->
        
      </div>
      <!-- <div class="row justify-content-center box-gabung-sipper">
        <div class="col-lg-12 mt-5 text-gabung-donor">
          <h1 style="font-size:2em;color:black;" class="display-4"><strong>Ayo </strong>Bergabung bersama kami</h1>
          <h1 style="font-size:1em;color:#153b77;" class="display-4">Gabung, temukan, minta bantuan dan bantu teman yang membutuhkan bantuan anda.</h1>
        </div>
        <div style="text-align: center;" class="col-lg-6">
          <button type="button" class="btn btn-warning btn-lg-lg">Daftar</button> <span>atau</span>
          <button type="button" class="btn btn-info btn-lg-lg">Login</button>
        </div>
      </div> -->
    </main>

    <footer style="margin-top:50px;padding-top: 11px;" class="text-muted">
      <div class="container">
        <img class="float-right" width="50px" src="<?php echo base_url(); ?>assets/img/Guard-Dark.png">
        <!-- <p class="float-right">
          <a href="#"><i class="fas fa-tint"></i></a>
        </p> -->
        <a href="#"><i style="font-size: 1.55em;color:#174a9b;" class="fab fa-facebook-square mr-2"></i></a>
        <a href="#"><i style="font-size: 1.55em;" class="fab fa-twitter mr-2"></i></a>
        <a href="#"><i style="font-size: 1.55em;color:red;" class="fab fa-youtube mr-2"></i></a>
        <p>Banyak hal diluar sana yang perlu di explore. Berkelilinglah, rasakan keagungan Tuhan.</p>
        <p>2018 &copy; Sipatuo Team, <i class="fas fa-heart"></i> <i class="fas fa-smile"></i></p>
      </div>
    </footer>
<?php
	$multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
	echo assets_js($multiple_js);
?>
</body>
</html>