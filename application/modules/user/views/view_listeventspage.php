<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sipatuo - Urgent</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
	<?php
		$multiple_css = array('all.css','bootstrap.min.css','startpage.css');
		echo assets_css($multiple_css);
	?>
</head>
<body class="bg-light">
	<nav style="" class="navbar shadow-sm navbar-expand-lg fixed-top navbar-dark bg-light">
      <?php $this->load->view('common/navbar_title_admin_view'); ?>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>home"><i class="fas fa-home"></i></a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;" class="nav-link dropdown-toggle" id="userdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url() ?>profil"><i class="far fa-list-alt"></i></a>
            <div style="margin-top:-17px;margin-right:130px;" class="dropdown-menu dropdown-menu-right" aria-labelledby="userdropdown">
              <h6 class="dropdown-header">General</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>campaign"><i class="fas fa-bullhorn mr-2"></i>Semua Campaign</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>events"><i class="far fa-calendar-alt mr-2"></i>Semua Kegiatan</a></span>
              <div class="dropdown-divider"></div>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>pendonor"><i class="fas fa-users mr-2"></i>Semua Pendonor</a></span>
            </div>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>notification"><i class="far fa-bell"></i>
              <label class="bg-danger count-new-notif-1"><strong>4</strong></label>
            </a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>message"><i class="far fa-envelope"></i>
              <label class="bg-danger count-new-msg-1"><strong>9</strong></label>
            </a>
          </li>
          <li class="nav-item active mr-lg-2">
            <a style="font-size:1.3em;color:#636363;" class="nav-link dropdown-toggle" id="userdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url() ?>profil"><i class="far fa-user-circle"></i></a>
            <div style="margin-top:-17px;margin-right:65px;" class="dropdown-menu dropdown-menu-right" aria-labelledby="userdropdown">
              <h6 class="dropdown-header">Personal</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/campaign"><i class="fas fa-bullhorn mr-2"></i>Campaign Anda</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/event"><i class="far fa-calendar-alt mr-2"></i>Kegiatan Anda</a></span>
              <div class="dropdown-divider"></div>
              <h6 class="dropdown-header">Profil</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/password"><i class="fas fa-fingerprint mr-2"></i>Ganti Password</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil"><i class="far fa-user mr-2"></i>Profil</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>signout"><i class="fas fa-power-off mr-2"></i>Keluar</a></span>
            </div>
          </li>
          <li class="nav-item">
            <a style="font-size:1.3em;color:#636363;" class="nav-link" href="<?php echo base_url(); ?>signout"><i class="fas fa-power-off"></i></a>
          </li>
        </ul>
      </div>
  </nav>

    <main role="main" class="container-fluid">
      <div style="min-height:600px;" class="row justify-content-center bg-white">
        
        <div class="col-lg-12">
            <div class="row justify-content-center pt-4">
              <div style="text-align: center;" class="col-lg-12 mt-2 mb-4 d-none d-md-block d-lg-block">
                <p style="color:#c80d0d;text-shadow: 2px 2px 12px white;font-size:2.5em;" class="display-4 mb-3 mt-3"><strong>#your</strong>Events</p>
              </div>
              <div style="text-align: center;" class="col-lg-12 pb-4 d-none d-md-block d-lg-block">
                <a class="text-dark" href="#"><i style="font-size:1.3em;" class="fas fa-search mr-2"></i></a>
                <a class="text-dark" href="#"><i style="font-size:1.3em;" class="fas fa-plus-circle"></i></a>
              </div>
            </div>
          </div>
          <div class="col-lg-9 mb-5 pb-5">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                  <div class="row justify-content-center">
                    <div style="min-height:400px;border:solid 1px #e8e8e8 !important;" class="my-3 p-3 bg-white rounded shadow-sm ml-lg-3">
                      <a style="font-size:0.75em;" href="#" class="float-right">Lihat Semua</a>
                      <h6 class="border-bottom border-gray pb-3 mb-0">Open Event <span style="font-size: 1.2em;" class="badge badge-success">21</span></h6>
                      <div class="media text-dark pt-3">
                        <img data-src="holder.js/32x32?theme=thumb&bg=#018715&fg=#018715&size=1" alt="" class="mr-2 rounded">
                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                          <strong class="d-block text-dark">Butuh : A+ (3 Kantong)</strong>
                          <span style="color:#960000;" class="d-block mb-2">Request by : Yunita Sinegar</span>
                          Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                          
                          <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                          <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-info ml-0 mt-2"><i class="fas fa-pen mr-1"></i> Edit</a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-trash mr-1"></i> Delete</a>
                        </p>
                      </div>
                      <div class="media text-dark pt-3">
                        <img data-src="holder.js/32x32?theme=thumb&bg=#018715&fg=#018715&size=1" alt="" class="mr-2 rounded">
                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                          <strong class="d-block text-dark">Butuh : Trombosit (10 Kantong)</strong>
                          <span style="color:#960000;" class="d-block mb-2">Request by : Ringki datas</span>
                          Donec id elit non mi porta gravida at eget metus.               
                          <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                          <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-info ml-0 mt-2"><i class="fas fa-pen mr-1"></i> Edit</a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-trash mr-1"></i> Delete</a>
                        </p>
                      </div>
                      <div class="media text-dark pt-3">
                        <img data-src="holder.js/32x32?theme=thumb&bg=#018715&fg=#018715&size=1" alt="" class="mr-2 rounded">
                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                          <strong class="d-block text-dark">Butuh : AB+ (10 Kantong)</strong>
                          <span style="color:#960000;" class="d-block mb-2">Request by : Ananda Riswa </span>
                          Donec id elit non mi porta gravida at eget metus.
                          
                          <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                          <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-info ml-0 mt-2"><i class="fas fa-pen mr-1"></i> Edit</a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-trash mr-1"></i> Delete</a>
                        </p>
                      </div>
                      <div class="media text-dark pt-3">
                        <img data-src="holder.js/32x32?theme=thumb&bg=#018715&fg=#018715&size=1" alt="" class="mr-2 rounded">
                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                          <strong class="d-block text-dark">Butuh : B+ (20 Kantong)</strong>
                          <span style="color:#960000;" class="d-block mb-2">Request by : Indrawan Silha</span>
                          Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                          
                          <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                          <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-info ml-0 mt-2"><i class="fas fa-pen mr-1"></i> Edit</a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-trash mr-1"></i> Delete</a>
                        </p>
                      </div>
                      <small class="d-block text-right mt-3 mb-3">
                        <a class="text-dark" href="#">Lihat Kegiatan Aktif Lain</a>
                      </small>
                    </div>
                  </div>
                </div>

                <div class="col-lg-6">
                  <div class="row justify-content-center">
                    <div style="min-height:400px;border:solid 0px #c40303 !important;" class="my-3 p-3 bg-light rounded shadow-sm ml-lg-3">
                      <a style="font-size:0.75em;" href="#" class="float-right text-dark">Lihat Semua</a>
                      <h6 class="border-bottom text-warning border-gray pb-3 mb-0">Complete Events <span style="font-size: 1.2em;" class="badge badge-dark">33</span></h6>
                      <div class="media text-secondary pt-3">
                        <img data-src="holder.js/32x32?theme=thumb&bg=828282&fg=828282&size=1" alt="" class="mr-2 rounded">
                        <p class="media-body text-secondary pb-3 mb-0 small lh-125 border-bottom border-gray">
                          <strong class="d-block text-secondary">Butuh : A+ (3 Kantong)</strong>
                          <span style="color:#a4b4aa;" class="d-block mb-2">Request by : Yunita Sinegar</span>
                          Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                          
                          <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                          <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-pen"></i></a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-trash"></i></a>
                        </p>
                      </div>
                      <div class="media text-secondary pt-3">
                        <img data-src="holder.js/32x32?theme=thumb&bg=828282&fg=828282&size=1" alt="" class="mr-2 rounded">
                        <p class="media-body text-secondary pb-3 mb-0 small lh-125 border-bottom border-gray">
                          <strong class="d-block text-secondary">Butuh : Trombosit (10 Kantong)</strong>
                          <span style="color:#a4b4aa;" class="d-block mb-2">Request by : Ringki datas</span>
                          Donec id elit non mi porta gravida at eget metus.               
                          <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                          <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-pen"></i></a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-trash"></i></a>
                        </p>

                      </div>
                      <div class="media text-secondary pt-3">
                        <img data-src="holder.js/32x32?theme=thumb&bg=828282&fg=828282&size=1" alt="" class="mr-2 rounded">
                        <p class="media-body text-secondary pb-3 mb-0 small lh-125 border-bottom border-gray">
                          <strong class="d-block text-secondary">Butuh : AB+ (10 Kantong)</strong>
                          <span style="color:#a4b4aa;" class="d-block mb-2">Request by : Ananda Riswa </span>
                          Donec id elit non mi porta gravida at eget metus.
                          
                          <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                          <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-pen"></i></a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-trash"></i></a>
                        </p>
                      </div>
                      <div class="media text-secondary pt-3">
                        <img data-src="holder.js/32x32?theme=thumb&bg=828282&fg=828282&size=1" alt="" class="mr-2 rounded">
                        <p class="media-body text-secondary pb-3 mb-0 small lh-125 border-bottom border-gray">
                          <strong class="d-block text-secondary">Butuh : B+ (20 Kantong)</strong>
                          <span style="color:#a4b4aa;" class="d-block mb-2">Request by : Indrawan Silha</span>
                          Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                          
                          <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                          <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/edit/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-pen"></i></a>
                          <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/delete/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-trash"></i></a>
                        </p>
                      </div>
                      <small class="d-block text-right mt-3 mb-3">
                        <a class="text-dark" href="#">Lihat Campaign Complete lain</a>
                      </small>
                    </div>
                  </div>
                </div>
            </div>
          </div>

      </div>
    </main>

    <?php $this->load->view('common/footer_view'); ?>
    
<?php
	$multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
	echo assets_js($multiple_js);
?>
</body>
</html>