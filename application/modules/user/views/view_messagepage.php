<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sipatuo - Hadir membantu anda.</title>
    <link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
    <?php
      $multiple_css = array('all.css','bootstrap.min.css','startpage.css');
      echo assets_css($multiple_css);
    ?>
  </head>
  <body class="bg-light">
    <nav style="" class="navbar shadow-sm navbar-expand-lg fixed-top navbar-dark bg-light">
      <?php $this->load->view('common/navbar_title_admin_view'); ?>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>home"><i class="fas fa-home"></i></a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;" class="nav-link dropdown-toggle" id="userdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url() ?>profil"><i class="far fa-list-alt"></i></a>
            <div style="margin-top:-17px;margin-right:130px;" class="dropdown-menu dropdown-menu-right" aria-labelledby="userdropdown">
              <h6 class="dropdown-header">General</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>campaign"><i class="fas fa-bullhorn mr-2"></i>Semua Campaign</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>events"><i class="far fa-calendar-alt mr-2"></i>Semua Kegiatan</a></span>
              <div class="dropdown-divider"></div>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>pendonor"><i class="fas fa-users mr-2"></i>Semua Pendonor</a></span>
            </div>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>notification"><i class="far fa-bell"></i>
              <label class="bg-danger count-new-notif-1"><strong>4</strong></label>
            </a>
          </li>
          <li class="nav-item active mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>message"><i class="far fa-envelope"></i>
              <label class="bg-danger count-new-msg-1"><strong>9</strong></label>
            </a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;" class="nav-link dropdown-toggle" id="userdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url() ?>profil"><i class="far fa-user-circle"></i></a>
            <div style="margin-top:-17px;margin-right:65px;" class="dropdown-menu dropdown-menu-right" aria-labelledby="userdropdown">
              <h6 class="dropdown-header">Personal</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/campaign"><i class="fas fa-bullhorn mr-2"></i>Campaign Anda</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/event"><i class="far fa-calendar-alt mr-2"></i>Kegiatan Anda</a></span>
              <div class="dropdown-divider"></div>
              <h6 class="dropdown-header">Profil</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/password"><i class="fas fa-fingerprint mr-2"></i>Ganti Password</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil"><i class="far fa-user mr-2"></i>Profil</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>signout"><i class="fas fa-power-off mr-2"></i>Keluar</a></span>
            </div>
          </li>
          <li class="nav-item">
            <a style="font-size:1.3em;color:#636363;" class="nav-link" href="<?php echo base_url(); ?>signout"><i class="fas fa-power-off"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    

    <main role="main" class="container-fluid">
      
      <div style="min-height:800px;" class="row justify-content-center">

        <div class="col-lg-7">
          <div class="row justify-content-center pt-4">
            <div style="text-align: center;" class="col-lg-12 d-none d-md-block d-lg-block">
              <p style="color:#c80d0d;text-shadow: 2px 2px 12px white;font-size:2.5em;" class="display-4 mb-3 mt-3"><strong>#your</strong>Messages</p>
            </div>
            <div class="col-lg-12">
              <div class="my-3 p-3 bg-white rounded shadow-sm">
                <a style="font-size:0.75em;" href="#" class="float-right">Mark all as read</a>
                <h6 class="border-bottom border-gray pb-2 mb-0">Messages List</h6>
                <a href="#" style="padding: 0px 0px 0px 2rem;" class="list-group-item list-group-item-action flex-column align-items-start unread">
                  <div class="media text-muted pt-3">
                    <i style="font-size: 1.5em;" class="fas fa-envelope mr-2"></i>
                    <span style="position: absolute;top:5px;left:7px;font-size:1em;"><label class="badge badge-pill badge-success">1.5k</label></span>
                    <p class="media-body pb-3 mb-0 small lh-125 border-gray">
                      <span style="position: absolute;top:10px;right:10px;font-size: 0.85em;">23 minutes ago</span>
                      <strong class="d-block text-gray-dark">Ahmad Imron</strong>
                      Pesan : Siapa yang suka pada kerinduan yang mendalam, sebagai hasil dari...
                    </p>
                  </div>
                </a>
                <a href="#" style="padding: 0px 0px 0px 2rem;" class="list-group-item list-group-item-action flex-column align-items-start unread">
                  <div class="media text-muted pt-3">
                    <i style="font-size: 1.5em;" class="fas fa-envelope mr-2"></i>
                    <span style="position: absolute;top:5px;left:7px;font-size:1em;"><label class="badge badge-pill badge-success">10</label></span>
                    <p class="media-body pb-3 mb-0 small lh-125 border-gray">
                      <span style="position: absolute;top:10px;right:10px;font-size: 0.85em;">33 hours ago</span>
                      <strong class="d-block text-gray-dark">Suryadarmadi</strong>
                      Pesan : Ingat untuk datang ke rumah
                    </p>
                  </div>
                </a>
                <a href="#" style="padding: 0px 0px 0px 2rem;" class="list-group-item list-group-item-action flex-column align-items-start">
                  <div class="media text-muted pt-3">
                    <i style="font-size: 1.5em;" class="far fa-envelope-open mr-3"></i>
                    <p class="media-body pb-3 mb-0 small lh-125 border-gray">
                      <span style="position: absolute;top:10px;right:10px;font-size: 0.85em;">33 hours ago</span>
                      <strong class="d-block text-gray-dark">jjokwidod</strong>
                      Pesan : Hanya untuk tes data yang masuk atau belum sodara lama
                    </p>
                  </div>
                </a>
                <a href="#" style="padding: 0px 0px 0px 2rem;" class="list-group-item list-group-item-action flex-column align-items-start">
                  <div class="media text-muted pt-3">
                    <i style="font-size: 1.5em;" class="far fa-envelope-open mr-3"></i>
                    <p class="media-body pb-3 mb-0 small lh-125 border-gray">
                      <span style="position: absolute;top:10px;right:10px;font-size: 0.85em;">33 hours ago</span>
                      <strong class="d-block text-gray-dark">santodagi</strong>
                      Pesan : Ping
                    </p>
                  </div>
                </a>
                <a href="#" style="padding: 0px 0px 0px 2rem;" class="list-group-item list-group-item-action flex-column align-items-start">
                  <div class="media text-muted pt-3">
                    <i style="font-size: 1.5em;" class="far fa-envelope-open mr-3"></i>
                    <p class="media-body pb-3 mb-0 small lh-125 border-gray">
                      <span style="position: absolute;top:10px;right:10px;font-size: 0.85em;">33 hours ago</span>
                      <strong class="d-block text-gray-dark">Wahyudi</strong>
                      Pesan : Soto ?
                    </p>
                  </div>
                </a>
                <small class="d-block text-right mt-3">
                  <a href="#">Lihat lainnya</a>
                </small>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      
      <?php $this->load->view('common/invitation_view'); ?>
    </main>

    <?php $this->load->view('common/footer_view'); ?>

    <?php
      $multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
      echo assets_js($multiple_js);
    ?>
  </body>
</html>