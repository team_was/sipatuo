<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sipatuo - Hadir membantu anda.</title>
    <link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
    <?php
      $multiple_css = array('all.css','bootstrap.min.css','startpage.css');
      echo assets_css($multiple_css);
    ?>
  </head>
  <body class="bg-light">
    <nav style="" class="navbar shadow-sm navbar-expand-lg fixed-top navbar-dark bg-light">
      <a style="font-size:1.55em;" class="navbar-brand ml-lg-3 mr-lg-3" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/Guard-Red.png" width="50px"> <span style="color:#636363;">SIPATUO</span></a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
        <!-- <i style="color:#f2f2f2;margin-right:7px;" class="fas fa-ellipsis-v"></i> -->
      </button>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>home"><i class="fas fa-home"></i></a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="#"><i class="far fa-bell"></i>
              <!-- <label style="position:absolute;top:23px;right:350px;color:white;font-size: 0.52em;" class="badge badge-pill bg-danger">10</label> -->
            </a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="#"><i class="far fa-envelope"></i>
              <!-- <label style="position:absolute;top:23px;right:290px;color:white;font-size: 0.52em;" class="badge badge-pill bg-danger">10</label> -->
            </a>
          </li>
          <!-- <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="#"><i class="fas fa-bullhorn"></i>
              <label style="position:absolute;top:17px;right:130px;color:white;" class="badge badge-pill bg-dark">2</label>
            </a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="#"><i class="far fa-calendar-alt"></i>
              <label style="position:absolute;top:23px;right:233px;color:white;font-size: 0.52em;" class="badge badge-pill bg-danger">9</label>
            </a>
          </li> -->
          <!-- <li class="nav-item mr-lg-4">
            <a style="font-size:;color:#636363;" class="nav-link" href="<?php echo base_url(); ?>notifikasi">Notifikasi
              <span style="position:absolute;top:16px;right:600px;color:white;font-size: 0.67em;" class="badge badge-pill bg-dark">3</span>
            </a>
          </li> -->
          <!-- <li class="nav-item mr-lg-4">
            <a style="font-size:;color:#636363;" class="nav-link" href="<?php echo base_url(); ?>pesan">Pesan</a>
            <span style="position:absolute;top:16px;right:510px;color:white;font-size: 0.67em;" class="badge badge-pill bg-dark">99+</span>
          </li>
          <li class="nav-item mr-lg-4">
            <a style="font-size:;color:#636363;" class="nav-link" href="<?php echo base_url(); ?>campaign">Campaign</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a style="font-size:;color:#636363;" class="nav-link" href="<?php echo base_url(); ?>events">Kegiatan</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a style="font-size:;color:#636363;" class="nav-link" href="<?php echo base_url(); ?>pendonor">Pendonor</a>
          </li> -->
          <!-- <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;" class="nav-link" href="<?php echo base_url(); ?>users"><i class="fas fa-users"></i></a>
          </li> -->
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;" class="nav-link dropdown-toggle" id="userdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url() ?>profil"><i class="far fa-list-alt"></i></a>
            <div style="margin-top:-17px;margin-right:130px;" class="dropdown-menu dropdown-menu-right" aria-labelledby="userdropdown">
              <h6 class="dropdown-header">General</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="#"><i class="fas fa-bullhorn mr-2"></i>Semua Campaign</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="#"><i class="far fa-calendar-alt mr-2"></i>Semua Kegiatan</a></span>
              <div class="dropdown-divider"></div>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="#"><i class="fas fa-users mr-2"></i>Semua Pendonor</a></span>
              <!-- <div class="dropdown-divider"></div> -->
              <!-- <h6 class="dropdown-header">Profil</h6> -->
              <!-- <span><a style="font-size:0.88em;" class="dropdown-item" href="#"><i class="fas fa-fingerprint mr-2"></i>Ganti Password</a></span> -->
              <!-- <span><a style="font-size:0.88em;" class="dropdown-item" href="#"><i class="far fa-user mr-2"></i>Profil</a></span> -->
              <!-- <span><a style="font-size:0.88em;" class="dropdown-item" href="#"><i class="fas fa-power-off mr-2"></i>Keluar</a></span> -->
            </div>
          </li>
          <li class="nav-item active mr-lg-2">
            <a style="font-size:1.3em;color:#636363;" class="nav-link dropdown-toggle" id="userdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url() ?>profil"><i class="far fa-user-circle"></i></a>
            <div style="margin-top:-17px;margin-right:65px;" class="dropdown-menu dropdown-menu-right" aria-labelledby="userdropdown">
              <h6 class="dropdown-header">Personal</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="#"><i class="fas fa-bullhorn mr-2"></i>Campaign Anda</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="#"><i class="far fa-calendar-alt mr-2"></i>Kegiatan Anda</a></span>
              <div class="dropdown-divider"></div>
              <h6 class="dropdown-header">Profil</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="#"><i class="fas fa-fingerprint mr-2"></i>Ganti Password</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="#"><i class="far fa-user mr-2"></i>Profil</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="#"><i class="fas fa-power-off mr-2"></i>Keluar</a></span>
            </div>
          </li>

          <!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-dark" href="https://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Settings</a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li> -->

          <li class="nav-item">
            <a style="font-size:1.3em;color:#636363;" class="nav-link" href="<?php echo base_url(); ?>signout"><i class="fas fa-power-off"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    

    <main role="main" class="container-fluid">
      
      <div style="min-height:800px;" class="row">

        <div class="col-lg-4 bg-white">
          <div style="text-align: center;" class="col-lg-12 d-none d-md-block d-lg-block">
            <!-- <p style="color:white;text-shadow: 2px 2px 12px #c80d0d;font-size: 1.55em;" class="display-4 mb-5 mt-5 pt-3"><strong>#kebutuhan</strong>Lainnya</p> -->
          </div>

          <div class="my-3 p-3 rounded mt-5 bg-white">
            <div class="tabs pl-0 pr-0">
              <div class="tab-button-outer">
                <ul id="tab-button">
                  <li><a href="#tab01">Campaign Anda</a></li>
                  <li><a href="#tab02">Kegiatan Anda</a></li>
                  <!-- <li><a href="#tab03">Tab 3</a></li>
                  <li><a href="#tab04">Tab 4</a></li>
                  <li><a href="#tab05">Tab 5</a></li> -->
                </ul>
              </div>
              <div class="tab-select-outer">
                <select id="tab-select">
                  <li><a href="#tab01">Campaign Anda</a></li>
                  <li><a href="#tab02">Kegiatan Anda</a></li>
                  <!-- <option value="#tab03">Tab 3</option>
                  <option value="#tab04">Tab 4</option>
                  <option value="#tab05">Tab 5</option> -->
                </select>
              </div>

              <div id="tab01" class="tab-contents pt-4 p-0">
                <!-- <h2>Campaign Anda</h2> -->
                
                <a style="font-size:0.75em;" href="<?php echo base_url(); ?>profil/campaign" class="float-right">Lihat Semua Campaign Anda</a>
                <h6 class="border-bottom border-gray pb-2 mb-0">Campaign Anda</h6>

                <div class="media text-black pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Butuh : Trombosit (10 Kantong)</strong>
                    <span style="color:#e8e8e8;" class="d-block text-secondary mb-2">Request by : Ringki datas ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                    "Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih."
                    <span style="font-size:0.95em;" class="d-block text-secondary pt-2">Lokasi : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-secondary">Deadline : 24 Juli 2018 (Status : Completed)</span>
                    <span style="font-size:0.95em;" class="d-block text-secondary pt-2"><i class="fas fa-child"></i> 15</span>
                  </p>
                </div>
                <div class="media text-white pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=0ccadb&fg=0ccadb&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Wahyudi</strong>
                    <span class="d-block text-secondary mb-2">Goldar : O Rhesus + ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                    Motto / Bio : Jangan pernah berhenti berbuat baik kepada sesama manusia dan alam semesta.
                    <span style="font-size:0.95em;" class="d-block text-dark pt-2">Alamat : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-dark">Terakhir Online : 2 Tahun yang lalu</span>
                  </p>
                </div>
                <div class="media text-white pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=6f42c1&fg=6f42c1&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Handiko Prasetri Mandire</strong>
                    <span class="d-block text-secondary mb-2">Goldar : AB ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                    Motto / Bio : Jangan pernah berhenti berbuat baik kepada sesama manusia dan alam semesta.
                    <span style="font-size:0.95em;" class="d-block text-dark pt-2">Alamat : Kamlimantan Selatan</span>
                    <span style="font-size:0.95em;" class="d-block text-dark">Terakhir Online : 2 Tahun yang lalu</span>
                  </p>
                </div>
                <div class="media text-white pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=dbbb0c&fg=dbbb0c&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Rangga sentanu pergunibo</strong>
                    <span class="d-block text-secondary mb-2">Goldar : A ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                    Motto / Bio : Bahagia itu sederhana.
                    <span style="font-size:0.95em;" class="d-block text-dark pt-2">Alamat : Jakarta Pusat</span>
                    <span style="font-size:0.95em;" class="d-block text-dark">Terakhir Online : 1 Tahun yang lalu</span>
                  </p>
                </div>
                <div class="media text-white pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=aad005&fg=aad005&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Relangga Persutopo Asdio</strong>
                    <span class="d-block text-secondary mb-2">Goldar : A ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                    Motto / Bio : Pendukung Kebaikan.
                    <span style="font-size:0.95em;" class="d-block text-dark pt-2">Alamat : Sumatra Selatan</span>
                    <span style="font-size:0.95em;" class="d-block text-dark">Terakhir Online : 2 Tahun yang lalu</span>
                  </p>
                </div>
                <small class="d-block text-right mt-3 mb-3">
                  <a class="text-danger" href="<?php echo base_url(); ?>profil/campaign">Lihat Semua Campaign Anda</a>
                </small>


              </div>
              <div id="tab02" class="tab-contents pt-4 p-0">
                <!-- <h2>Kegiatan Anda</h2> -->
                <a style="font-size:0.75em;" href="<?php echo base_url(); ?>profil/event" class="float-right">Lihat Semua Kegiatan Anda</a>
                <h6 class="border-bottom border-gray pb-2 mb-0">Kegiatan Anda</h6>

                <div class="media text-black pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Bersama menyelamatkan sesama manusia</strong>
                    <span style="color:#e8e8e8;" class="d-block text-secondary mb-2">Request by : Ringki datas ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                    "Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih."
                    <span style="font-size:0.95em;" class="d-block text-secondary pt-2">Lokasi : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-secondary">Deadline : 24 Juli 2018 (Status : Completed)</span>
                    <span style="font-size:0.95em;" class="d-block text-secondary pt-2"><i class="far fa-eye"></i> 15 <i class="ml-3 fas fa-child"></i> 15</span>
                  </p>
                </div>
                <div class="media text-white pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=0ccadb&fg=0ccadb&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Wahyudi</strong>
                    <span class="d-block text-secondary mb-2">Goldar : O Rhesus + ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                    Motto / Bio : Jangan pernah berhenti berbuat baik kepada sesama manusia dan alam semesta.
                    <span style="font-size:0.95em;" class="d-block text-dark pt-2">Alamat : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-dark">Terakhir Online : 2 Tahun yang lalu</span>
                  </p>
                </div>
                <div class="media text-white pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=6f42c1&fg=6f42c1&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Handiko Prasetri Mandire</strong>
                    <span class="d-block text-secondary mb-2">Goldar : AB ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                    Motto / Bio : Jangan pernah berhenti berbuat baik kepada sesama manusia dan alam semesta.
                    <span style="font-size:0.95em;" class="d-block text-dark pt-2">Alamat : Kamlimantan Selatan</span>
                    <span style="font-size:0.95em;" class="d-block text-dark">Terakhir Online : 2 Tahun yang lalu</span>
                  </p>
                </div>
                <div class="media text-white pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=dbbb0c&fg=dbbb0c&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Rangga sentanu pergunibo</strong>
                    <span class="d-block text-secondary mb-2">Goldar : A ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                    Motto / Bio : Bahagia itu sederhana.
                    <span style="font-size:0.95em;" class="d-block text-dark pt-2">Alamat : Jakarta Pusat</span>
                    <span style="font-size:0.95em;" class="d-block text-dark">Terakhir Online : 1 Tahun yang lalu</span>
                  </p>
                </div>
                <div class="media text-white pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=aad005&fg=aad005&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Relangga Persutopo Asdio</strong>
                    <span class="d-block text-secondary mb-2">Goldar : A ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                    Motto / Bio : Pendukung Kebaikan.
                    <span style="font-size:0.95em;" class="d-block text-dark pt-2">Alamat : Sumatra Selatan</span>
                    <span style="font-size:0.95em;" class="d-block text-dark">Terakhir Online : 2 Tahun yang lalu</span>
                  </p>
                </div>
                <small class="d-block text-right mt-3 mb-3">
                  <a class="text-white" href="<?php echo base_url(); ?>profil/event">Lihat Semua Kegiatan Anda</a>
                </small>
              </div>
              
            </div>



            
          </div>
        </div>

        <div class="col-lg-8 pb-3 bg-light">
          <div class="col-lg-12 pt-5">
            <p style="color:#c80d0d;text-shadow: 2px 2px 12px white;font-size: 2.33em;" class="display-4 mb-4 mt-0"><strong>#profil</strong>Anda</p>
          </div>
          <div class="col-lg-12">
            <div class="row justify-content-center">
              
              <div class="col-lg-4 pb-3 mr-2">
                <div class="row">
                  <div style="min-height:200px;" class="col-lg-12 p-2 bg-white">
                    <label style="position: absolute;top:7;left:7;font-size: 0.60em;" class="btn btn-dark"> <i class="fas fa-camera mr-1"></i> Change Photo<input type="file" style="display: none;"></label>
                    <img class="img-fluid" src="<?php echo base_url(); ?>assets/img/user-3.png">
                  </div>
                  <div style="word-wrap: break-word;" class="col-lg-12 bg-primary">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Bio / Motto :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">Bahagia itu sederhana, sesederhana perasaan ini yang mudah jatuh karenamu.
                  </p>
                  </div>
                  
                  <!-- <div class="col-lg-12 pl-2 pt-3">
                    <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-line text-success"></i> line_id</span>
                    <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-facebook-square text-primary"></i> facebook_id</span>
                    <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-blackberry text-danger"></i> BBM_id</span>
                    <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-whatsapp"></i> whatsapp_no</span>
                    <span class="btn btn-outline-dark btn-sm mb-2">#donordarah</span>
                    <span class="btn btn-outline-dark btn-sm mb-2">#kegiatandonor</span>
                  </div> -->
                </div>
              </div>
              <div style="min-height:500px;" class="col-lg-5 pb-3">
                <div class="row">
                  <div class="col-lg-12 bg-white">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:black;">Nama :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3"><strong>Suryadaramadi Sadar.</strong></p>
                  </div>
                  <div class="col-lg-12 bg-warning">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Golongan Darah :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">A Rhesus + (Positif)</p>
                  </div>
                  <div class="col-lg-12 bg-info">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Lokasi :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">Indonesia, Sulawesi Selatan, Makassar</p>
                  </div>
                  <div class="col-lg-12 bg-danger">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Email :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">suryadarmadi@gmail.com</p>
                  </div>
                  <div class="col-lg-12 bg-secondary">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Phone :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">081 111 111 111</p>
                  </div>
                  <div style="background: #ffb5b5;" class="col-lg-12">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Status :</span>
                    <p style="font-size: 0.88em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">
                      Bergabung pada 12 Juli 2018. <br/>
                      Terakhir online 3 Menit yang lalu.
                    </p>
                  </div>
                  <div style="" class="col-lg-12 bg-white">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:black;">Sosial Media :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead p-2 pb-3">
                      <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-line text-success"></i> line_id</span>
                      <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-facebook-square text-primary"></i> facebook_id</span>
                      <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-blackberry text-danger"></i> BBM_id</span>
                      <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-whatsapp"></i> whatsapp_no</span>
                      <span class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-telegram"></i> telegram_id</span>
                    </p>
                  </div>
                  <div class="col-lg-12 pt-3 pl-0">
                    <a href="#" class="btn btn-warning btn-sm pl-3 pr-3 p-2"><i class="fas fa-pencil-alt mr-2"></i> Edit Profil</a>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
        
      </div>
      
      <div style="background:white;height:300px;" class="row justify-content-center">
        <div style="text-align: center;height:30px;" class="col-lg-12 mt-5">
          <h1 style="font-size:2em;color:black;" class="display-4"><strong>Undang </strong>teman anda</h1>
          <h1 style="font-size:1em;color:#153b77;" class="display-4">Undang teman anda untuk bergabung bersama sipatuo, semakin banyak yang bergabung semakin banyak kehidupan yang bisa kita selamatkan.</h1>
        </div>
        <div style="text-align: center;" class="col-lg-5">
          <form>
            <div class="form-row align-items-center">
              <div class="col-sm-9 my-1">
                <label class="sr-only" for="kontak">Kontak</label>
                <input type="text" class="form-control form-control-sm" id="kontak" placeholder="Masukkan email atau nomor telepon teman anda">
              </div>
              <div class="col-auto my-1">
                <button type="submit" class="btn btn-primary btn-sm">Send Invitation</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </main>

    <footer style="margin-top:50px;padding-top: 11px;" class="text-muted">
      <div class="container">
        <img class="float-right" width="50px" src="<?php echo base_url(); ?>assets/img/Guard-Dark.png">
        <!-- <p class="float-right">
          <a href="#"><i class="fas fa-tint"></i></a>
        </p> -->
        <a href="#"><i style="font-size: 1.55em;color:#174a9b;" class="fab fa-facebook-square mr-2"></i></a>
        <a href="#"><i style="font-size: 1.55em;" class="fab fa-twitter mr-2"></i></a>
        <a href="#"><i style="font-size: 1.55em;color:red;" class="fab fa-youtube mr-2"></i></a>
        <p>Banyak hal diluar sana yang perlu di explore. Berkelilinglah, rasakan keagungan Tuhan.</p>
        <p>2018 &copy; Sipatuo Team, <i class="fas fa-heart"></i> <i class="fas fa-smile"></i></p>
      </div>
    </footer>

    <?php
      $multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
      echo assets_js($multiple_js);
    ?>
  </body>
</html>