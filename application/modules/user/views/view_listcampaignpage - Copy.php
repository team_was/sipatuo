<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sipatuo - Urgent</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
	<?php
		$multiple_css = array('all.css','bootstrap.min.css','startpage.css');
		echo assets_css($multiple_css);
	?>
</head>
<body class="bg-light">
	<nav style="" class="navbar shadow-sm navbar-expand-lg fixed-top navbar-dark bg-light">
      <a style="font-size:1.55em;" class="navbar-brand ml-lg-3 mr-lg-3" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/Guard-Red.png" class="sip-logo"> <span style="color:#636363;">SIPATUO</span></a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
        <!-- <i style="color:#f2f2f2;margin-right:7px;" class="fas fa-ellipsis-v"></i> -->
      </button>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>home"><i class="fas fa-home"></i></a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;" class="nav-link dropdown-toggle" id="userdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url() ?>profil"><i class="far fa-list-alt"></i></a>
            <div style="margin-top:-17px;margin-right:130px;" class="dropdown-menu dropdown-menu-right" aria-labelledby="userdropdown">
              <h6 class="dropdown-header">General</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>campaign"><i class="fas fa-bullhorn mr-2"></i>Semua Campaign</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>events"><i class="far fa-calendar-alt mr-2"></i>Semua Kegiatan</a></span>
              <div class="dropdown-divider"></div>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>pendonor"><i class="fas fa-users mr-2"></i>Semua Pendonor</a></span>
            </div>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>notification"><i class="far fa-bell"></i>
              <label class="bg-danger count-new-notif-1"><strong>4</strong></label>
            </a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>message"><i class="far fa-envelope"></i>
              <label class="bg-danger count-new-msg-1"><strong>9</strong></label>
            </a>
          </li>
          <li class="nav-item active mr-lg-2">
            <a style="font-size:1.3em;color:#636363;" class="nav-link dropdown-toggle" id="userdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url() ?>profil"><i class="far fa-user-circle"></i></a>
            <div style="margin-top:-17px;margin-right:65px;" class="dropdown-menu dropdown-menu-right" aria-labelledby="userdropdown">
              <h6 class="dropdown-header">Personal</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/campaign"><i class="fas fa-bullhorn mr-2"></i>Campaign Anda</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/event"><i class="far fa-calendar-alt mr-2"></i>Kegiatan Anda</a></span>
              <div class="dropdown-divider"></div>
              <h6 class="dropdown-header">Profil</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/password"><i class="fas fa-fingerprint mr-2"></i>Ganti Password</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil"><i class="far fa-user mr-2"></i>Profil</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>signout"><i class="fas fa-power-off mr-2"></i>Keluar</a></span>
            </div>
          </li>
          <li class="nav-item">
            <a style="font-size:1.3em;color:#636363;" class="nav-link" href="<?php echo base_url(); ?>signout"><i class="fas fa-power-off"></i></a>
          </li>
        </ul>
      </div>
  </nav>

    <main role="main" class="container-fluid">
      <div style="min-height:600px;" class="row justify-content-center bg-white">
        <!-- <div style="position: fixed;bottom:10px;right:45px;">
          <a href="<?php echo base_url(); ?>events/create">NEW</a>
        </div> -->
        <div class="col-lg-9">
          <div class="row justify-content-center pt-4">
            <div style="text-align: center;" class="col-lg-12 mt-2 mb-4 d-none d-md-block d-lg-block">
              <p style="color:#c80d0d;text-shadow: 2px 2px 12px white;font-size:2.5em;" class="display-4 mb-3 mt-3"><strong>#your</strong>Campaigns</p>
            </div>
            <div style="text-align: center;" class="col-lg-12 pb-5">
              <button style="border-radius:0px;border:solid 1px #ffe2e2;box-shadow: 3px 3px 2px #ffe2e2;" type="button" class="btn btn-outline-secondary mr-2"><i class="fas fa-search"></i></button>
              <a href="<?php echo base_url(); ?>campaign/create" style="border-radius:0px;border:solid 1px #ffe2e2;box-shadow: 3px 3px 2px #ffe2e2;" class="btn btn-outline-secondary"><i class="fas fa-plus-circle"></i></a>
            </div>
          <div class="col-lg-4 col-md-4 mb-4 pb-1 d-none d-md-block d-lg-block">
            <div class="box-urgent-me-active">
              <label class="display-4 urgent-blood">A+</label><br/>
              <label class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong></label>
              <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih."</p>
              <p class="urgent-user">Yuanita Siregar</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
              <span class="urgent-deadline d-md-none d-lg-block">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>

              <div style="text-align:right;" class="d-flex justify-content-end d-md-none d-lg-block">
                <!-- <button type="button" class="btn btn-outline-dark btn-sm urgent-info ml-1"><i class="fas fa-map-marker-alt mr-1"></i> Kalimantan Selatan</button> -->
                <a href="<?php echo base_url(); ?>urgent/edit/ff1234512" class="btn btn-outline-success btn-sm urgent-info ml-1"><i style="color:black;" class="fas fa-pencil-alt"></i>
                </a>
                <a href="<?php echo base_url(); ?>urgent/delete/ff1234512" class="btn btn-outline-danger btn-sm urgent-info ml-1"><i class="far fa-trash-alt"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mb-4 pb-1 d-none d-md-block d-lg-block">
            <div class="box-urgent-me-active">
              <label class="display-4 urgent-blood">A+</label><br/>
              <label class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong></label>
              <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih."</p>
              <p class="urgent-user">Yuanita Siregar</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
              <span class="urgent-deadline d-md-none d-lg-block">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>

              <div style="text-align:right;" class="d-flex justify-content-end d-md-none d-lg-block">
                <!-- <button type="button" class="btn btn-outline-dark btn-sm urgent-info ml-1"><i class="fas fa-map-marker-alt mr-1"></i> Kalimantan Selatan</button> -->
                <a href="<?php echo base_url(); ?>urgent/edit/ff1234512" class="btn btn-outline-success btn-sm urgent-info ml-1"><i style="color:black;" class="fas fa-pencil-alt"></i>
                </a>
                <a href="<?php echo base_url(); ?>urgent/delete/ff1234512" class="btn btn-outline-danger btn-sm urgent-info ml-1"><i class="far fa-trash-alt"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mb-4 pb-1 d-none d-md-block d-lg-block">
            <div class="box-urgent-me">
              <label class="display-4 urgent-blood">A+</label><br/>
              <label class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong></label>
              <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih."</p>
              <p class="urgent-user">Yuanita Siregar</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
              <span class="urgent-deadline d-md-none d-lg-block">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>

              <div style="text-align:right;" class="d-flex justify-content-end d-md-none d-lg-block">
                <!-- <button type="button" class="btn btn-outline-dark btn-sm urgent-info ml-1"><i class="fas fa-map-marker-alt mr-1"></i> Kalimantan Selatan</button> -->
                <a href="<?php echo base_url(); ?>urgent/edit/ff1234512" class="btn btn-outline-success btn-sm urgent-info ml-1"><i style="color:black;" class="fas fa-pencil-alt"></i>
                </a>
                <a href="<?php echo base_url(); ?>urgent/delete/ff1234512" class="btn btn-outline-danger btn-sm urgent-info ml-1"><i class="far fa-trash-alt"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mb-4 pb-1 d-none d-md-block d-lg-block">
            <div class="box-urgent-me">
              <label class="display-4 urgent-blood">A+</label><br/>
              <label class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong></label>
              <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih."</p>
              <p class="urgent-user">Yuanita Siregar</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
              <span class="urgent-deadline d-md-none d-lg-block">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>

              <div style="text-align:right;" class="d-flex justify-content-end d-md-none d-lg-block">
                <!-- <button type="button" class="btn btn-outline-dark btn-sm urgent-info ml-1"><i class="fas fa-map-marker-alt mr-1"></i> Kalimantan Selatan</button> -->
                <a href="<?php echo base_url(); ?>urgent/edit/ff1234512" class="btn btn-outline-success btn-sm urgent-info ml-1"><i style="color:black;" class="fas fa-pencil-alt"></i>
                </a>
                <a href="<?php echo base_url(); ?>urgent/delete/ff1234512" class="btn btn-outline-danger btn-sm urgent-info ml-1"><i class="far fa-trash-alt"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mb-4 pb-1 d-none d-md-block d-lg-block">
            <div class="box-urgent-me">
              <label class="display-4 urgent-blood">A+</label><br/>
              <label class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong></label>
              <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih."</p>
              <p class="urgent-user">Yuanita Siregar</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
              <span class="urgent-deadline d-md-none d-lg-block">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>

              <div style="text-align:right;" class="d-flex justify-content-end d-md-none d-lg-block">
                <!-- <button type="button" class="btn btn-outline-dark btn-sm urgent-info ml-1"><i class="fas fa-map-marker-alt mr-1"></i> Kalimantan Selatan</button> -->
                <a href="<?php echo base_url(); ?>urgent/edit/ff1234512" class="btn btn-outline-success btn-sm urgent-info ml-1"><i style="color:black;" class="fas fa-pencil-alt"></i>
                </a>
                <a href="<?php echo base_url(); ?>urgent/delete/ff1234512" class="btn btn-outline-danger btn-sm urgent-info ml-1"><i class="far fa-trash-alt"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mb-4 pb-1 d-none d-md-block d-lg-block">
            <div class="box-urgent-me">
              <label class="display-4 urgent-blood">Trombosit</label><br/>
              <label class="display-4 mb-3 urgent-bag"><strong>6 Kantong</strong></label>
              <p class="urgent-desc">"Butuh segera untuk penanganan darurat om saya."</p>
              <p class="urgent-user">Liliana Nana</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Makassar</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
              <span class="urgent-deadline d-md-none d-lg-block">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 menit yang lalu</span>
              <div style="text-align:right;" class="d-flex justify-content-end d-md-none d-lg-block">
                <!-- <button type="button" class="btn btn-outline-dark btn-sm urgent-info ml-1"><i class="fas fa-map-marker-alt mr-1"></i> Makassar</button> -->
                <a href="<?php echo base_url(); ?>urgent/edit/ff1234512" class="btn btn-outline-success btn-sm urgent-info ml-1"><i style="color:black;" class="fas fa-pencil-alt"></i>
                </a>
                <a href="<?php echo base_url(); ?>urgent/delete/ff1234512" class="btn btn-outline-danger btn-sm urgent-info ml-1"><i class="far fa-trash-alt"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mb-4 pb-1 d-none d-md-block d-lg-block">
            <div class="box-urgent-me">
              <label class="display-4 urgent-blood">Trombosit</label><br/>
              <label class="display-4 mb-3 urgent-bag"><strong>6 Kantong</strong></label>
              <p class="urgent-desc">"Butuh segera untuk penanganan darurat om saya."</p>
              <p class="urgent-user">Liliana Nana</p>
              <span class="urgent-deadline d-md-none d-lg-block">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 menit yang lalu</span>
              <div style="text-align:right;" class="d-flex justify-content-end d-md-none d-lg-block">
                <!-- <button type="button" class="btn btn-outline-dark btn-sm urgent-info ml-1"><i class="fas fa-map-marker-alt mr-1"></i> Makassar</button> -->
                <a href="<?php echo base_url(); ?>urgent/edit/ff1234512" class="btn btn-outline-success btn-sm urgent-info ml-1"><i style="color:black;" class="fas fa-pencil-alt"></i>
                </a>
                <a href="<?php echo base_url(); ?>urgent/delete/ff1234512" class="btn btn-outline-danger btn-sm urgent-info ml-1"><i class="far fa-trash-alt"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mb-4 pb-1 d-none d-md-block d-lg-block">
            <div class="box-urgent-me">
              <label class="display-4 urgent-blood">Trombosit</label><br/>
              <label class="display-4 mb-3 urgent-bag"><strong>6 Kantong</strong></label>
              <p class="urgent-desc">"Butuh segera untuk penanganan darurat om saya."</p>
              <p class="urgent-user">Liliana Nana</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Nusa Tenggara Timur</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
              <span class="urgent-deadline d-md-none d-lg-block">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 menit yang lalu</span>
              <div style="text-align:right;" class="d-flex justify-content-end d-md-none d-lg-block">
                <!-- <button type="button" class="btn btn-outline-dark btn-sm urgent-info ml-1"><i class="fas fa-map-marker-alt mr-1"></i> Nusa Tenggara Timur</button> -->
                <a href="<?php echo base_url(); ?>urgent/edit/ff1234512" class="btn btn-outline-success btn-sm urgent-info ml-1"><i style="color:black;" class="fas fa-pencil-alt"></i>
                </a>
                <a href="<?php echo base_url(); ?>urgent/delete/ff1234512" class="btn btn-outline-danger btn-sm urgent-info ml-1"><i class="far fa-trash-alt"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mb-4 pb-1 d-none d-md-block d-lg-block">
            <div class="box-urgent-me">
              <label class="display-4 urgent-blood">Trombosit</label><br/>
              <label class="display-4 mb-3 urgent-bag"><strong>6 Kantong</strong></label>
              <p class="urgent-desc">"Butuh segera untuk penanganan darurat om saya."</p>
              <p class="urgent-user">Liliana Nana</p>
              <span class="urgent-deadline d-md-none d-lg-block">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 menit yang lalu</span>
              <div style="text-align:right;" class="d-flex justify-content-end d-md-none d-lg-block">
                <!-- <button type="button" class="btn btn-outline-dark btn-sm urgent-info ml-1"><i class="fas fa-map-marker-alt mr-1"></i> Makassar</button> -->
                <a href="<?php echo base_url(); ?>urgent/edit/ff1234512" class="btn btn-outline-success btn-sm urgent-info ml-1"><i style="color:black;" class="fas fa-pencil-alt"></i>
                </a>
                <a href="<?php echo base_url(); ?>urgent/delete/ff1234512" class="btn btn-outline-danger btn-sm urgent-info ml-1"><i class="far fa-trash-alt"></i>
                </a>
              </div>
            </div>
          </div>
          </div>
          
        </div>
          
        <div style="text-align: center;" class="col-lg-12 mb-5 mt-3 d-none d-lg-block pb-5">
          <a style="border-radius:25px;" href="<?php echo base_url(); ?>campaign" class="btn btn-success"><i style="color:white;" class="fas fa-th mr-2"></i> Lihat Lainnya</a>
        </div>
        <!-- <div class="col-lg-4 bg-light">
          
        </div> -->
      </div>
    </main>

    <?php $this->load->view('common/footer_view'); ?>
    
<?php
	$multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
	echo assets_js($multiple_js);
?>
</body>
</html>