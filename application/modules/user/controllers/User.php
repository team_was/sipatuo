<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends MX_Controller {
	function __construct()
	{
		parent::__construct();
	}

	function signin()
	{
		$this->load->view('view_loginuser');
	}

	function signup()
	{
		$this->load->view('view_signuppage');
	}

	function signout()
	{

	}

	function profil()
	{
		$this->load->view('view_profilpage');
	}

	function notification()
	{
		$this->load->view('view_notificationpage');
	}

	function message()
	{
		$this->load->view('view_messagepage');
	}

	function campaign()
	{
		$this->load->view('view_listcampaignpage');
	}

	function events()
	{
		$this->load->view('view_listeventspage');
	}

	function password()
	{
		$this->load->view('view_passwordpage');
	}

	function editprofil()
	{
		$this->load->view('view_editprofil');
	}
}