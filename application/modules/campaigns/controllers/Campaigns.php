<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaigns extends MX_Controller {
	function __construct()
	{
		parent::__construct();
	}

	public function campaign_detail()
	{
		$this->load->view('view_detailcampaignpage');
	}

	function index()
	{
		$this->load->view('view_listcampaignpage');
	}

	function create_campaign()
	{
		$this->load->view('view_createcampaignpage');
	}
}