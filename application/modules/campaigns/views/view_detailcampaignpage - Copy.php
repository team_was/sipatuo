<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sipatuo - Detail Urgent</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
	<?php
		$multiple_css = array('all.css','bootstrap.min.css','offcanvas.css');
		echo assets_css($multiple_css);
	?>
</head>
<body class="bg-light">
	<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <a style="font-size:1.55em;" class="navbar-brand ml-lg-3 mr-lg-3" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/Guard-Red.png" class="sip-logo"> <span class="">SIPATUO</span></a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
        <!-- <i style="color:#f2f2f2;margin-right:7px;" class="fas fa-ellipsis-v"></i> -->
      </button>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>"><i class="fas fa-smile-beam"></i> <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>campaign">Campaign</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>pendonor">Pendonor</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>events">Kegiatan</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>signup"><i class="fas fa-user-plus"></i></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a alt="Login" title="Login" class="nav-link" href="<?php echo base_url(); ?>signin"><i class="fas fa-door-open"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="container-fluid">

      <div style="min-height:800px;" class="row">
        <div class="col-lg-8 bg-light">
          <div style="text-align: center;" class="col-lg-12 mt-5">
            <!-- <p style="color:#c80d0d;text-shadow: 2px 2px 12px white;font-size: 2.33em;" class="display-4 mb-5 mt-5"><strong>#detail</strong>Kebutuhan</p> -->
            <!-- <div class="row">
              <div class="col-lg-3">
                <div class=""><div style="height:170px;width:100%;background:black;"></div></div>
              </div>
              <div class="col-lg-3">
                <div class=""><div style="height:170px;width:100%;background:green;"></div></div>
              </div>
              <div class="col-lg-3">
                <div class=""><div style="height:170px;width:100%;background:blue;"></div></div>
              </div>
              <div class="col-lg-3">
                <div class=""><div style="height:170px;width:100%;background:yellow;"></div></div>
              </div>
            </div> -->
          </div>
          <div class="col-lg-12">
            <p style="color:#c80d0d;text-shadow: 2px 2px 12px white;font-size: 2.33em;" class="display-4 mb-4 mt-0"><strong>#detail</strong>Kebutuhan</p>
          </div>
          <div class="col-lg-12">
            <div class="row justify-content-center">
              <div style="background:#24ad5f;height:130px;text-align: center;" class="col-lg-2">
                <span style="position: absolute;top:2px;left:5px;color:white;font-size:0.85em;">Pasien :</span>
                <label style="margin-top:25px;color:white;font-size:1.4em;" class="display-4">John Sediawan Andri</label>
              </div>
              <div style="background:red;height:130px;text-align: center;" class="col-lg-3">
                <span style="position: absolute;top:2px;left:5px;color:white;font-size:0.85em;">Goldar :</span>
                <label style="margin-top:31px;color:white;" class="display-4">A+</label>
              </div>
              <div style="background:#094687;height:130px;text-align: center;" class="col-lg-2">
                <span style="position: absolute;top:2px;left:5px;color:white;font-size:0.85em;">Kantong :</span>
                <label style="margin-top:31px;color:white;" class="display-4">12</label>
              </div>
              <div style="background:gray;height:130px;text-align: center;" class="col-lg-3">
                <span style="position: absolute;top:2px;left:5px;color:white;font-size:0.85em;">Kontak :</span>
                <label style="margin-top:51px;color:white;font-size:1.4em;" class="display-4">081231454675</label>
              </div>
              <div class="col-lg-12"></div>
              <div style="background:black;min-height:130px;text-align:left;padding:0px 13px 15px 23px;" class="col-lg-10">
                <span style="position: absolute;top:2px;left:5px;color:white;font-size:0.85em;">Deskripsi :</span>
                <p style="margin-top:31px;font-size:1em;" class="lead text-light">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
              </div>
              <div class="col-lg-12"></div>
              <div style="background:blue;height:130px;text-align: center;" class="col-lg-3">
                <span style="position: absolute;top:2px;left:5px;color:white;font-size:0.85em;">Lokasi :</span>
                <p style="color:white;margin-top:45px;">Rumah Sakit Wahidin Sudirohusodo</p>
              </div>
              <div style="background:gray;height:130px;text-align: center;" class="col-lg-2">
                <span style="position: absolute;top:2px;left:5px;color:white;font-size:0.85em;">Deadline :</span>
                <label style="color:white;margin-top:31px;margin-bottom: 0px;">12</label><br/>
                <label style="color:white;margin-bottom:0px;">SEPTEMBER</label><br/>
                <label style="color:white;margin-bottom:0px;">2018</label>
              </div>
              <div style="height:140px;" class="col-lg-5 bg-light"></div>
              <div class="col-lg-12"></div>
              <div style="height:140px;padding:7px 0px 0px 0px;" class="col-lg-10 bg-light mb-5">
                <a style="border-radius: 0px;" class="btn btn-sm btn-outline-secondary">Date Input : 1 Agustus 2018</a>
                <a style="border-radius: 0px;" class="btn btn-sm btn-outline-secondary">Viewers : 0</a><br/>
                <div class="mt-3"><span class="">Lokasi donor terdekat dari pasien :</span><br/>
                <span class="ml-3">1. Makassar</span> <br/>
                <span class="ml-3">2. Rumah Sakit</span></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 bg-secondary">
          <div style="text-align: center;" class="col-lg-12 d-none d-md-block d-lg-block">
            <!-- <p style="color:white;text-shadow: 2px 2px 12px #c80d0d;font-size: 1.55em;" class="display-4 mb-5 mt-5 pt-3"><strong>#kebutuhan</strong>Lainnya</p> -->
          </div>
          <div class="my-3 p-3 bg-secondary rounded mt-5">
            <a style="font-size:0.75em;" href="#" class="float-right">Lihat Semua</a>
            <h6 class="border-bottom border-gray pb-2 mb-0">Link terkait</h6>
            <div class="media text-white pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=0ccadb&fg=0ccadb&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-white">Butuh : A+ (3 Kantong)</strong>
                <span style="color:#e8e8e8;" class="d-block mb-2">Request by : Yunita Sinegar ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                <!-- <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span> -->
                <!-- <a style="font-size:0.85em;" class="btn btn-dark btn-sm mt-1" href="#">Lihat Detail</a> -->
              </p>
            </div>
            <div class="media text-white pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-white">Butuh : Trombosit (10 Kantong)</strong>
                <span style="color:#e8e8e8;" class="d-block mb-2">Request by : Ringki datas ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                Donec id elit non mi porta gravida at eget metus. 
                
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                <!-- <a style="font-size:0.85em;" class="btn btn-dark btn-sm mt-1" href="#">Lihat Detail</a> -->
              </p>
            </div>
            <div class="media text-white pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=6f42c1&fg=6f42c1&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-white">Butuh : AB+ (10 Kantong)</strong>
                <span style="color:#e8e8e8;" class="d-block mb-2">Request by : Ananda Riswa ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                Donec id elit non mi porta gravida at eget metus.
                
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                <!-- <a style="font-size:0.85em;" class="btn btn-dark btn-sm mt-1" href="#">Lihat Detail</a> -->
              </p>
            </div>
            <div class="media text-white pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=dbbb0c&fg=dbbb0c&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-white">Butuh : B+ (20 Kantong)</strong>
                <span style="color:#e8e8e8;" class="d-block mb-2">Request by : Indrawan Silha ( <a class="text-dark" href="#">Lihat Detail</a> )</span>
                Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                <!-- <a style="font-size:0.85em;" class="btn btn-dark btn-sm mt-1" href="#">Lihat Detail</a> -->
              </p>
            </div>
            <small class="d-block text-right mt-3 mb-3">
              <a class="text-white" href="#">Lihat Semua Kebutuhan</a>
            </small>
          </div>
        </div>
      </div>
      <div class="row justify-content-center box-gabung-sipper">
        <div class="col-lg-12 mt-5 text-gabung-donor">
          <h1 style="font-size:2em;color:black;" class="display-4"><strong>Ayo </strong>Bergabung bersama kami</h1>
          <h1 style="font-size:1em;color:#153b77;" class="display-4">Gabung, temukan, minta bantuan dan bantu teman yang membutuhkan bantuan anda.</h1>
        </div>
        <div style="text-align: center;" class="col-lg-6">
          <button type="button" class="btn btn-warning btn-lg-lg">Daftar</button> <span>atau</span>
          <button type="button" class="btn btn-info btn-lg-lg">Login</button>
        </div>
      </div>
    </main>

    <footer style="margin-top:50px;padding-top: 11px;" class="text-muted">
      <div class="container">
        <img class="float-right" width="50px" src="<?php echo base_url(); ?>assets/img/Guard-Dark.png">
        <!-- <p class="float-right">
          <a href="#"><i class="fas fa-tint"></i></a>
        </p> -->
        <a href="#"><i style="font-size: 1.55em;color:#174a9b;" class="fab fa-facebook-square mr-2"></i></a>
        <a href="#"><i style="font-size: 1.55em;" class="fab fa-twitter mr-2"></i></a>
        <a href="#"><i style="font-size: 1.55em;color:red;" class="fab fa-youtube mr-2"></i></a>
        <p>Banyak hal diluar sana yang perlu di explore. Berkelilinglah, rasakan keagungan Tuhan.</p>
        <p>2018 &copy; Sipatuo Team, <i class="fas fa-heart"></i> <i class="fas fa-smile"></i></p>
      </div>
    </footer>
<?php
	$multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
	echo assets_js($multiple_js);
?>
</body>
</html>