<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sipatuo - Urgent</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
	<?php
		$multiple_css = array('all.css','bootstrap.min.css','offcanvas.css');
		echo assets_css($multiple_css);
	?>
</head>
<body class="bg-light">
	<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <?php $this->load->view('common/navbar_title_view'); ?>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>"><i class="fas fa-smile-beam"></i> <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>campaign">Campaign</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>pendonor">Pendonor</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>events">Kegiatan</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>signup"><i class="fas fa-user-plus"></i></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a alt="Login" title="Login" class="nav-link" href="<?php echo base_url(); ?>signin"><i class="fas fa-door-open"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <?php $datacamp = 1; ?>

    <main role="main" class="container-fluid">
      <div style="min-height:600px;" class="row justify-content-center bg-white">
        <div class="col-lg-9 col-12">
          <!-- <div class="my-3 p-3 bg-white rounded d-lg-none d-md-none"></div> -->
          <div class="row justify-content-center pt-4">
          <?php if($datacamp > 0){ ?>
          <div style="text-align: center;" class="col-lg-12 mt-5 mb-4 d-none d-md-block d-lg-block">
            <h1 style="font-size:2.5em;color:black;" class="display-4"><strong>#urgent</strong>Needs</h1>
          </div>

          
          <div style="text-align: center;" class="col-lg-12 pb-5 d-none d-md-block d-lg-block">
            <a class="text-dark" href="#"><i style="font-size:1.3em;" class="fas fa-search mr-2"></i></a>
            <a class="text-dark" href="#"><i style="font-size:1.3em;" class="fas fa-plus-circle"></i></a>
          </div>
          <div class="col-lg-12 d-lg-none d-md-none">
            <div class="float-right"><a style="font-size:1.15em;" href="#" class="text-success mr-1"><i class="fas fa-plus-circle"></i></a> <a style="font-size:1.15em;" href="#" class="text-dark"><i class="fas fa-search"></i></a></div>
            <h6 style="" class="border-bottom border-gray pb-3 mb-0">Urgent Needs</h6>
          </div>
          <div class="col-lg-4 col-md-4 mb-lg-4 pb-1 box-main">
            <div class="media text-muted pt-3 d-lg-none d-md-none">
              <!-- <img data-src="holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1" alt="" class="mr-2 rounded"> -->
              <div style="width: 32px;height:32px;background:black;color:white;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>YS</strong></div>
              <div style="width: 32px;height:32px;background:#c4c4c4;color:white;text-align: center;padding-top:3px;position: absolute;top:51px;" class="mr-2 rounded"><strong>Tr</strong></div>
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-gray-dark">Butuh : Trombosit (10 Kantong)</strong>
                <strong class="d-block text-gray-dark mb-2">Yunita Sinegar</strong>
                Donec id elit non mi porta gravida at eget metus.
                
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Status : Open</span>
                <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-comments"></i> 0</a>
              </p>
            </div>
            <div class="box-urgent d-none d-md-block d-lg-block">
              <div class="display-4 urgent-blood">A+</div>
              <div class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong></div>
              <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih." (area Jawa Barat)</p>
              <p class="urgent-user text-danger">Yuanita Siregar</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
              <span class="urgent-deadline d-md-none d-lg-block">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>
              <span class="urgent-status d-md-none d-lg-block">Status : <strong style="color:#adabab;">Completed</strong></span>

              <div style="text-align:right;" class="box-info justify-content-end d-none d-md-block d-lg-block">
                <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-secondary btn-sm urgent-info ml-1"><i class="fas fa-comments"></i> 0</a>
                <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-warning btn-sm urgent-info"><i class="fas fa-info-circle mr-1"></i> Detail Kontak</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mb-lg-4 pb-1 box-main">
            <div class="media text-muted pt-3 d-lg-none d-md-none">
              <!-- <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded"> -->
              <div style="width: 32px;height:32px;background:#fc5a5a;color:white;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>AW</strong></div>
              <div style="width: 32px;height:32px;background:#c4c4c4;color:white;text-align: center;padding-top:3px;position: absolute;top:51px;" class="mr-2 rounded"><strong>A+</strong></div>
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-gray-dark">Butuh : A+ (7 Kantong)</strong>
                <strong class="d-block text-gray-dark">Andi Waya Meraja</strong>
                Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Status : Open</span>
                <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-success ml-0 mt-2"><i class="fas fa-comments"></i> 5</a>
              </p>
            </div>
            <div class="box-urgent d-none d-md-block d-lg-block">
              <div class="display-4 urgent-blood">AB+</div>
              <div class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong> <span class="blink_me" style="font-size:0.70em;">(Butuh 2 Kantong lagi)</span></div>
              <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih."</p>
              <p class="urgent-user text-danger">Yuanita Siregar</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
              <span class="urgent-deadline">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>
              <span class="urgent-status d-md-none d-lg-block">Status : <strong style="color:#1ecc49;">Open</strong></span>

              <div style="text-align:right;" class="box-info justify-content-end d-none d-md-block d-lg-block">
                <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-secondary btn-sm urgent-info ml-1"><i class="fas fa-comments"></i> 0</a>
                <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-warning btn-sm urgent-info"><i class="fas fa-info-circle mr-1"></i> Detail Kontak</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mb-lg-4 pb-1 box-main">
            <div class="media text-muted pt-3 d-lg-none d-md-none">
              <!-- <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded"> -->
              <div style="width: 32px;height:32px;background:#fc5a5a;color:white;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>LN</strong></div>
              <div style="width: 32px;height:32px;background:#c4c4c4;color:white;text-align: center;padding-top:3px;position: absolute;top:51px;" class="mr-2 rounded"><strong style="font-size:0.8em;">AB+</strong></div>
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-gray-dark">Butuh : A+ (7 Kantong)</strong>
                <strong class="d-block text-gray-dark">Liliana Nana</strong>
                Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Status : Open</span>
                <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-comments"></i> 0</a>
              </p>
            </div>
            <div class="box-urgent d-none d-md-block d-lg-block">
              <div class="display-4 urgent-blood">B+</div>
              <div class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong></div>
              <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih." (area Makassar)</p>
              <p class="urgent-user text-danger">Yuanita Siregar</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
              <span class="urgent-deadline">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>
              <span class="urgent-status d-md-none d-lg-block">Status : <strong style="color:#adabab;">Completed</strong></span>

              <div style="text-align:right;" class="box-info justify-content-end d-none d-md-block d-lg-block">
                <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-secondary btn-sm urgent-info ml-1"><i class="fas fa-comments"></i> 0</a>
                <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-warning btn-sm urgent-info"><i class="fas fa-info-circle mr-1"></i> Detail Kontak</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mb-lg-4 pb-1 box-main">
            <div class="media text-muted pt-3 d-lg-none d-md-none">
              <!-- <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded"> -->
              <div style="width: 32px;height:32px;background:#fc5a5a;color:white;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>LN</strong></div>
              <div style="width: 32px;height:32px;background:#c4c4c4;color:white;text-align: center;padding-top:3px;position: absolute;top:51px;" class="mr-2 rounded"><strong>B+</strong></div>
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-gray-dark">Butuh : A+ (7 Kantong)</strong>
                <strong class="d-block text-gray-dark">Liliana Nana</strong>
                Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Status : Open</span>
                <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-comments"></i> 0</a>
              </p>
            </div>
            <div class="box-urgent d-none d-md-block d-lg-block">
              <div class="display-4 urgent-blood">O+</div>
              <div class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong></div>
              <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih."</p>
              <p class="urgent-user text-danger">Yuanita Siregar</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
              <span class="urgent-deadline">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>
              <span class="urgent-status d-md-none d-lg-block">Status : <strong style="color:#adabab;">Completed</strong></span>

              <div style="text-align:right;" class="box-info justify-content-end d-none d-md-block d-lg-block">
                <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-secondary btn-sm urgent-info ml-1"><i class="fas fa-comments"></i> 0</a>
                <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-warning btn-sm urgent-info"><i class="fas fa-info-circle mr-1"></i> Detail Kontak</a>
              </div>
            </div>
          </div>

          <div style="text-align: center;" class="col-lg-12 mb-5 mt-3 pb-lg-5">
            <a style="border-radius: 20px;" href="<?php echo base_url(); ?>campaign" class="btn btn-sm btn-success px-4"><i style="color:white;" class="fas fa-th mr-2"></i> Lihat Lainnya</a>
          </div>
          <?php } else { ?>

          <div class="col-lg-7 mt-5 mb-5">
            <div class="jumbotron text-center pt-5">
                <img width="120px" src="<?php echo base_url(); ?>assets/img/nodata_campaign.png" alt="">
                <h1 style="font-size:1.7em;" class="display-4 mb-4">Belum ada data <strong style="color:#008743;">campaign</strong>.</h1>
                <p style="font-size:1.1em;" class="lead mb-4">Campaign bertujuan untuk mencari pendonor sesuai dengan kebutuhan darah pasien.</p>
                <hr class="my-4">
                <a style="border-radius: 20px;" href="<?php echo base_url(); ?>campaign" class="btn btn-sm btn-success px-4"><i style="color:white;" class="fas fa-plus-circle mr-2"></i> Buat Campaign</a> 
            </div>
          </div>
          

          <?php } ?>
          </div>
        </div>
          
        

      </div>
    </main>

    <?php $this->load->view('common/footer_view'); ?>
    
<?php
	$multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
	echo assets_js($multiple_js);
?>
</body>
</html>