<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sipatuo - Urgent</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
	<?php
		$multiple_css = array('all.css','bootstrap.min.css','offcanvas.css');
		echo assets_css($multiple_css);
	?>
</head>
<body class="bg-light">
	<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <a style="font-size:1.55em;" class="navbar-brand ml-lg-3 mr-lg-3" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/Guard-Red.png" class="sip-logo"> <span class="">SIPATUO</span></a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
        <!-- <i style="color:#f2f2f2;font-size: 1.5em;font-weight: 100;" class="fas fa-stream"></i> -->
      </button>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>"><i class="fas fa-smile-beam"></i> <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>campaign">Campaign</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>pendonor">Pendonor</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>events">Kegiatan</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>signup"><i class="fas fa-user-plus"></i></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a alt="Login" title="Login" class="nav-link" href="<?php echo base_url(); ?>signin"><i class="fas fa-door-open"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="container-fluid">
      <div style="min-height:600px;" class="row justify-content-center bg-white">
        <div class="col-lg-9 col-12">
          <!-- <div class="my-3 p-3 bg-white rounded d-lg-none d-md-none"></div> -->
          <div class="row justify-content-center pt-4">
            <div style="text-align: center;" class="col-lg-12 mt-5 mb-4 d-none d-md-block d-lg-block">
              <h1 style="font-size:2.5em;color:black;" class="display-4"><strong>#urgent</strong>Needs</h1>
            </div>
            <div style="text-align: center;" class="col-lg-12 pb-5 d-none d-md-block d-lg-block">
              <button style="border-radius:0px;border:solid 1px #ffe2e2;box-shadow: 3px 3px 2px #ffe2e2;" type="button" class="btn btn-outline-secondary mr-2"><i class="fas fa-search"></i></button>
              <a href="<?php echo base_url(); ?>campaign/create" style="border-radius:0px;border:solid 1px #ffe2e2;box-shadow: 3px 3px 2px #ffe2e2;" class="btn btn-outline-secondary"><i class="fas fa-plus-circle"></i></a>
            </div>
          <div class="col-lg-12 d-lg-none d-md-none">
            <div class="float-right"><a style="font-size:1.15em;" href="#" class="text-success mr-1"><i class="fas fa-plus-circle"></i></a> <a style="font-size:1.15em;" href="#" class="text-dark"><i class="fas fa-search"></i></a></div>
            <h6 style="" class="border-bottom border-gray pb-3 mb-0">Urgent Needs</h6>
          </div>
          <div class="col-lg-4 col-md-4 mb-lg-4 pb-1 box-main">
            <div class="media text-muted pt-3 d-lg-none d-md-none">
              <!-- <img data-src="holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1" alt="" class="mr-2 rounded"> -->
              <div style="width: 32px;height:32px;background:black;color:white;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>YS</strong></div>
              <div style="width: 32px;height:32px;background:#c4c4c4;color:white;text-align: center;padding-top:3px;position: absolute;top:51px;" class="mr-2 rounded"><strong>Tr</strong></div>
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-gray-dark">Butuh : Trombosit (10 Kantong)</strong>
                <strong class="d-block text-gray-dark mb-2">Yunita Sinegar</strong>
                Donec id elit non mi porta gravida at eget metus.
                
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
              </p>
            </div>
            <div class="box-urgent d-none d-md-block d-lg-block">
              <div class="display-4 urgent-blood">A+</div>
              <div class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong></div>
              <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih."</p>
              <p class="urgent-user text-danger">Yuanita Siregar</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
              <span class="urgent-deadline d-md-none d-lg-block">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>

              <div style="text-align:right;" class="box-info justify-content-end d-none d-md-block d-lg-block">
                <button type="button" class="btn btn-outline-dark btn-sm urgent-info ml-1"><i class="fas fa-map-marker-alt mr-1"></i> Kalimantan Selatan</button>
                <a href="<?php echo base_url(); ?>urgent/detail/ff1234512" class="btn btn-outline-danger btn-sm urgent-info ml-1"><i class="fas fa-info-circle mr-1"></i> Detail Kontak</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mb-lg-4 pb-1 box-main">
            <div class="media text-muted pt-3 d-lg-none d-md-none">
              <!-- <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded"> -->
              <div style="width: 32px;height:32px;background:#fc5a5a;color:white;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>AW</strong></div>
              <div style="width: 32px;height:32px;background:#c4c4c4;color:white;text-align: center;padding-top:3px;position: absolute;top:51px;" class="mr-2 rounded"><strong>A+</strong></div>
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-gray-dark">Butuh : A+ (7 Kantong)</strong>
                <strong class="d-block text-gray-dark">Andi Waya Meraja</strong>
                Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
              </p>
            </div>
            <div class="box-urgent d-none d-md-block d-lg-block">
              <div class="display-4 urgent-blood">A+</div>
              <div class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong></div>
              <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih."</p>
              <p class="urgent-user text-danger">Yuanita Siregar</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
              <span class="urgent-deadline">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>

              <div style="text-align:right;" class="box-info justify-content-end d-none d-md-block d-lg-block">
                <button type="button" class="btn btn-outline-dark btn-sm urgent-info ml-1"><i class="fas fa-map-marker-alt mr-1"></i> Kalimantan Selatan</button>
                <a href="<?php echo base_url(); ?>urgent/detail/ff1234512" class="btn btn-outline-danger btn-sm urgent-info ml-1"><i class="fas fa-info-circle mr-1"></i> Detail Kontak</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mb-lg-4 pb-1 box-main">
            <div class="media text-muted pt-3 d-lg-none d-md-none">
              <!-- <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded"> -->
              <div style="width: 32px;height:32px;background:#fc5a5a;color:white;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>LN</strong></div>
              <div style="width: 32px;height:32px;background:#c4c4c4;color:white;text-align: center;padding-top:3px;position: absolute;top:51px;" class="mr-2 rounded"><strong style="font-size:0.8em;">AB+</strong></div>
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-gray-dark">Butuh : A+ (7 Kantong)</strong>
                <strong class="d-block text-gray-dark">Liliana Nana</strong>
                Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
              </p>
            </div>
            <div class="box-urgent d-none d-md-block d-lg-block">
              <div class="display-4 urgent-blood">A+</div>
              <div class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong></div>
              <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih."</p>
              <p class="urgent-user text-danger">Yuanita Siregar</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
              <span class="urgent-deadline">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>

              <div style="text-align:right;" class="box-info justify-content-end d-none d-md-block d-lg-block">
                <button type="button" class="btn btn-outline-dark btn-sm urgent-info ml-1"><i class="fas fa-map-marker-alt mr-1"></i> Kalimantan Selatan</button>
                <a href="<?php echo base_url(); ?>urgent/detail/ff1234512" class="btn btn-outline-danger btn-sm urgent-info ml-1"><i class="fas fa-info-circle mr-1"></i> Detail Kontak</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mb-lg-4 pb-1 box-main">
            <div class="media text-muted pt-3 d-lg-none d-md-none">
              <!-- <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded"> -->
              <div style="width: 32px;height:32px;background:#fc5a5a;color:white;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>LN</strong></div>
              <div style="width: 32px;height:32px;background:#c4c4c4;color:white;text-align: center;padding-top:3px;position: absolute;top:51px;" class="mr-2 rounded"><strong>B+</strong></div>
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-gray-dark">Butuh : A+ (7 Kantong)</strong>
                <strong class="d-block text-gray-dark">Liliana Nana</strong>
                Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
              </p>
            </div>
            <div class="box-urgent d-none d-md-block d-lg-block">
              <div class="display-4 urgent-blood">A+</div>
              <div class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong></div>
              <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih."</p>
              <p class="urgent-user text-danger">Yuanita Siregar</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
              <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
              <span class="urgent-deadline">Deadline : 24 Juli 2018</span>
              <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>

              <div style="text-align:right;" class="box-info justify-content-end d-none d-md-block d-lg-block">
                <button type="button" class="btn btn-outline-dark btn-sm urgent-info ml-1"><i class="fas fa-map-marker-alt mr-1"></i> Kalimantan Selatan</button>
                <a href="<?php echo base_url(); ?>urgent/detail/ff1234512" class="btn btn-outline-danger btn-sm urgent-info ml-1"><i class="fas fa-info-circle mr-1"></i> Detail Kontak</a>
              </div>
            </div>
          </div>
          
          
          </div>
          
        </div>
          
        <div style="text-align: center;" class="col-lg-12 mb-5 mt-3 pb-lg-5">
          <a style="border-radius: 20px;" href="<?php echo base_url(); ?>campaign" class="btn btn-sm btn-success px-4"><i style="color:white;" class="fas fa-th mr-2"></i> Lihat Lainnya</a>
        </div>
        <!-- <div class="col-lg-4 bg-light">
          
        </div> -->
      </div>
    </main>

    <footer style="margin-top:50px;padding-top: 11px;" class="text-muted">
      <div class="container">
        <img class="float-right" width="50px" src="<?php echo base_url(); ?>assets/img/Guard-Dark.png">
        <!-- <p class="float-right">
          <a href="#"><i class="fas fa-tint"></i></a>
        </p> -->
        <a href="#"><i style="font-size: 1.55em;color:#174a9b;" class="fab fa-facebook-square mr-2"></i></a>
        <a href="#"><i style="font-size: 1.55em;" class="fab fa-twitter mr-2"></i></a>
        <a href="#"><i style="font-size: 1.55em;color:red;" class="fab fa-youtube mr-2"></i></a>
        <p>Banyak hal diluar sana yang perlu di explore. Berkelilinglah, rasakan keagungan Tuhan.</p>
        <p>2018 &copy; Sipatuo Team, <i class="fas fa-heart"></i> <i class="fas fa-smile"></i></p>
      </div>
    </footer>
<?php
	$multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
	echo assets_js($multiple_js);
?>
</body>
</html>