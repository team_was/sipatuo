<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sipatuo - Detail Urgent</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
	<?php
		$multiple_css = array('all.css','bootstrap.min.css','offcanvas.css');
		echo assets_css($multiple_css);
	?>
</head>
<body class="bg-light">
	<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <?php $this->load->view('common/navbar_title_view'); ?>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>"><i class="fas fa-smile-beam"></i> <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>campaign">Campaign</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>pendonor">Pendonor</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>events">Kegiatan</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>signup"><i class="fas fa-user-plus"></i></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a alt="Login" title="Login" class="nav-link" href="<?php echo base_url(); ?>signin"><i class="fas fa-door-open"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="container-fluid">

      <div style="min-height:800px;" class="row justify-content-center">
        <div class="col-lg-8 bg-light">
          <div style="text-align: center;" class="col-lg-12 mt-5">
            <!-- <p style="color:#c80d0d;text-shadow: 2px 2px 12px white;font-size: 2.33em;" class="display-4 mb-5 mt-5"><strong>#detail</strong>Kebutuhan</p> -->
            <!-- <div class="row">
              <div class="col-lg-3">
                <div class=""><div style="height:170px;width:100%;background:black;"></div></div>
              </div>
              <div class="col-lg-3">
                <div class=""><div style="height:170px;width:100%;background:green;"></div></div>
              </div>
              <div class="col-lg-3">
                <div class=""><div style="height:170px;width:100%;background:blue;"></div></div>
              </div>
              <div class="col-lg-3">
                <div class=""><div style="height:170px;width:100%;background:yellow;"></div></div>
              </div>
            </div> -->
          </div>
          <div class="col-lg-12 text-center">
            <p style="color:#c80d0d;text-shadow: 2px 2px 12px white;font-size: 2.33em;" class="display-4 mb-5 mt-0"><strong>#detail</strong>Campaign</p>
          </div>
          <div style="margin: 0 auto;" class="col-lg-10">
            <div class="row justify-content-center">
              <div class="col-lg-12 pl-0">
                <p class="pl-0">Berniat membantu ? silahkan hubungi nomor kontak dibawah.</p>
              </div>
              <div style="min-height:130px;text-align: center;border:solid 1px white;border-radius:10px;" class="col-lg-3 bg-danger">
                <span style="position: absolute;top:5px;left:10px;color:black;font-size:0.85em;">Goldar :</span>
                <label style="margin-top:27px;color:white;" class="display-4">A+</label>
              </div>
              <div style="min-height:130px;text-align: center;border:solid 1px white;border-radius:10px;" class="col-lg-3 bg-success">
                <span style="position: absolute;top:5px;left:10px;color:black;font-size:0.85em;">Kantong :</span>
                <label style="margin-top:27px;color:white;" class="display-4">12</label>
              </div>
              <div style="min-height:130px;text-align: center;border:solid 1px white;border-radius:10px;" class="col-lg-3 bg-dark">
                <span style="position: absolute;top:5px;left:10px;color:white;font-size:0.85em;">Pasien :</span>
                <label style="margin-top:40px;color:white;font-size:1.4em;" class="display-4">John Sediawan Andri</label>
              </div>
              <div style="min-height:130px;text-align: center;border:solid 1px white;border-radius:10px;" class="col-lg-3 bg-secondary">
                <span style="position: absolute;top:5px;left:10px;color:black;font-size:0.85em;">Kontak :</span>
                <label style="margin-top:51px;color:white;font-size:1.4em;" class="display-4">081231454675</label>
              </div>
              <!-- <div class="col-lg-12"></div> -->
              <div style="background:;min-height:130px;text-align:left;padding:0px 13px 15px 23px;border:solid 1px white;border-radius:15px;" class="col-lg-12 mt-1 mb-1 bg-warning">
                <span style="position: absolute;top:5px;left:10px;color:white;font-size:0.85em;">Deskripsi :</span>
                <p style="margin-top:31px;font-size:1em;" class="lead text-dark">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
              </div>
              <div style="background:;min-height:130px;text-align: center;border:solid 1px white;border-radius:10px;" class="col-lg-3 bg-info">
                <span style="position: absolute;top:5px;left:10px;color:black;font-size:0.85em;">Lokasi :</span>
                <p style="color:white;margin-top:29px;">Rumah Sakit Wahidin Sudirohusodo</p>
              </div>
              <div style="background:;min-height:130px;text-align: center;border:solid 1px white;border-radius:10px;" class="col-lg-3 bg-dark">
                <span style="position: absolute;top:5px;left:10px;color:white;font-size:0.85em;">Deadline :</span>
                <p style="color:white;margin-top:51px;"> 12 Sept 2018</p>
              </div>
              <div style="background:;min-height:130px;text-align: center;border:solid 1px white;" class="col-lg-6 bg-white">
                <span style="position: absolute;top:5px;left:10px;color:black;font-size:0.85em;">Status :</span>
                <p style="margin-top:20px;font-size: 3em;" class="text-success display-4 py-0 mb-0">Open</p>
                <small style="color:#7a7a7a;">Hubungi nomor kontak untuk informasi detail</small>
              </div>
              <!-- <div style="height:140px;" class="col-lg-3 bg-light"></div> -->
              <div style="height:140px;padding:7px 0px 0px 0px;" class="col-lg-12 bg-light mb-3">
                <a style="border-radius: 25px;" class="btn btn-sm btn-outline-dark px-4">Date Input : 1 Agustus 2018</a>
                <a style="border-radius: 25px;" class="btn btn-sm btn-outline-dark px-4">Join : 0</a><br/>
                <div class="mt-3"><span class="">Lokasi donor terdekat dari pasien :</span><br/>
                <span class="ml-3">1. Makassar</span> <br/>
                <span class="ml-3">2. Rumah Sakit</span></div>
              </div>
              <div style="border-radius: 15px;" class="col-lg-12 bg-white p-3 mb-5">
                <label style="font-size:2em;" class="display-4 mb-0">Komentar</label> <span style="font-size: 1.2em;" class="badge badge-dark">2</span>
                <hr/>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="media text-dark mb-2">
                      <img style="width:40px;height:40px;" src="<?php echo base_url(); ?>assets/img/user-3.png" alt="" class="mr-2 rounded">
                      <p class="media-body pb-3 mb-0 small lh-125 border-gray">
                        <strong class="d-block text-dark">Ahmad Imron</strong>
                        <span style="color:#afafaf;font-size:0.9em;" class="d-block mb-2"><i class="far fa-clock mr-2"></i>24 September 2018 , 08:00</span>
                        Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                      </p>
                    </div>
                    <div class="media text-dark mb-2">
                      <img style="width:40px;height:40px;" src="<?php echo base_url(); ?>assets/img/user-4.png" alt="" class="mr-2 rounded">
                      <p class="media-body pb-3 mb-0 small lh-125 border-gray">
                        <strong class="d-block text-dark">Suryadarmadi</strong>
                        <span style="color:#afafaf;font-size:0.9em;" class="d-block mb-2"><i class="far fa-clock mr-2"></i>24 September 2018 , 08:00</span>
                        Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                      </p>
                    </div>
                  </div>

                  <div class="col-lg-12 mb-3 px-4">
                    <form>
                      <hr/>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-lg-5">
                            <!-- <label class="lbl" for="username"><i style="font-size: 0.8em;" class="fab fa-facebook-square mr-2"></i>Facebook</label> -->
                            <input style="border-radius: 20px;font-size:0.8em;" type="text" class="form-control form-control-sm px-3 py-2" id="nama" placeholder="Masukkan nama anda">
                          </div>
                          <div class="col-lg-7">
                            <!-- <label class="lbl" for="username"><i style="font-size: 0.8em;" class="fab fa-line mr-2"></i>Line</label> -->
                            <input style="border-radius: 20px;font-size:0.8em;" type="email" class="form-control form-control-sm px-3 py-2" id="email" placeholder="Masukkan email anda">
                          </div>
                        </div>                   
                      </div>
                      <div class="form-group mb-2">
                        <textarea placeholder="Masukkan Komentar Anda" rows="4" style="border-radius: 20px;font-size:0.8em;" class="form-control"></textarea>
                      </div>
                      <div class="col-lg-12 text-right">
                        <button style="border-radius: 25px" type="submit" class="btn btn-sm btn-warning mb-2 px-4"><i class="far fa-comment-dots mr-1"></i>Simpan Komentar</button>
                      </div>
                    </form>
                  </div>  
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="col-lg-4 bg-white">

          <div class="row justify-content-center">
            <div class="my-3 p-3 bg-white rounded ml-lg-3 mt-5">
              <a style="font-size:0.75em;" href="#" class="float-right">Lihat Semua</a>
              <h6 class="border-bottom border-gray pb-2 mb-0">Link terkait</h6>
              <div class="media text-dark pt-3">
                <img data-src="holder.js/32x32?theme=thumb&bg=0ccadb&fg=0ccadb&size=1" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-dark">Butuh : A+ (3 Kantong)</strong>
                  <span style="color:#960000;" class="d-block mb-2">Request by : Yunita Sinegar</span>
                  Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                  
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                </p>
              </div>
              <div class="media text-dark pt-3">
                <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-dark">Butuh : Trombosit (10 Kantong)</strong>
                  <span style="color:#960000;" class="d-block mb-2">Request by : Ringki datas</span>
                  Donec id elit non mi porta gravida at eget metus.               
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                </p>

              </div>
              <div class="media text-dark pt-3">
                <img data-src="holder.js/32x32?theme=thumb&bg=6f42c1&fg=6f42c1&size=1" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-dark">Butuh : AB+ (10 Kantong)</strong>
                  <span style="color:#960000;" class="d-block mb-2">Request by : Ananda Riswa </span>
                  Donec id elit non mi porta gravida at eget metus.
                  
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                </p>
              </div>
              <div class="media text-dark pt-3">
                <img data-src="holder.js/32x32?theme=thumb&bg=dbbb0c&fg=dbbb0c&size=1" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-dark">Butuh : B+ (20 Kantong)</strong>
                  <span style="color:#960000;" class="d-block mb-2">Request by : Indrawan Silha</span>
                  Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                  
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                </p>
              </div>
              <small class="d-block text-right mt-3 mb-3">
                <a class="text-dark" href="#">Lihat Semua Kebutuhan</a>
              </small>
            </div>
          </div>
            
        </div>
      </div>
      <div class="row justify-content-center box-gabung-sipper">
        <div class="col-lg-12 mt-5 text-gabung-donor">
          <h1 style="font-size:2em;color:black;" class="display-4"><strong>Ayo </strong>Bergabung bersama kami</h1>
          <h1 style="font-size:1em;color:#153b77;" class="display-4">Gabung, temukan, minta bantuan dan bantu teman yang membutuhkan bantuan anda.</h1>
        </div>
        <div style="text-align: center;" class="col-lg-6">
          <a style="border-radius:20px;" href="<?php echo base_url(); ?>signup" class="btn btn-outline-warning px-lg-5 btn-lg-lg">Daftar</a> <span class="px-lg-2">atau</span>
          <a style="border-radius:20px;" href="<?php echo base_url(); ?>signin" class="btn btn-outline-info px-lg-5 btn-lg-lg">Login</a>
        </div>
      </div>
    </main>

    <?php $this->load->view('common/footer_view'); ?>
    
<?php
	$multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
	echo assets_js($multiple_js);
?>
</body>
</html>