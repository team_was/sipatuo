<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sipatuo - Urgent</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
	<?php
		$multiple_css = array('all.css','bootstrap.min.css','offcanvas.css');
		echo assets_css($multiple_css);
	?>
</head>
<body class="bg-light">
	<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <a style="font-size:1.55em;" class="navbar-brand ml-lg-3 mr-lg-3" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/Guard-Red.png" class="sip-logo"> <span class="">SIPATUO</span></a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
        <!-- <i style="color:#f2f2f2;margin-right:7px;" class="fas fa-ellipsis-v"></i> -->
      </button>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>"><i class="fas fa-smile-beam"></i> <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>campaign">Campaign</a>
          </li>
          <li class="nav-item active mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>pendonor">Pendonor</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>events">Kegiatan</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>signup"><i class="fas fa-user-plus"></i></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a alt="Login" title="Login" class="nav-link" href="<?php echo base_url(); ?>signin"><i class="fas fa-door-open"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="container-fluid">
      <div style="min-height:600px;" class="row justify-content-center bg-white">
        <div class="col-lg-10">
          <div class="row justify-content-center pt-4">
            <div style="text-align: center;" class="col-lg-12 mt-5 mb-5 d-none d-md-block d-lg-block">
              <h1 style="font-size:2.5em;color:black;" class="display-4"><strong>#daftar</strong>Pendonor</h1>
            </div>
            <div class="col-lg-12 d-lg-none d-md-none">
              <div class="float-right"><a style="font-size:1.15em;" href="#" class="text-success mr-1"><i class="fas fa-plus-circle"></i></a> <a style="font-size:1.15em;" href="#" class="text-dark"><i class="fas fa-search"></i></a></div>
              <h6 style="" class="border-bottom border-gray pb-3 mb-0">Daftar Pendonor</h6>
            </div>

            <div class="col-lg-2 col-md-4 mb-lg-5 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img style="border:solid 1px #efefef" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-2.jpg" alt="" class="mr-2 rounded">
                <div style="width: 60px;height:25px;background:#ffc4c4;color:white;text-align: center;padding-top:0px;position: absolute;top:80px;" class="mr-2 rounded"><strong>A+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Nama : Suryadarmadi</strong>
                  <strong class="d-block text-secondary mb-2">Goldar : A+</strong>
                  Bahagia itu sederhana, sesederhana perasaan ini yang mudah jatuh karenamu.
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Sulawesi Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Join : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Online : 1 Minute ago</span>
                  <span class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-id-card text-info"></i></span>
                  <span class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-envelope text-success"></i></span>
                </p>
              </div>
              <div style="box-shadow: 3px 4px 9px #515151;" class="card d-none d-md-block d-lg-block">
                <div class="font-blood"><strong>A+</strong></div>
                <img width="100%" height="150px" class="card-img-top" src="<?php echo base_url(); ?>assets/img/user-2.jpg" alt="Card image cap">
                <div style="min-height:230px;text-align: center;" class="card-body pt-3">
                  <h5 style="margin-bottom:9px;font-size: 0.92em;" class="card-title"><a class="text-dark" href="<?php echo base_url(); ?>pendonor/detail">Suryadarmadi Sadar Pegalungan barat daya</a></h5>
                  <h6 style="font-size:0.75em;font-weight: 200;" class="card-subtitle mt-2 mb-2 text-muted">Lokasi : Sulawesi Selatan</h6>
                  <p class="card-text mb-0">
                    <button type="button" class="btn btn-outline-success urgent-info mb-2"><i class="fas fa-envelope"></i></button>
                  </p>
                  <p class="mb-0"><a class="mb-0" style="font-size:0.70em;" href="<?php echo base_url(); ?>pendonor/detail">Lihat Detail</a></p>
                  <p style="font-size:0.72em;" class="card-text mt-0"><small class="text-muted mt-0">Last online 3 mins ago</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 mb-lg-5 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <div style="width: 60px;height:60px;background:#918f8f;color:white;text-align: center;padding-top:17px;" class="mr-2 rounded"><strong>AIM</strong></div>
                <div style="width: 60px;height:25px;background:#ffc4c4;color:white;text-align: center;padding-top:0px;position: absolute;top:80px;" class="mr-2 rounded"><strong>O+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Nama : Ahmad Imron Sadar</strong>
                  <strong class="d-block text-secondary mb-2">Goldar : O+</strong>
                  Celahmu akan dianggap sempurna oleh hati yang memang ditakdirkan untukmu.
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Sulawesi Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Join : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Online : 1 Minute ago</span>
                  <span class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-id-card text-info"></i></span>
                  <span class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-envelope text-success"></i></span>
                </p>
              </div>
              <div style="box-shadow: 3px 4px 9px #515151;" class="card d-none d-md-block d-lg-block">
                <div class="font-blood"><strong>AB+</strong></div>
                <img width="100%" height="150px" class="card-img-top" src="<?php echo base_url(); ?>assets/img/user-1.png" alt="Card image cap">
                <div style="min-height:230px;text-align: center;" class="card-body pt-3 pb-3">
                  <h5 style="margin-bottom:9px;font-size: 0.92em;" class="card-title">Ahmad Imron Sadar Mastang oke</h5>
                  <h6 style="font-size:0.75em;font-weight: 200;" class="card-subtitle mt-2 mb-2 text-muted">Lokasi : Sulawesi Selatan</h6>
                  <p class="card-text mb-1">
                    <button type="button" class="btn btn-outline-success urgent-info mb-2"><i class="fas fa-envelope"></i></button>
                  </p>
                  <p class="mb-0"><a class="mb-0" style="font-size:0.70em;" href="<?php echo base_url(); ?>pendonor/detail">Lihat Detail</a></p>
                  <p style="font-size:0.72em;" class="card-text"><small class="text-muted">Last online 3 mins ago</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 mb-lg-5 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <div style="width: 60px;height:60px;background:#918f8f;color:white;text-align: center;padding-top:17px;" class="mr-2 rounded"><strong>MM</strong></div>
                <div style="width: 60px;height:25px;background:#ffc4c4;color:white;text-align: center;padding-top:0px;position: absolute;top:80px;" class="mr-2 rounded"><strong>O+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Nama : Manda Munda</strong>
                  <strong class="d-block text-secondary mb-2">Goldar : O+</strong>
                  -
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Sulawesi Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Join : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Online : 1 Minute ago</span>
                  <span class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-id-card text-info"></i></span>
                  <span class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-envelope text-success"></i></span>
                </p>
              </div>
              <div style="box-shadow: 3px 4px 9px #515151;" class="card d-none d-md-block d-lg-block">
                <div class="font-blood"><strong>B-</strong></div>
                <img width="100%" height="150px" class="card-img-top" src="<?php echo base_url(); ?>assets/img/user-3.png" alt="Card image cap">
                <div style="min-height:230px;text-align: center;" class="card-body pt-3">
                  <h5 style="margin-bottom:9px;font-size: 0.92em;" class="card-title">Wahyudi Sadar</h5>
                  <h6 style="font-size:0.75em;font-weight: 200;" class="card-subtitle mt-2 mb-2 text-muted">Lokasi : Sulawesi Selatan</h6>
                  <p class="card-text mb-0"><button type="button" class="btn btn-outline-success urgent-info mb-2"><i class="fas fa-envelope"></i></button></p>
                  <p class="mb-0"><a class="mb-0" style="font-size:0.70em;" href="<?php echo base_url(); ?>pendonor/detail">Lihat Detail</a></p>
                  <p style="font-size:0.72em;" class="card-text"><small class="text-muted">Last online 3 mins ago</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 mb-lg-5 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <div style="width: 60px;height:60px;background:#918f8f;color:white;text-align: center;padding-top:17px;" class="mr-2 rounded"><strong>WS</strong></div>
                <div style="width: 60px;height:25px;background:#ffc4c4;color:white;text-align: center;padding-top:0px;position: absolute;top:80px;" class="mr-2 rounded"><strong>O+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Nama : Wahyudi Sadar</strong>
                  <strong class="d-block text-secondary mb-2">Goldar : O+</strong>
                  -
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Sulawesi Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Join : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Online : 1 Minute ago</span>
                  <span class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-id-card text-info"></i></span>
                  <span class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-envelope text-success"></i></span>
                </p>
              </div>
              <div style="box-shadow: 3px 4px 9px #515151;" class="card d-none d-md-block d-lg-block">
                <div class="font-blood"><strong>O-</strong></div>
                <img width="100%" height="150px" class="card-img-top" src="<?php echo base_url(); ?>assets/img/user-4.png" alt="Card image cap">
                <div style="min-height:230px;text-align: center;" class="card-body pt-3">
                  <h5 style="margin-bottom:9px;font-size: 0.92em;" class="card-title">Handoko joko tinggori</h5>
                  <h6 style="font-size:0.75em;font-weight: 200;" class="card-subtitle mt-2 mb-2 text-muted">Lokasi : Kalimantan Selatan</h6>
                  <p class="card-text mb-0"><button type="button" class="btn btn-outline-success urgent-info mb-2"><i class="fas fa-envelope"></i></button></p>
                  <p class="mb-0"><a class="mb-0" style="font-size:0.70em;" href="<?php echo base_url(); ?>pendonor/detail">Lihat Detail</a></p>
                  <p style="font-size:0.72em;" class="card-text"><small class="text-muted">Last online 3 mins ago</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 mb-lg-5 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-3.png" alt="" class="mr-2 rounded">
                <div style="width: 60px;height:25px;background:#ffc4c4;color:white;text-align: center;padding-top:0px;position: absolute;top:80px;" class="mr-2 rounded"><strong>O+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Nama : Ahmad Imron Sadar</strong>
                  <strong class="d-block text-secondary mb-2">Goldar : O+</strong>
                  -
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Sulawesi Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Join : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Online : 1 Minute ago</span>
                  <span class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-id-card text-info"></i></span>
                  <span class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-envelope text-success"></i></span>
                </p>
              </div>
              <div style="box-shadow: 3px 4px 9px #515151;" class="card d-none d-md-block d-lg-block">
                <div class="font-blood"><strong>O+</strong></div>
                <img width="100%" height="150px" class="card-img-top" src="<?php echo base_url(); ?>assets/img/person.png" alt="Card image cap">
                <div style="min-height:230px;text-align: center;" class="card-body pt-3">
                  <h5 style="margin-bottom:9px;font-size: 0.92em;" class="card-title">Handoko joko tinggori</h5>
                  <h6 style="font-size:0.75em;font-weight: 200;" class="card-subtitle mt-2 mb-2 text-muted">Lokasi : Kalimantan Selatan</h6>
                  <p class="card-text mb-0"><button type="button" class="btn btn-outline-success urgent-info mb-2"><i class="fas fa-envelope"></i></button></p>
                  <p class="mb-0"><a class="mb-0" style="font-size:0.70em;" href="<?php echo base_url(); ?>pendonor/detail">Lihat Detail</a></p>
                  <p style="font-size:0.72em;" class="card-text"><small class="text-muted">Last online 3 mins ago</small></p>
                </div>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 mb-lg-5 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-3.png" alt="" class="mr-2 rounded">
                <div style="width: 60px;height:25px;background:#ffc4c4;color:white;text-align: center;padding-top:0px;position: absolute;top:80px;" class="mr-2 rounded"><strong>AB+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Nama : Wahyudi</strong>
                  <strong class="d-block text-secondary mb-2">Goldar : AB+</strong>
                  Cinta sejati tak selalu berjalan mulus.
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Sulawesi Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Join : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Online : 1 Minute ago</span>
                  <span class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-id-card text-info"></i></span>
                  <span class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-envelope text-success"></i></span>
                </p>
              </div>
              <div style="box-shadow: 3px 4px 9px #515151;" class="card d-none d-md-block d-lg-block">
                <div class="font-blood"><strong>AB-</strong></div>
                <img width="100%" height="150px" class="card-img-top" src="<?php echo base_url(); ?>assets/img/user-6.png" alt="Card image cap">
                <div style="min-height:230px;text-align: center;" class="card-body pt-3">
                  <h5 style="margin-bottom:9px;font-size: 0.92em;" class="card-title">Sendiru</h5>
                  <h6 style="font-size:0.75em;font-weight: 200;" class="card-subtitle mt-2 mb-2 text-muted">Lokasi : Nanggroe aceh Darussalam</h6>
                  <p class="card-text mb-0"><button type="button" class="btn btn-outline-success urgent-info mb-2"><i class="fas fa-envelope"></i></button></p>
                  <p class="mb-0"><a class="mb-0" style="font-size:0.70em;" href="<?php echo base_url(); ?>pendonor/detail">Lihat Detail</a></p>
                  <p style="font-size:0.72em;" class="card-text"><small class="text-muted">Last online 3 mins ago</small></p>
                </div>
              </div>
            </div>

            <div class="col-lg-2 col-md-4 mb-lg-5 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-4.png" alt="" class="mr-2 rounded">
                <div style="width: 60px;height:25px;background:#ffc4c4;color:white;text-align: center;padding-top:0px;position: absolute;top:80px;" class="mr-2 rounded"><strong>O+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Nama : Ahmad Imron Sadar</strong>
                  <strong class="d-block text-secondary mb-2">Goldar : O+</strong>
                  -
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Sulawesi Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Join : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Online : 1 Minute ago</span>
                  <span class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-id-card text-info"></i></span>
                  <span class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-envelope text-success"></i></span>
                </p>
              </div>
              <div style="box-shadow: 3px 4px 9px #515151;" class="card d-none d-md-block d-lg-block">
                <!-- <div style="position: absolute;top:0;left:0;background:black;color:white;padding:7px 10px 7px 10px;"><strong>A+</strong></div> -->
                <img width="100%" height="150px" class="card-img-top" src="<?php echo base_url(); ?>assets/img/user-2.jpg" alt="Card image cap">
                <div style="min-height:190px;text-align: center;" class="card-body pt-3">
                  <h5 style="margin-bottom:9px;font-size: 0.92em;" class="card-title">Suryadaramadi Sadar</h5>
                  <h6 style="font-size:0.75em;font-weight: 200;" class="card-subtitle mt-2 mb-2 text-muted">Lokasi : Sulawesi Selatan</h6>
                  <p class="card-text mb-0">
                    <button type="button" class="btn btn-outline-danger urgent-info mb-0"><i class="fas fa-tint mr-1"></i> A+</button>
                  </p>
                  <p class="mb-0"><a class="mb-0" style="font-size:0.70em;" href="<?php echo base_url(); ?>pendonor/detail">Lihat Detail</a></p>
                  <p style="font-size:0.72em;" class="card-text"><small class="text-muted">Last online 3 mins ago</small></p>
                </div>
              </div>
            </div>
            
            <div style="text-align: center;" class="col-lg-12 mb-5 mt-3">
              <button style="border-radius: 20px;" type="button" class="btn btn-sm btn-danger px-4"><i style="color:white;" class="fas fa-expand mr-2"></i> Lebih banyak</button>
            </div>
          </div>
          
        </div>
      </div>
    </main>

    <footer style="margin-top:50px;padding-top: 11px;" class="text-muted">
      <div class="container">
        <img class="float-right" width="50px" src="<?php echo base_url(); ?>assets/img/Guard-Dark.png">
        <!-- <p class="float-right">
          <a href="#"><i class="fas fa-tint"></i></a>
        </p> -->
        <a href="#"><i style="font-size: 1.55em;color:#174a9b;" class="fab fa-facebook-square mr-2"></i></a>
        <a href="#"><i style="font-size: 1.55em;" class="fab fa-twitter mr-2"></i></a>
        <a href="#"><i style="font-size: 1.55em;color:red;" class="fab fa-youtube mr-2"></i></a>
        <p>Banyak hal diluar sana yang perlu di explore. Berkelilinglah, rasakan keagungan Tuhan.</p>
        <p>2018 &copy; Sipatuo Team, <i class="fas fa-heart"></i> <i class="fas fa-smile"></i></p>
      </div>
    </footer>
<?php
	$multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
	echo assets_js($multiple_js);
?>
</body>
</html>