<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sipatuo - Detail Pendonor</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
	<?php
		$multiple_css = array('all.css','bootstrap.min.css','offcanvas.css');
		echo assets_css($multiple_css);
	?>
</head>
<body class="bg-light">
	<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <?php $this->load->view('common/navbar_title_view'); ?>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>"><i class="fas fa-smile-beam"></i> <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>campaign">Campaign</a>
          </li>
          <li class="nav-item active mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>pendonor">Pendonor</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>events">Kegiatan</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>signup"><i class="fas fa-user-plus"></i></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a alt="Login" title="Login" class="nav-link" href="<?php echo base_url(); ?>signin"><i class="fas fa-door-open"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="container-fluid">

      <div style="min-height:800px;" class="row">
        <div class="col-lg-8 pb-3 bg-light">
          <div class="col-lg-12 pt-5 text-center">
            <p style="color:#c80d0d;text-shadow: 2px 2px 12px white;font-size: 2.33em;" class="display-4 mb-5 mt-0"><strong>#detail</strong>Pendonor</p>
          </div>
          <div class="col-lg-12">
            <div class="row justify-content-center">
              
              <div class="col-lg-4 pb-3 mr-2">
                <div class="row">
                  <div style="min-height:200px;" class="col-lg-12 p-2 bg-white">
                    <!-- <span style="font-size: 0.88em;position:absolute;top:5px;left:10px;color:black;">Gambar :</span> -->
                    <img class="img-fluid" src="<?php echo base_url(); ?>assets/img/user-1.png">
                    <!-- <span class="btn btn-outline-danger btn-sm mb-2">Viewer : 0</span> -->
                    <!-- <span class="btn btn-outline-info btn-sm mb-2">Join : 0</span> -->
                  </div>
                  <div style="word-wrap: break-word;border-radius: 10px;" class="col-lg-12 bg-primary">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:white;">Bio / Motto :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead text-light p-2 pb-3">Bahagia itu sederhana, sesederhana perasaan ini yang mudah jatuh karenamu.
                  </p>
                  </div>
                </div>
              </div>
              <div style="min-height:500px;" class="col-lg-5 pb-3">
                <div class="row">
                  <div style="border-radius: 10px;" class="col-lg-12 bg-white mb-1">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:black;">Nama :</span>
                    <p style="font-size: 1.1em;margin-bottom: 0px;padding-top:25px !important;" class="lead text-center p-2 pb-4"><strong>Suryadarmadi.</strong></p>
                  </div>
                  <div style="border-radius: 10px;" class="col-lg-12 bg-warning mb-1">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:black;">Golongan Darah :</span>
                    <p style="font-size: 1.1em;margin-bottom: 0px;padding-top:25px !important;" class="lead text-light text-center p-2 pb-4">A Rhesus + (Positif)</p>
                  </div>
                  <div style="border-radius: 10px;" class="col-lg-12 bg-info mb-1">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:black;">Lokasi :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:25px !important;" class="lead text-light text-center p-2 pb-4">Indonesia, Sulawesi Selatan, Makassar</p>
                  </div>
                  <div style="border-radius: 10px;" class="col-lg-12 bg-danger mb-1">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:black;">Email :</span>
                    <p style="font-size: 1.2em;margin-bottom: 0px;padding-top:25px !important;" class="lead text-light text-center p-2 pb-4">suryadarmadi@gmail.com</p>
                  </div>
                  <div style="border-radius: 10px;" class="col-lg-12 bg-secondary mb-1">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:black;">Phone :</span>
                    <p style="font-size: 1.2em;margin-bottom: 0px;padding-top:25px !important;" class="lead text-light text-center p-2 pb-4">081 111 111 111</p>
                  </div>
                  <div style="border-radius: 10px;background: #ffb5b5;" class="col-lg-12 mb-1">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:black;">Status :</span>
                    <p style="font-size: 0.88em;margin-bottom: 0px;padding-top:25px !important;" class="lead text-center p-2 pb-4">
                      Bergabung pada 12 Juli 2018. <br/>
                      Terakhir online 3 Menit yang lalu.
                    </p>
                  </div>
                  <div style="border-radius: 10px;" style="" class="col-lg-12 bg-white mb-1">
                    <span style="font-size: 0.88em;position:absolute;top:5px;left:13px;color:black;">Sosial Media :</span>
                    <p style="font-size: 1em;margin-bottom: 0px;padding-top:30px !important;" class="lead text-center p-2 pb-3">
                      <span style="border-radius:25px;" class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-line text-success"></i> line_id</span>
                      <span style="border-radius:25px;" class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-facebook-square text-primary"></i> facebook_id</span>
                      <span style="border-radius:25px;" class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-blackberry text-danger"></i> BBM_id</span>
                      <span style="border-radius:25px;" class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-whatsapp"></i> whatsapp_no</span>
                      <span style="border-radius:25px;" class="btn btn-outline-dark btn-sm mb-2"><i class="fab fa-telegram"></i> telegram_id</span>
                    </p>
                  </div>
                  <div class="col-lg-12 text-center pt-3 pl-0">
                    <a style="border-radius:25px;" href="#" class="btn btn-dark btn-sm px-4 p-2"><i class="fas fa-bullhorn mr-2"></i> Minta Bantuan</a>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="col-lg-4 bg-white">
          <div class="row justify-content-center">
            <div class="my-3 p-3 rounded mt-5 ml-lg-3 bg-white">
              <a style="font-size:0.75em;" href="#" class="float-right">Lihat Semua</a>
              <h6 class="border-bottom border-gray pb-2 mb-0">Pendonor lainnya</h6>

              <div class="media text-white pt-3">
                <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 text-dark border-bottom border-gray">
                  <strong class="d-block text-dark">Ahmad Imron Sadar.</strong>
                  <span class="d-block text-secondary mb-2">Goldar : A Rhesus +</span>
                  Motto / Bio : -
                  <span style="font-size:0.95em;" class="d-block text-dark pt-2">Alamat : Makassar</span>
                  <span style="font-size:0.95em;" class="d-block text-dark">Terakhir Online : 3 Menit yang lalu</span>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>pendonor/detail" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                </p>
              </div>
              <div class="media text-white pt-3">
                <img data-src="holder.js/32x32?theme=thumb&bg=0ccadb&fg=0ccadb&size=1" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 text-dark border-bottom border-gray">
                  <strong class="d-block text-dark">Wahyudi</strong>
                  <span class="d-block text-secondary mb-2">Goldar : O Rhesus +</span>
                  Motto / Bio : Jangan pernah berhenti berbuat baik kepada sesama manusia dan alam semesta.
                  <span style="font-size:0.95em;" class="d-block text-dark pt-2">Alamat : Makassar</span>
                  <span style="font-size:0.95em;" class="d-block text-dark">Terakhir Online : 2 Tahun yang lalu</span>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>pendonor/detail" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                </p>
              </div>
              <div class="media text-white pt-3">
                <img data-src="holder.js/32x32?theme=thumb&bg=6f42c1&fg=6f42c1&size=1" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 text-dark border-bottom border-gray">
                  <strong class="d-block text-dark">Handiko Prasetri Mandire</strong>
                  <span class="d-block text-secondary mb-2">Goldar : AB</span>
                  Motto / Bio : Jangan pernah berhenti berbuat baik kepada sesama manusia dan alam semesta.
                  <span style="font-size:0.95em;" class="d-block text-dark pt-2">Alamat : Kamlimantan Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-dark">Terakhir Online : 2 Tahun yang lalu</span>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>pendonor/detail" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                </p>
              </div>
              <div class="media text-white pt-3">
                <img data-src="holder.js/32x32?theme=thumb&bg=dbbb0c&fg=dbbb0c&size=1" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 text-dark border-bottom border-gray">
                  <strong class="d-block text-dark">Rangga sentanu pergunibo</strong>
                  <span class="d-block text-secondary mb-2">Goldar : A</span>
                  Motto / Bio : Bahagia itu sederhana.
                  <span style="font-size:0.95em;" class="d-block text-dark pt-2">Alamat : Jakarta Pusat</span>
                  <span style="font-size:0.95em;" class="d-block text-dark">Terakhir Online : 1 Tahun yang lalu</span>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>pendonor/detail" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                </p>
              </div>
              <div class="media text-white pt-3">
                <img data-src="holder.js/32x32?theme=thumb&bg=aad005&fg=aad005&size=1" alt="" class="mr-2 rounded">
                <p class="media-body pb-3 mb-0 small lh-125 text-dark border-bottom border-gray">
                  <strong class="d-block text-dark">Relangga Persutopo Asdio</strong>
                  <span class="d-block text-secondary mb-2">Goldar : A</span>
                  Motto / Bio : Pendukung Kebaikan.
                  <span style="font-size:0.95em;" class="d-block text-dark pt-2">Alamat : Sumatra Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-dark">Terakhir Online : 2 Tahun yang lalu</span>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>pendonor/detail" class="btn btn-sm btn-danger ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                </p>
              </div>
              <small class="d-block text-right mt-3 mb-3">
                <a class="text-dark" href="<?php echo base_url(); ?>pendonor">Lihat Semua Pendonor</a>
              </small>
            </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-center box-gabung-sipper">
        <div class="col-lg-12 mt-5 text-gabung-donor">
          <h1 style="font-size:2em;color:black;" class="display-4"><strong>Ayo </strong>Bergabung bersama kami</h1>
          <h1 style="font-size:1em;color:#153b77;" class="display-4">Gabung, temukan, minta bantuan dan bantu teman yang membutuhkan bantuan anda.</h1>
        </div>
        <div style="text-align: center;" class="col-lg-6">
          <a style="border-radius:20px;" href="<?php echo base_url(); ?>signup" class="btn btn-outline-warning px-lg-5 btn-lg-lg">Daftar</a> <span class="px-lg-2">atau</span>
          <a style="border-radius:20px;" href="<?php echo base_url(); ?>signin" class="btn btn-outline-info px-lg-5 btn-lg-lg">Login</a>
        </div>
      </div>
    </main>

    <?php $this->load->view('common/footer_view'); ?>
    
<?php
	$multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
	echo assets_js($multiple_js);
?>
</body>
</html>