<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sipatuo - Urgent</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
	<?php
		$multiple_css = array('all.css','bootstrap.min.css','offcanvas.css');
		echo assets_css($multiple_css);
	?>
</head>
<body class="bg-light">
	<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <?php $this->load->view('common/navbar_title_view'); ?>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>"><i class="fas fa-smile-beam"></i> <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>campaign">Campaign</a>
          </li>
          <li class="nav-item active mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>pendonor">Pendonor</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>events">Kegiatan</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>signup"><i class="fas fa-user-plus"></i></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a alt="Login" title="Login" class="nav-link" href="<?php echo base_url(); ?>signin"><i class="fas fa-door-open"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <?php $datadonor = 1; ?>
    <main role="main" class="container-fluid">
      <div style="min-height:600px;" class="row justify-content-center bg-white">
        <div class="col-lg-10">
          <div class="row justify-content-center pt-4">
            <?php if($datadonor > 0){ ?>

            <div style="text-align: center;" class="col-lg-12 mt-5 mb-4 d-none d-md-block d-lg-block">
              <h1 style="font-size:2.5em;color:black;" class="display-4"><strong>#daftar</strong>Pendonor</h1>
            </div>
            <div style="text-align: center;" class="col-lg-12 pb-5 d-none d-md-block d-lg-block">
              <a class="text-dark" href="#"><i style="font-size:1.3em;" class="fas fa-search mr-2"></i></a>
              <a class="text-dark" href="#"><i style="font-size:1.3em;" class="fas fa-plus-circle"></i></a>
            </div>
            <div class="col-lg-12 d-lg-none d-md-none">
              <div class="float-right"><a style="font-size:1.15em;" href="#" class="text-success mr-1"><i class="fas fa-plus-circle"></i></a> <a style="font-size:1.15em;" href="#" class="text-dark"><i class="fas fa-search"></i></a></div>
              <h6 style="" class="border-bottom border-gray pb-3 mb-0">Daftar Pendonor</h6>
            </div>

            <div class="col-lg-3 col-md-4 mb-lg-5 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img style="border:solid 1px #efefef" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-2.jpg" alt="" class="mr-2 rounded">
                <div style="width: 60px;height:25px;background:#ffc4c4;color:white;text-align: center;padding-top:0px;position: absolute;top:80px;" class="mr-2 rounded"><strong>A+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Nama : Suryadarmadi</strong>
                  <strong class="d-block text-secondary mb-2">Goldar : A+</strong>
                  Bahagia itu sederhana, sesederhana perasaan ini yang mudah jatuh karenamu.
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Sulawesi Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Join : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Online : 1 Minute ago</span>
                  <a href="<?php echo base_url(); ?>pendonor/detail" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-id-card text-info"></i></a>
                  <a href="<?php echo base_url(); ?>" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-envelope text-success"></i></a>
                </p>
              </div>
              <div style="" class="card text-center d-none d-md-block d-lg-block">
                <img style="width:100px;height:100px;" class="rounded-circle mt-3" src="<?php echo base_url(); ?>assets/img/user-2.jpg" alt="Card image cap">
                <div style="min-height:170px;text-align: center;" class="card-body pt-3">
                  <h5 style="margin-bottom:5px;font-size: 0.95em;" class="card-title"><a class="text-dark" href="<?php echo base_url(); ?>pendonor/detail">Mayrani Arrang Wulandari </a></h5>
                  <span class="btn btn-danger urgent-info py-0 mb-2">Goldar : A+</span>
                  <p style="font-size:0.8em;color:#7c7b7b;">"Lorem ipsum dolor sit amet, consectetur adipisicing elit."</p>
                  <p style="font-size:0.85em;" class="card-text mt-0"><small class="text-muted mt-0">Last online 3 mins ago</small></p>

                  <a href="#"><i class="far fa-address-book mr-2"></i></a>
                  <a href="#"><i class="far fa-envelope text-danger mr-2"></i></a>
                  <a href="#"><i class="fas fa-map-marker-alt text-dark"></i></a>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 mb-lg-5 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <div style="width: 60px;height:60px;background:#918f8f;color:white;text-align: center;padding-top:17px;" class="mr-2 rounded"><strong>AIM</strong></div>
                <div style="width: 60px;height:25px;background:#ffc4c4;color:white;text-align: center;padding-top:0px;position: absolute;top:80px;" class="mr-2 rounded"><strong>O+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Nama : Ahmad Imron Sadar</strong>
                  <strong class="d-block text-secondary mb-2">Goldar : O+</strong>
                  Celahmu akan dianggap sempurna oleh hati yang memang ditakdirkan untukmu.
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Sulawesi Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Join : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Online : 1 Minute ago</span>
                  <a href="<?php echo base_url(); ?>pendonor/detail" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-id-card text-info"></i></a>
                  <a href="<?php echo base_url(); ?>" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-envelope text-success"></i></a>
                </p>
              </div>
              <div style="" class="card text-center d-none d-md-block d-lg-block">
                <img style="width:100px;height:100px;" class="rounded-circle mt-3" src="<?php echo base_url(); ?>assets/img/user-1.png" alt="Card image cap">
                <div style="min-height:170px;text-align: center;" class="card-body pt-3">
                  <h5 style="margin-bottom:5px;font-size: 0.95em;" class="card-title"><a class="text-dark" href="<?php echo base_url(); ?>pendonor/detail">Ahmad Imron Sadar </a></h5>
                  <span class="btn btn-danger urgent-info py-0 mb-2">Goldar : A+</span>
                  <p style="font-size:0.8em;color:#7c7b7b;">"Lorem ipsum dolor sit amet, consectetur adipisicing elit."</p>
                  <p style="font-size:0.85em;" class="card-text mt-0"><small class="text-muted mt-0">Last online 13 mins ago</small></p>

                  <a href="#"><i class="far fa-address-book mr-2"></i></a>
                  <a href="#"><i class="far fa-envelope text-danger mr-2"></i></a>
                  <a href="#"><i class="fas fa-map-marker-alt text-dark"></i></a>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 mb-lg-5 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <div style="width: 60px;height:60px;background:#918f8f;color:white;text-align: center;padding-top:17px;" class="mr-2 rounded"><strong>MM</strong></div>
                <div style="width: 60px;height:25px;background:#ffc4c4;color:white;text-align: center;padding-top:0px;position: absolute;top:80px;" class="mr-2 rounded"><strong>O+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Nama : Manda Munda</strong>
                  <strong class="d-block text-secondary mb-2">Goldar : O+</strong>
                  -
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Sulawesi Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Join : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Online : 1 Minute ago</span>
                  <a href="<?php echo base_url(); ?>pendonor/detail" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-id-card text-info"></i></a>
                  <a href="<?php echo base_url(); ?>" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-envelope text-success"></i></a>
                </p>
              </div>
              <div style="" class="card text-center d-none d-md-block d-lg-block">
                <img style="width:100px;height:100px;" class="rounded-circle mt-3" src="<?php echo base_url(); ?>assets/img/user-3.png" alt="Card image cap">
                <div style="min-height:170px;text-align: center;" class="card-body pt-3">
                  <h5 style="margin-bottom:5px;font-size: 0.95em;" class="card-title"><a class="text-dark" href="<?php echo base_url(); ?>pendonor/detail">Suryadarmadi Sadar </a></h5>
                  <span class="btn btn-danger urgent-info py-0 mb-2">Goldar : AB+</span>
                  <p style="font-size:0.8em;color:#7c7b7b;">"Lorem ipsum dolor sit amet, consectetur adipisicing elit."</p>
                  <p style="font-size:0.85em;" class="card-text mt-0"><small class="text-muted mt-0">Last online 35 mins ago</small></p>

                  <a href="#"><i class="far fa-address-book mr-2"></i></a>
                  <a href="#"><i class="far fa-envelope text-danger mr-2"></i></a>
                  <a href="#"><i class="fas fa-map-marker-alt text-dark"></i></a>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 mb-lg-5 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <div style="width: 60px;height:60px;background:#918f8f;color:white;text-align: center;padding-top:17px;" class="mr-2 rounded"><strong>WS</strong></div>
                <div style="width: 60px;height:25px;background:#ffc4c4;color:white;text-align: center;padding-top:0px;position: absolute;top:80px;" class="mr-2 rounded"><strong>O+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Nama : Wahyudi Sadar</strong>
                  <strong class="d-block text-secondary mb-2">Goldar : O+</strong>
                  -
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Sulawesi Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Join : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Online : 1 Minute ago</span>
                  <a href="<?php echo base_url(); ?>pendonor/detail" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-id-card text-info"></i></a>
                  <a href="<?php echo base_url(); ?>" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-envelope text-success"></i></a>
                </p>
              </div>
              <div style="" class="card text-center d-none d-md-block d-lg-block">
                <img style="width:100px;height:100px;" class="rounded-circle mt-3" src="<?php echo base_url(); ?>assets/img/user-4.png" alt="Card image cap">
                <div style="min-height:170px;text-align: center;" class="card-body pt-3">
                  <h5 style="margin-bottom:5px;font-size: 0.95em;" class="card-title"><a class="text-dark" href="<?php echo base_url(); ?>pendonor/detail">Wahyudi Sadar </a></h5>
                  <span class="btn btn-danger urgent-info py-0 mb-2">Goldar : AB+</span>
                  <p style="font-size:0.8em;color:#7c7b7b;">"Lorem ipsum dolor sit amet, consectetur adipisicing elit."</p>
                  <p style="font-size:0.85em;" class="card-text mt-0"><small class="text-muted mt-0">Last online 35 mins ago</small></p>

                  <a href="#"><i class="far fa-address-book mr-2"></i></a>
                  <a href="#"><i class="far fa-envelope text-danger mr-2"></i></a>
                  <a href="#"><i class="fas fa-map-marker-alt text-dark"></i></a>
                </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-4 mb-lg-5 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-3.png" alt="" class="mr-2 rounded">
                <div style="width: 60px;height:25px;background:#ffc4c4;color:white;text-align: center;padding-top:0px;position: absolute;top:80px;" class="mr-2 rounded"><strong>O+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Nama : Ahmad Imron Sadar</strong>
                  <strong class="d-block text-secondary mb-2">Goldar : O+</strong>
                  -
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Sulawesi Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Join : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Online : 1 Minute ago</span>
                  <a href="<?php echo base_url(); ?>pendonor/detail" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-id-card text-info"></i></a>
                  <a href="<?php echo base_url(); ?>" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-envelope text-success"></i></a>
                </p>
              </div>
              <div class="card text-center d-none d-md-block d-lg-block">
                <img style="width:100px;height:100px;" class="rounded-circle mt-3" src="<?php echo base_url(); ?>assets/img/user-4.png" alt="Card image cap">
                <div style="min-height:170px;text-align: center;" class="card-body pt-3">
                  <h5 style="margin-bottom:5px;font-size: 0.95em;" class="card-title"><a class="text-dark" href="<?php echo base_url(); ?>pendonor/detail">Wahyudi Sadar </a></h5>
                  <span class="btn btn-danger urgent-info py-0 mb-2">Goldar : AB+</span>
                  <p style="font-size:0.8em;color:#7c7b7b;">"Lorem ipsum dolor sit amet, consectetur adipisicing elit."</p>
                  <p style="font-size:0.85em;" class="card-text mt-0"><small class="text-muted mt-0">Last online 35 mins ago</small></p>

                  <a href="#"><i class="far fa-address-book mr-2"></i></a>
                  <a href="#"><i class="far fa-envelope text-danger mr-2"></i></a>
                  <a href="#"><i class="fas fa-map-marker-alt text-dark"></i></a>
                </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-4 mb-lg-5 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-3.png" alt="" class="mr-2 rounded">
                <div style="width: 60px;height:25px;background:#ffc4c4;color:white;text-align: center;padding-top:0px;position: absolute;top:80px;" class="mr-2 rounded"><strong>O+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Nama : Ahmad Imron Sadar</strong>
                  <strong class="d-block text-secondary mb-2">Goldar : O+</strong>
                  -
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Sulawesi Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Join : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Online : 1 Minute ago</span>
                  <a href="<?php echo base_url(); ?>pendonor/detail" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-id-card text-info"></i></a>
                  <a href="<?php echo base_url(); ?>" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-envelope text-success"></i></a>
                </p>
              </div>
              <div class="card text-center d-none d-md-block d-lg-block">
                <img style="width:100px;height:100px;" class="rounded-circle mt-3" src="<?php echo base_url(); ?>assets/img/user-4.png" alt="Card image cap">
                <div style="min-height:170px;text-align: center;" class="card-body pt-3">
                  <h5 style="margin-bottom:5px;font-size: 0.95em;" class="card-title"><a class="text-dark" href="<?php echo base_url(); ?>pendonor/detail">Wahyudi Sadar </a></h5>
                  <span class="btn btn-danger urgent-info py-0 mb-2">Goldar : AB+</span>
                  <p style="font-size:0.8em;color:#7c7b7b;">"Lorem ipsum dolor sit amet, consectetur adipisicing elit."</p>
                  <p style="font-size:0.85em;" class="card-text mt-0"><small class="text-muted mt-0">Last online 35 mins ago</small></p>

                  <a href="#"><i class="far fa-address-book mr-2"></i></a>
                  <a href="#"><i class="far fa-envelope text-danger mr-2"></i></a>
                  <a href="#"><i class="fas fa-map-marker-alt text-dark"></i></a>
                </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-4 mb-lg-5 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-3.png" alt="" class="mr-2 rounded">
                <div style="width: 60px;height:25px;background:#ffc4c4;color:white;text-align: center;padding-top:0px;position: absolute;top:80px;" class="mr-2 rounded"><strong>O+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Nama : Ahmad Imron Sadar</strong>
                  <strong class="d-block text-secondary mb-2">Goldar : O+</strong>
                  -
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Sulawesi Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Join : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Online : 1 Minute ago</span>
                  <a href="<?php echo base_url(); ?>pendonor/detail" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-id-card text-info"></i></a>
                  <a href="<?php echo base_url(); ?>" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-envelope text-success"></i></a>
                </p>
              </div>
              <div class="card text-center d-none d-md-block d-lg-block">
                <img style="width:100px;height:100px;" class="rounded-circle mt-3" src="<?php echo base_url(); ?>assets/img/user-4.png" alt="Card image cap">
                <div style="min-height:170px;text-align: center;" class="card-body pt-3">
                  <h5 style="margin-bottom:5px;font-size: 0.95em;" class="card-title"><a class="text-dark" href="<?php echo base_url(); ?>pendonor/detail">Wahyudi Sadar </a></h5>
                  <span class="btn btn-danger urgent-info py-0 mb-2">Goldar : AB+</span>
                  <p style="font-size:0.8em;color:#7c7b7b;">"Lorem ipsum dolor sit amet, consectetur adipisicing elit."</p>
                  <p style="font-size:0.85em;" class="card-text mt-0"><small class="text-muted mt-0">Last online 35 mins ago</small></p>

                  <a href="#"><i class="far fa-address-book mr-2"></i></a>
                  <a href="#"><i class="far fa-envelope text-danger mr-2"></i></a>
                  <a href="#"><i class="fas fa-map-marker-alt text-dark"></i></a>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 mb-lg-5 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <img width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-3.png" alt="" class="mr-2 rounded">
                <div style="width: 60px;height:25px;background:#ffc4c4;color:white;text-align: center;padding-top:0px;position: absolute;top:80px;" class="mr-2 rounded"><strong>O+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Nama : Ahmad Imron Sadar</strong>
                  <strong class="d-block text-secondary mb-2">Goldar : O+</strong>
                  -
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Sulawesi Selatan</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Join : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Online : 1 Minute ago</span>
                  <a href="<?php echo base_url(); ?>pendonor/detail" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-id-card text-info"></i></a>
                  <a href="<?php echo base_url(); ?>" class="btn btn-outline-secondary btn-sm mt-2"><i class="far fa-envelope text-success"></i></a>
                </p>
              </div>
              <div style="background: #ededed;border:none;" class="card text-center d-none d-md-block d-lg-block">
                <img style="width:100px;height:100px;" class="rounded-circle mt-3" src="<?php echo base_url(); ?>assets/img/bg-white-100.jpg" alt="Card image cap">
                <div style="min-height:170px;text-align: center;" class="card-body pt-3">
                  <h5 style="margin-bottom:5px;font-size: 0.95em;" class="card-title"><a class="text-dark" href="<?php echo base_url(); ?>pendonor/detail">Donate Name </a></h5>
                  <span style="background:#ccc9c9;color:#ccc9c9;border:none;" class="btn btn-danger urgent-info py-0 mb-2">Goldar : AB+</span>
                  <p style="font-size:0.8em;color:#7c7b7b;">"Lorem ipsum dolor sit amet, consectetur adipisicing elit."</p>
                  <p style="font-size:0.85em;" class="card-text mt-0"><small class="text-muted mt-0">Last online 0 mins ago</small></p>

                  <a href="#"><i style="color:#ccc9c9;" class="far fa-address-book mr-2"></i></a>
                  <a href="#"><i style="color:#ccc9c9;" class="far fa-envelope mr-2"></i></a>
                  <a href="#"><i style="color:#ccc9c9;" class="fas fa-map-marker-alt"></i></a>
                </div>
              </div>
            </div>
           
            <div style="text-align: center;" class="col-lg-12 mb-5 mt-3">
              <button style="border-radius: 20px;" type="button" class="btn btn-sm btn-danger px-4"><i style="color:white;" class="fas fa-expand mr-2"></i> Lebih banyak</button>
            </div>

            <?php } else { ?>
              <div class="col-lg-6 mt-5 mb-5">
                <div class="jumbotron text-center pt-5">
                    <img width="120px" src="<?php echo base_url(); ?>assets/img/help.png" alt="">
                    <h1 style="font-size:1.7em;" class="display-4 mb-4">Belum ada data <strong style="color:#d66604;">pendonor</strong>.</h1>
                    <p style="font-size:1.1em;" class="lead mb-4">Silahkan bergabung bersama kami untuk menjadi pendonor atau undang teman anda.</p>
                    <hr class="my-4">
                    <a style="border-radius: 20px;" href="<?php echo base_url(); ?>pendonor" class="btn btn-sm btn-warning mb-2 px-4"><i style="color:white;" class="fas fa-plus-circle mr-2"></i> Jadi Pendonor</a><br/>
                    <a style="border-radius: 20px;" href="<?php echo base_url(); ?>pendonor/undang" class="btn btn-sm btn-info px-4"><i class="far fa-user mr-2"></i>Undang teman</a> 
                </div>
              </div>
            <?php } ?>
          </div>
          
        </div>
      </div>
    </main>

    <?php $this->load->view('common/footer_view'); ?>
    
<?php
	$multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
	echo assets_js($multiple_js);
?>
</body>
</html>