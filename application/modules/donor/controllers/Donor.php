<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Donor extends MX_Controller {
	function __construct()
	{
		parent::__construct();
	}

	function pendonor()
	{
		$this->load->view('view_listpendonorpage');
	}

	function detail()
	{
		$this->load->view('view_detailpendonorpage');
	}
}