<?php
$cek = Modules::run('common/check_login');
if($cek == 0 ){
    echo '<div class="ui massive top fixed menu">';
} else {
    echo '<div class="ui massive top fixed inverted menu">';
}
?>
<div class="ui container">
    <a style="padding-top:25px;" class="toc item">
        <i class="sidebar icon"></i>
    </a>
    <a href="<?php echo base_url(); ?>" class="item logo_hov hide_sm"><img src="<?php echo base_url(); ?>assets/img/support/menu_jambuka.png"></a>
    <?php
    $cek = Modules::run('common/check_login');
    if($cek == 0 ){
        echo '<span style="padding-top:25px;padding-left:15px;color:black" id="title" class="hide_xl"></span>';
    } else {
        echo '<span style="padding-top:25px;padding-left:15px;color:white" id="title" class="hide_xl"></span>';
    }
    ?>
    <!-- <div class="ui compact menu"> -->
        <div class="ui simple dropdown item hide_sm">
            Daftar
            <i class="dropdown icon"></i>
            <div class="menu">
                <a href="<?php echo base_url(); ?>jam" class="item hide_sm">Jam Buka</a>
                <a href="<?php echo base_url(); ?>kategori" class="item hide_sm">Kategori</a>
                <a href="<?php echo base_url(); ?>libur" class="item hide_sm">Libur</a>
            </div>
        </div>
    <!-- </div> -->
    <!-- <a href="<?php echo base_url(); ?>jam" class="item hide_sm">Daftar Jam</a> -->
    <!-- <a href="<?php echo base_url(); ?>kategori" class="item hide_sm">Kategori</a> -->
    <div style="width:40%;" id="search_tb" class="item hide_sm hide_med">
        <form method="POST" action="<?php echo base_url(); ?>search">
            <div class="ui action input">
            <?php
            //$cek = Modules::run('common/check_login');
            //if($cek == 0 ){
            //    echo '<div class="ui action input">';
            //} else {
            //    echo ' <div class="ui action input">';
            //}
            ?>
            
                <input required id="keyword" name="search_data" type="text" value='<?php if(isset($key)){echo $key; } ?>' placeholder="Cari Tempat">
                <button type="submit" class="ui icon grey medium button"><i class="search icon"></i></button>
            
        </div>
        </form>
    </div>
    <div class="right menu hide_sm">
        <?php
        $cek = Modules::run('common/check_login');
        if($cek == 0){
            echo '
                <a href="'.base_url().'login" class="item">Masuk</a>
                <a href="'.base_url().'register" class="item">Registrasi</a>
            ';
        } else {
            echo '<a href="'.base_url().'tambah/jam" class="item"><i class="add circle icon"></i> Jaka</a>';
            echo '<a href="'.base_url().'profil/'.Modules::run('common/get_username_login').'" class="item">Profil</a>';
            echo '<a href="'.base_url().'logout" class="item">Keluar</a>';
        }
        ?>
      
    </div>
    <div class="right menu hide_xl">
        <?php
        $cek = Modules::run('common/check_login');
        if($cek == 0 ){
            echo '<a href="'.base_url().'" class="item"><img src="'.base_url().'assets/img/support/clock-grey.png"></a>';
        } else {
            echo '<a href="'.base_url().'" class="item"><img src="'.base_url().'assets/img/support/clock-red.png"></a>';
        }
        ?>
        
    </div>
</div>
</div> <!-- end menu -->