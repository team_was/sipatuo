<?php
    echo css('hr_css/pnotify.custom.min.css');
    echo css('hr_css/semantic.min.css');
    echo css('hr_css/dataTables.semanticui.min.css');
    echo css('hr_css/bootstrap.min.css');

    echo css('hr_css/font-awesome.min.css');
    echo css('hr_css/ionicons.min.css');

    echo css('hr_css/style.css');
    echo css('hr_css/jquery-confirm.css');
    echo css('select2.css');
    echo css('select2-bootstrap.css');


    echo js('hr_js/jquery-1.12.3.js');
    echo js('hr_js/pnotify.custom.min.js');
    echo js('hr_js/select2.js');
?>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
