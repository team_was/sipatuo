<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<meta name="keywords" content="Jam Buka, Jam Besuk, Jam Operasional, Open Hour, Jam Kerja, Jam Aktif" />
<meta name="description" content="Tempat anda menemukan jam buka, jam operasional, jam kerja suatu tempat atau lokasi di sekitar anda." />
<meta name="rating" content="General" />
<meta name="revisit-after" content="7 days" />
<meta name="classification" content="" />
<meta name="distribution" content="Global" />
<meta name="author" content="Imron Ahmad" />
<meta name="language" content="Indonesian" />
<meta name="publisher" content="SIPPO GROUP, Makassar, Indonesia" />
<meta name="robots" content="noindex,nofollow" />
<meta name="generator" content="jEdit 5.2.0" />
<link rel="icon" href="<?php echo base_url(); ?>/assets/images/favicon.png">
<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
<?php echo css('bootstrap.min.css'); ?>
