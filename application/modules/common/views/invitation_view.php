<div style="background:#f2f2f2;height:300px;" class="row justify-content-center pt-5">
  <div style="text-align: center;height:30px;" class="col-lg-12 mt-2">
    <h1 style="font-size:2em;color:black;" class="display-4"><strong>Undang </strong>teman anda</h1>
    <h1 style="font-size:1em;color:#153b77;" class="display-4">Undang teman anda untuk bergabung bersama sipatuo, semakin banyak yang bergabung semakin banyak kehidupan yang bisa kita selamatkan.</h1>
  </div>
  <div style="text-align: center;" class="col-lg-6">
    <form>
      <div class="form-row align-items-center">
        <div class="col-sm-9 my-1">
          <label class="sr-only" for="kontak">Kontak</label>
          <input style="border-radius: 25px;" type="text" class="form-control form-control-sm px-3 py-2" id="kontak" placeholder="Masukkan email atau nomor telepon teman anda">
        </div>
        <div class="col-auto my-1">
          <button style="border-radius: 25px;" type="submit" class="btn btn-primary btn-sm px-3 py-2">Send Invitation</button>
        </div>
      </div>
    </form>
  </div>
</div>