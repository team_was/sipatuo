<footer style="margin-top:50px;padding-top: 11px;" class="text-muted">
  <div class="container">
    <img class="float-right" width="50px" src="<?php echo base_url(); ?>assets/img/Guard-Dark-xs.png">
    <!-- <p class="float-right">
      <a href="#"><i class="fas fa-tint"></i></a>
    </p> -->
    <a href="#"><i style="font-size: 1.55em;color:#174a9b;" class="fab fa-facebook-square mr-2"></i></a>
    <a href="#"><i style="font-size: 1.55em;" class="fab fa-twitter mr-2"></i></a>
    <a href="#"><i style="font-size: 1.55em;color:red;" class="fab fa-youtube mr-2"></i></a>
    <p>Banyak hal diluar sana yang perlu di explore. Berkelilinglah, rasakan keagungan Tuhan.</p>
    <p>
      <a class="text-danger mr-3" href="#">Home</a>
      <a class="text-info mr-3" href="#">Campaign</a>
      <a class="text-success mr-3" href="#">Donor</a>
      <a class="text-dark mr-3" href="#">Event</a>
      <a class="text-warning mr-3" href="<?php echo base_url(); ?>privacy">Privasi</a>
      <a href="<?php echo base_url(); ?>faq">FAQ</a>
    </p>
    <p>2018 &copy; Sipatuo Team, <i class="fas fa-heart"></i> <i class="fas fa-smile"></i></p>
  </div>
</footer>