<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_common extends CI_Model {
	function __construct()
	{
		parent::__construct();
		
	}

	public function get_provinsi()
    {
        $this->db->order_by('nama_provinsi', 'asc');
        return $this->db->get('common_provinsi')->result();
    }

    public function get_kota()
    {
        // kita joinkan tabel kota dengan provinsi
        $this->db->join('common_provinsi', 'common_kabupaten.id_prov = common_provinsi.id_prov');
        //$this->db->order_by('nama', 'asc');
        return $this->db->get('common_kabupaten')->result();
    }

    function autocomplete_data_jaka($data)
    {
    	$this->db->select('jaka_header.id,jaka_header.slug,jaka_header.nama as nama_header,jaka_header.zona, jaka_header.alamat,jaka_kategori.nama as kategori');
    	$this->db->from('jaka_header');
    	$this->db->join('jaka_kategori','jaka_kategori.id = jaka_header.id_kategori');
    	$this->db->where('isDeleted', 0);
    	$this->db->like('jaka_header.nama',$data);
    	$this->db->or_like('jaka_kategori.nama',$data);
    	$this->db->limit(15);
    	$query = $this->db->get();
        return $query;
    }

    function autocomplete_data_kategori($data)
    {
    	$this->db->where('isDeleted', 0);
    	$this->db->like('nama',$data);
    	$this->db->or_like('shortname',$data);
    	$this->db->limit(15);
    	$query = $this->db->get('jaka_kategori');
        return $query;
    }

    function search_data_detail($data)
    {
    	$this->db->where('isDeleted', 0);
    	$this->db->like('nama',$data);
    	$this->db->limit(15);
    	$query = $this->db->get('jaka_header');
        return $query;
    }

    function search_data($keyword)
    {
    	$this->db->select('jaka_header.id,jaka_header.slug,jaka_header.nama as nama_header,jaka_header.zona, jaka_header.alamat,jaka_kategori.nama as kategori,common_provinsi.nama_provinsi,common_kabupaten.nama as kabupaten, jaka_user.nama as fullname, jaka_user.username');
    	$this->db->from('jaka_header');
    	$this->db->join('jaka_kategori','jaka_kategori.id = jaka_header.id_kategori');
    	$this->db->join('common_provinsi', 'common_provinsi.id_prov = jaka_header.provinsi');
    	$this->db->join('common_kabupaten', 'common_kabupaten.id_kab = jaka_header.kab_kota');
    	$this->db->join('jaka_user', 'jaka_user.id = jaka_header.input_by');
    	$this->db->where('isDeleted', 0);
    	$this->db->like('jaka_header.nama',$keyword);
    	// $this->db->or_like('jaka_user.nama',$keyword);
    	// $this->db->or_like('jaka_header.zona',$keyword);
    	$this->db->or_like('jaka_kategori.nama',$keyword);
    	// $this->db->or_like('common_provinsi.nama_provinsi',$keyword);
    	// $this->db->or_like('common_kabupaten.nama',$keyword);
    	// $this->db->or_like('jaka_user.username',$keyword);
    	$this->db->limit(15);

    	$query = $this->db->get();

    	return $query->result();
    }

    function count_search($keyword)
    {
    	$this->db->select('jaka_header.id,jaka_header.nama as nama_header,jaka_header.zona, jaka_header.alamat,jaka_kategori.nama as kategori,common_provinsi.nama_provinsi,common_kabupaten.nama as kabupaten, jaka_user.nama as fullname, jaka_user.username');
    	$this->db->from('jaka_header');
    	$this->db->join('jaka_kategori','jaka_kategori.id = jaka_header.id_kategori');
    	$this->db->join('common_provinsi', 'common_provinsi.id_prov = jaka_header.provinsi');
    	$this->db->join('common_kabupaten', 'common_kabupaten.id_kab = jaka_header.kab_kota');
    	$this->db->join('jaka_user', 'jaka_user.id = jaka_header.input_by');
    	$this->db->where('isDeleted', 0);
    	$this->db->like('jaka_header.nama',$keyword);
    	$this->db->or_like('jaka_kategori.nama',$keyword);
    	$this->db->limit(15);

    	$query = $this->db->get();

    	return $query->num_rows();
    }

    function search_data_kategori($keyword)
    {
    	$this->db->select('jaka_header.id,jaka_header.nama as nama_header,jaka_header.zona, jaka_header.alamat,jaka_kategori.nama as kategori,common_provinsi.nama_provinsi,common_kabupaten.nama as kabupaten, jaka_user.nama as fullname, jaka_user.username');
    	$this->db->from('jaka_header');
    	$this->db->join('jaka_kategori','jaka_kategori.id = jaka_header.id_kategori');
    	$this->db->join('common_provinsi', 'common_provinsi.id_prov = jaka_header.provinsi');
    	$this->db->join('common_kabupaten', 'common_kabupaten.id_kab = jaka_header.kab_kota');
    	$this->db->join('jaka_user', 'jaka_user.id = jaka_header.input_by');
    	$this->db->where('isDeleted', 0);
    	$this->db->like('jaka_header.nama',$keyword);
    	$this->db->limit(15);

    	$query = $this->db->get();

    	return $query->result();
    }

    function count_search_kategori($keyword)
    {
    	$this->db->select('jaka_header.id,jaka_header.nama as nama_header,jaka_header.zona, jaka_header.alamat,jaka_kategori.nama as kategori,common_provinsi.nama_provinsi,common_kabupaten.nama as kabupaten, jaka_user.nama as fullname, jaka_user.username');
    	$this->db->from('jaka_header');
    	$this->db->join('jaka_kategori','jaka_kategori.id = jaka_header.id_kategori');
    	$this->db->join('common_provinsi', 'common_provinsi.id_prov = jaka_header.provinsi');
    	$this->db->join('common_kabupaten', 'common_kabupaten.id_kab = jaka_header.kab_kota');
    	$this->db->join('jaka_user', 'jaka_user.id = jaka_header.input_by');
    	$this->db->where('isDeleted', 0);
    	$this->db->like('jaka_header.nama',$keyword);
    	$this->db->limit(15);
    	// $this->db->or_like('jaka_user.nama', $keyword);
    	// $this->db->or_like('jaka_header.zona', $keyword);
    	// $this->db->or_like('jaka_kategori.nama', $keyword);
    	// $this->db->or_like('common_provinsi.nama_provinsi', $keyword);
    	// $this->db->or_like('common_kabupaten.nama', $keyword);
    	// $this->db->or_like('jaka_user.username', $keyword);

    	$query = $this->db->get();

    	return $query->num_rows();
    }

	function get_by_table($table,$col)
	{
		$this->db->where('isDeleted', 0);
		$this->db->order_by($col, 'ASC');
		$query = $this->db->get($table);
        return $query;
	}

	public function get_jaka($page){
        $offset = 8*$page;
        $limit = 8;
        $sql = "SELECT * FROM jaka_header WHERE isDeleted = 0 ORDER BY date_input LIMIT $offset ,$limit";
        $result = $this->db->query($sql)->result();
        return $result;
	}

	public function get_jaka_kategori($page, $kategori_id){
        $offset = 8*$page;
        $limit = 8;
        $sql = "SELECT * FROM jaka_header WHERE isDeleted = 0 AND id_kategori = $kategori_id ORDER BY date_input LIMIT $offset ,$limit";
        $result = $this->db->query($sql)->result();
        return $result;
	}

	public function get_jaka_link_terkait($page, $kategori_id){
        $offset = 4*$page;
        $limit = 4;
        $sql = "SELECT * FROM jaka_header WHERE isDeleted = 0 AND id_kategori = $kategori_id ORDER BY date_input LIMIT $offset ,$limit";
        $result = $this->db->query($sql)->result();
        return $result;
	}

	public function get_jaka_dashboard($page){
        $offset = 4*$page;
        $limit = 4;
        $sql = "SELECT * FROM jaka_header WHERE isDeleted = 0 ORDER BY date_input LIMIT $offset ,$limit";
        $result = $this->db->query($sql)->result();
        return $result;
	}
	
	public function get_kategori($page){
        $offset = 12*$page;
        $limit = 12;
        $sql = "SELECT * FROM jaka_kategori ORDER BY nama LIMIT $offset ,$limit";
        $result = $this->db->query($sql)->result();
        return $result;
	}
	
	public function get_kategori_dashboard($page){
        $offset = 5*$page;
        $limit = 5;
        $sql = "SELECT * FROM jaka_kategori ORDER BY nama LIMIT $offset ,$limit";
        $result = $this->db->query($sql)->result();
        return $result;
    }

	function get_by_table_asc_no_del($table,$col)
	{
		$this->db->order_by($col, 'ASC');
		$query = $this->db->get($table);
        return $query;
	}

	function get_by_table_page($table,$col,$num,$offset)
	{
		$this->db->where('isDeleted', 0);
		$this->db->order_by($col, 'ASC');
		$query = $this->db->get($table,$num,$offset);
        return $query;
	}

	function get_by_table_or($table)
	{
		$query = $this->db->get($table);
        return $query;
	}

	function get_by_table_off($table,$col)
	{
        $this->db->where('isDeleted', 0);
        $this->db->order_by($col, 'ASC');
		$query = $this->db->get($table);
		return $query;
	}

	function get_by_table_desc($table,$col)
	{
		$this->db->where('isDeleted', 0);
		$this->db->order_by($col, 'DESC');
		$query = $this->db->get($table);
        return $query;
	}

	function get_by_table_desc_limit($table,$col,$limit)
	{
		$this->db->order_by($col, 'DESC');
		$this->db->limit($limit);
		$query = $this->db->get($table);
        return $query;
	}

	function get_where_custom_by_table($table,$col, $value)
	{
		$this->db->where($col, $value);
		$query = $this->db->get($table);
		return $query;
	}

	function get_where_custom_by_table_sort($table,$col,$value,$sortfield,$sort)
	{
		$this->db->order_by($sortfield, $sort);
		$this->db->where($col, $value);
		$query = $this->db->get($table);
		return $query;
	}

	function get_two_data($table,$col1,$col2,$value1,$value2)
	{
		$this->db->where($col1, $value1);
		$this->db->where($col2, $value2);
		$query = $this->db->get($table);
		return $query;
	}

	function get_three_data($table,$col1,$col2,$col3,$value1,$value2,$value3)
	{
		$this->db->where($col1, $value1);
		$this->db->where($col2, $value2);
		$this->db->where($col3, $value3);
		$query = $this->db->get($table);
		return $query;
	}

	function get_four_data($table,$col1,$col2,$col3,$col4,$value1,$value2,$value3,$value4)
	{
		$this->db->where($col1, $value1);
		$this->db->where($col2, $value2);
		$this->db->where($col3, $value3);
		$this->db->where($col4, $value4);
		$query = $this->db->get($table);
		return $query;
	}

	function get_where_id_by_table($table,$value)
	{
		$this->db->where('id', $value);
		$query = $this->db->get($table);
		return $query;
	}

	function _update_by_table($table,$id,$data)
	{
		$this->db->where('id', $id);
		$this->db->update($table, $data);
	}

	function _update_custom_table($table,$col,$val,$data)
	{
		$this->db->where($col, $val);
		$this->db->update($table, $data);
	}

	function update_two_criteria($table,$col1,$col2,$value1,$value2,$data)
	{
		$this->db->where($col1, $value1);
		$this->db->where($col2, $value2);
		$this->db->update($table, $data);
	}

	function _insert_by_table($table,$data)
	{
		$this->db->insert($table, $data);
	}

	function _delete_by_table($table,$id)
	{
		$data['isDeleted'] = 1;
		$this->db->where('id', $id);
		$this->db->update($table, $data);
	}

	function _delete_by_id($table,$id)
	{
		$this->db->where('id', $id);
		$this->db->delete($table);
	}

	function _delete_by_id_tf($table,$id)
	{
		$this->db->where('id', $id);
		if($this->db->delete($table)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function check_record_by_id_and_table($table,$id)
	{
		$this->db->where('id', $id);
		$this->db->from($table);
		return $this->db->count_all_results();
	}

	function check_user_by_password($table,$id,$pass)
	{
		$this->db->where('id', $id);
		$this->db->where('password', $pass);
		$this->db->from($table);
		return $this->db->count_all_results();
	}

	function check_three_data($table,$col1,$col2,$col3,$value1,$value2,$value3)
	{
		$this->db->where($col1, $value1);
		$this->db->where($col2, $value2);
		$this->db->where($col3, $value3);
		$this->db->from($table);
		return $this->db->count_all_results();
	}

	function check_two_data($table,$col1,$col2,$value1,$value2)
	{
		$this->db->where($col1, $value1);
		$this->db->where($col2, $value2);
		$this->db->from($table);
		return $this->db->count_all_results();
	}

	function check_one_data($table,$col1,$value1)
	{
		$this->db->where($col1, $value1);
		$this->db->from($table);
		return $this->db->count_all_results();
	}

	function check_custom_data($table,$data)
	{
		$this->db->where($data);
		$this->db->from($table);
		return $this->db->count_all_results();
	}

	function get_record_with_limit($table,$limit, $start)
	{
      $this->db->limit($limit, $start);
      $query = $this->db->get($table);
      if ($query->num_rows() > 0) {
        return $query->result_array();
      }
      return false;
    }

   	function total_count_by_table($table)
   	{
       return $this->db->count_all($table);
    }

    function total_count_by_table_ndel($table)
    {
       	$this->db->where('isDeleted', 0);
		$this->db->from($table);
		return $this->db->count_all_results();
    }

    function get_max_data($table,$field)
    {
    	$this->db->select_max($field);
		$query = $this->db->get($table);
		return $query;
    }

    function count_by_table_data($table,$data)
    {
    	$this->db->where($data);
    	$this->db->from($table);
    	return $this->db->count_all_results();
    }

	function get_custom_data($table,$data)
    {
    	$this->db->where($data);
			$query = $this->db->get($table);
			return $query;
    }

    function select_count_table($table)
    {
    	$this->db->select('SUM(count) AS total_rows', FALSE);
		$query = $this->db->get($table);
		return $query;
    }

    function check_kodeno($field, $table)
	{
		$this->db->select_max($field);
		$query = $this->db->get($table);
		return $query->result();
	}

	function pword_check($username, $passwd)
	{
		$this->db->where('nik', $username);
		$this->db->where('password', $passwd);
		$query = $this->db->get('tbl_karyawan');
		$num_rows = $query->num_rows();

		if($num_rows > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}