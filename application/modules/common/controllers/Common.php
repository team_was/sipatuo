<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common extends MX_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('mdl_common');
    }

    function check_before_action($table,$user,$id){
        Modules::run('security/make_sure_is_login');

        $cek_data = $this->check_one_data($table,'id',$id);
        if($cek_data == 1){
            $hari_detail = $this->get_where_custom_by_table($table,'id',$id);
            foreach ($hari_detail->result() as $value) {
                $id_header = $value->id_header;
            }
            $header_detail = $this->get_where_custom_by_table('jaka_header','id',$id_header);
            foreach ($header_detail->result() as $value) {
                $id_user = $value->input_by;
            }

            if($id_user != $user){
                redirect('/jam/detail/'.$id_header);
            }
        } else {
            redirect('/');
        }
    }

    function check_before_save($table, $user, $id){
        Modules::run('security/make_sure_is_login');
    }

    function check_dayoff()
    {
        $tod = date('d-m');
        $cek_tgl_fixed = $this->check_one_data('jaka_tanggal_libur','tanggal',$tod);
        if($cek_tgl_fixed == 1){
            $get_tgl_fixed = $this->get_where_custom_by_table('jaka_tanggal_libur','tanggal',$tod);
            foreach ($get_tgl_fixed->result() as $value) {
                $ket = $value->keterangan;
            }
            return $ket;
        } else {
            return 'nol';
        }
    }

    function change_date($data){
        $edit = explode('-', $data);
        if($edit[1] == '01'){
            $edit[1] = 'Januari';
        } else if($edit[1] == '02'){
            $edit[1] = 'Februari';
        } else if($edit[1] == '03'){
            $edit[1] = 'Maret';
        } else if($edit[1] == '04'){
            $edit[1] = 'April';
        } else if($edit[1] == '05'){
            $edit[1] = 'Mei';
        } else if($edit[1] == '06'){
            $edit[1] = 'Juni';
        } else if($edit[1] == '07'){
            $edit[1] = 'Juli';
        } else if($edit[1] == '08'){
            $edit[1] = 'Agustus';
        } else if($edit[1] == '09'){
            $edit[1] = 'September';
        } else if($edit[1] == '10'){
            $edit[1] = 'Oktober';
        } else if($edit[1] == '11'){
            $edit[1] = 'November';
        } else if($edit[1] == '12'){
            $edit[1] = 'Desember';
        }

        $data_edit = implode(' ', $edit);

        return $data_edit;
    }

    function get_kategori_short($shortname){
        $cek_data = $this->check_one_data('jaka_kategori','shortname',$shortname);
        if($cek_data == 1){
            $kategori_detail = $this->get_where_custom_by_table('jaka_kategori','shortname',$shortname);
            foreach ($kategori_detail->result() as $value) {
                $kategoriname = $value->nama;
            }
            return $kategoriname;
        } else {
            return "";
        } 
    }

    function get_hari_id($id_header, $hari){
        $cek_data = $this->check_two_data('jaka_detail_hari','id_header','hari',$id_header,$hari);
        if($cek_data == 1){
            $get_data = $this->get_two_data('jaka_detail_hari','id_header','hari',$id_header,$hari);
            foreach ($get_data->result() as $value) {
                $id = $value->id;
            }
            return $id;
        } else {
            return 0;
        }
    }

    function get_tanggal_id($id_header, $tanggal){
        $cek_data = $this->check_two_data('jaka_detail_tanggal','id_header','tanggal',$id_header,$tanggal);
        if($cek_data == 1){
            $get_data = $this->get_two_data('jaka_detail_tanggal','id_header','tanggal',$id_header,$tanggal);
            foreach ($get_data->result() as $value) {
                $id = $value->id;
            }
            return $id;
        } else {
            return 0;
        }
    }
      
    function get_kategori_name($id)
    {
        $kategori_detail = $this->get_where_custom_by_table('jaka_kategori','id',$id);
		foreach ($kategori_detail->result() as $value) {
			$kategoriname = $value->nama;
		}
        return $kategoriname;
    }

    function get_prokab_name($type,$id)
    {
        if($type == 'provinsi'){
            $provinsi_detail = $this->get_where_custom_by_table('common_provinsi','id_prov',$id);
            foreach ($provinsi_detail->result() as $value) {
                $name = $value->nama_provinsi;
            }
        } else if($type == 'kab'){
            $kab_detail = $this->get_where_custom_by_table('common_kabupaten','id_kab',$id);
            foreach ($kab_detail->result() as $value) {
                $name = $value->nama;
            }
        }
        return $name;
    }
    
    function get_input_data($id)
    {
		$user_detail = $this->get_where_custom_by_table('jaka_user_new','id',$id);
		foreach ($user_detail->result() as $value) {
			$loginname = $value->username;
		}
        return $loginname;
    }

    function get_input_data_name($id)
    {
        $user_detail = $this->get_where_custom_by_table('jaka_user_new','id',$id);
        foreach ($user_detail->result() as $value) {
            $fullname = $value->nama;
        }

        $out = strlen($fullname) > 10 ? substr($fullname,0,10).".." : $fullname;
        return $out;
    }

    function get_user_avatar($id)
    {
		$user_detail = $this->get_where_custom_by_table('jaka_user_new','id',$id);
		foreach ($user_detail->result() as $value) {
			$loginname = $value->path_photo;
		}
        return $loginname;
    }

    function get_user_avatar_thumb($id)
    {
        $user_detail = $this->get_where_custom_by_table('jaka_user_new','id',$id);
        foreach ($user_detail->result() as $value) {
            $loginname = $value->thumb_photo;
        }
        return $loginname;
    }

    function autocomplete_data_jaka($data)
    {
        $query = $this->mdl_common->autocomplete_data_jaka($data);
        return $query;
    }

    function autocomplete_data_kategori($data)
    {
        $query = $this->mdl_common->autocomplete_data_kategori($data);
        return $query;
    }

    function search_data($keyword)
    {
        $query = $this->mdl_common->search_data($keyword);
        return $query;
    }

    function count_search($keyword)
    {
        $query = $this->mdl_common->count_search($keyword);
        return $query;
    }

    function get_jaka($page)
    {
        $query = $this->mdl_common->get_jaka($page);
        return $query;
    }

    function get_jaka_kategori($page, $id_kategori)
    {
        $query = $this->mdl_common->get_jaka_kategori($page, $id_kategori);
        return $query;
    }

    function get_jaka_link_terkait($page, $id_kategori)
    {
        $query = $this->mdl_common->get_jaka_link_terkait($page, $id_kategori);
        return $query;
    }

    function get_provinsi()
    {
        $query = $this->mdl_common->get_provinsi();
        return $query;
    }

    function get_kota()
    {
        $query = $this->mdl_common->get_kota();
        return $query;
    }

    function get_jaka_dashboard($page)
    {
        $query = $this->mdl_common->get_jaka_dashboard($page);
        return $query;
    }

    function get_kategori($page)
    {
        $query = $this->mdl_common->get_kategori($page);
        return $query;
    }
    
    function get_kategori_dashboard($page)
    {
        $query = $this->mdl_common->get_kategori_dashboard($page);
        return $query;
    }

    function _createThumbnail($filename)
    {
        $config['image_library']    = "gd2";
        $config['source_image']     = "assets/data/profil/" .$filename;
        $config['create_thumb']     = TRUE;
        $config['maintain_ratio']   = TRUE;
        $config['width'] = "80";
        $config['height'] = "80";
        $this->load->library('image_lib',$config);
        if(!$this->image_lib->resize())
        {
            echo $this->image_lib->display_errors();
        }
    }

    function _configFilePhoto()
    {
        $dir_path = "assets/data/profil/";
        $config['upload_path']   =   $dir_path;
        $config['allowed_types'] =   "gif|jpg|jpeg|png";
        $config['max_size']      =   "5000";
        $config['min_width']     =   "110";
        $config['max_width']     =   "1100";
        $config['min_height']    =   "100";
        $config['max_height']    =   "1000";
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
    }

    function check_login()
    {
        $user_id = $this->session->userdata('user_id_jaka');
        $user_id_provider = $this->session->userdata('user_id_provider');

        if($user_id == '' || empty($user_id))
		{
			$result =  0;
		} else {
            //if($user_id_provider == 'local'){
            //    $cek_id = Modules::run('common/check_one_data','jaka_user_new','id',$user_id);
            //} else {
                $cek_id = Modules::run('common/check_one_data','jaka_user_new','id',$user_id);
            //}
            
            if($cek_id < 1){
                $result = 0;
            } else {
                $result = 1;
            }
        }
        return $result;
    }

    function get_by_table_or($table)
    {
        $query = $this->mdl_common->get_by_table_or($table);
        return $query;
    }

    function get_by_table($table,$order_by)
    {
        $query = $this->mdl_common->get_by_table_off($table,$order_by);
        return $query;
    }

    function get_by_table_asc_no_del($table,$order_by)
    {
        $query = $this->mdl_common->get_by_table_asc_no_del($table,$order_by);
        return $query;
    }

    function get_by_table_page($table,$order_by,$num,$offset)
    {
        $query = $this->mdl_common->get_by_table_page($table,$order_by,$num,$offset);
        return $query;
    }

    function get_by_table_desc($table,$order_by)
    {
        $query = $this->mdl_common->get_by_table_desc($table,$order_by);
        return $query;
    }

    function get_by_table_desc_limit($table,$order_by,$limit)
    {
        $query = $this->mdl_common->get_by_table_desc_limit($table,$order_by,$limit);
        return $query;
    }

    function get_where_id_by_table($table,$id)
    {
        $query = $this->mdl_common->get_where_id_by_table($table,$id);
        return $query;
    }

    function update_two_criteria($table,$col1,$col2,$value1,$value2,$data)
    {
        $this->mdl_common->update_two_criteria($table,$col1,$col2,$value1,$value2,$data);
    }

    function _update_by_table($table,$id, $data)
    {
        $this->mdl_common->_update_by_table($table,$id, $data);
    }

    function _update_custom_table($table,$col,$val,$data)
    {
        $this->mdl_common->_update_custom_table($table,$col,$val,$data);
    }

    function _insert_by_table($table,$data)
    {
        $this->mdl_common->_insert_by_table($table,$data);
    }

    function _delete_by_table($table,$data)
    {
        $this->mdl_common->_delete_by_table($table,$data);
    }

    function _delete_by_id($table,$id)
    {
        $this->mdl_common->_delete_by_id($table,$id);
    }

    function _delete_by_id_tf($table,$id)
    {
        $this->mdl_common->_delete_by_id_tf($table,$id);
    }

    function check_record_by_id_and_table($table,$id)
    {
        $query = $this->mdl_common->check_record_by_table($table,$id);
        return $query;
    }

    function get_record_with_limit($table,$limit, $start)
    {
        return $this->mdl_common->get_record_with_limit($table,$limit, $start);
    }

    function total_count_by_table($table)
    {
        $query = $this->mdl_common->total_count_by_table($table);
        return $query;
    }

    function total_count_by_table_ndel($table)
    {
        $query = $this->mdl_common->total_count_by_table_ndel($table);
        return $query;
    }

    function total_count_by_table_unread($table)
    {
        $query = $this->mdl_common->total_count_by_table_unread($table);
        return $query;
    }

    function get_max_data($table,$field)
    {
        $query = $this->mdl_common->get_max_data($table,$field);
        return $query->result();
    }

    function count_by_table_data($table,$data)
    {
        $query = $this->mdl_common->count_by_table_data($table,$data);
        return $query;
    }

    function select_count_table($table)
    {
        $query = $this->mdl_common->select_count_table($table);
        return $query;
    }

    function get_where_custom_by_table($table,$col,$value)
    {
        $query = $this->mdl_common->get_where_custom_by_table($table,$col,$value);
        return $query;
    }

    function get_where_custom_by_table_sort($table,$col,$value,$sortfield,$sort)
    {
        $query = $this->mdl_common->get_where_custom_by_table_sort($table,$col,$value,$sortfield,$sort);
        return $query;
    }

    function get_two_data($table,$col1,$col2,$value1,$value2)
    {
        $query = $this->mdl_common->get_two_data($table,$col1,$col2,$value1,$value2);
        return $query;
    }

    function get_three_data($table,$col1,$col2,$col3,$value1,$value2,$value3)
    {
        $query = $this->mdl_common->get_three_data($table,$col1,$col2,$col3,$value1,$value2,$value3);
        return $query;
    }

    function get_four_data($table,$col1,$col2,$col3,$col4,$value1,$value2,$value3,$value4)
    {
        $query = $this->mdl_common->get_four_data($table,$col1,$col2,$col3,$col4,$value1,$value2,$value3,$value4);
        return $query;
    }

    function check_three_data($table,$col1,$col2,$col3,$value1,$value2,$value3)
    {
        $query = $this->mdl_common->check_three_data($table,$col1,$col2,$col3,$value1,$value2,$value3);
        return $query;
    }

    function check_two_data($table,$col1,$col2,$value1,$value2)
    {
        $query = $this->mdl_common->check_two_data($table,$col1,$col2,$value1,$value2);
        return $query;
    }

    function check_one_data($table,$col1,$value1)
    {
        $query = $this->mdl_common->check_one_data($table,$col1,$value1);
        return $query;
    }

    function check_custom_data($table,$data)
    {
        $query = $this->mdl_common->check_custom_data($table,$data);
        return $query;
    }

    function pword_check($username, $passwd)
    {
        $query = $this->mdl_common->pword_check($username, $passwd);
        return $query;
    }

    function get_custom_data($table,$data)
    {
      $query = $this->mdl_common->get_custom_data($table,$data);
      return $query;
    }

    function get_user_name($id)
  	{
  		$cek_user = $this->get_where_custom_by_table('jaka_user_new','id',$id);
  		foreach ($cek_user->result() as $value) { $nama_user = $value->nama; }
  		return $nama_user;
    }

    function get_id($username)
    {
        $cek_user = $this->get_where_custom_by_table('jaka_user_new','username',$username);
        foreach ($cek_user->result() as $value) { $id = $value->id; }
        return $id;
  }

    function get_username_login()
    {
        $id = $this->session->userdata('user_id_jaka');
        $cek_user = $this->get_where_custom_by_table('jaka_user_new','id',$id);
        foreach ($cek_user->result() as $value) { $nama_user = $value->username; }
        return $nama_user;
    }

    function check_jaka_detail($id)
    {
        $user_id = $this->session->userdata('user_id_jaka');
        $cek_data = $this->check_two_data('jaka_header','input_by','id',$user_id,$id);
        if($cek_data > 0){
            return "yes";
        } else {
            return "no";
        }
    }

    function check_data_jaka($username)
    {
        $get_id = $this->get_id($username);
        $cek_jaka = $this->check_two_data('jaka_header','input_by','isDeleted',$get_id,0);
        return $cek_jaka;
    }

    function cek_username($username, $idlogin)
    {
        $get_data = $this->get_where_custom_by_table('jaka_user_new','username',$username);
        foreach($get_data->result() as $datas){
            $id = $datas->id;
        }

        if($id == $idlogin){ return "yes"; } else { return "no"; }
    }

    function get_hari_ini()
    {
        $hari_ini = date('N');
        if($hari_ini == 1){ return "SENIN";}
        else if($hari_ini == 2){ return "SELASA";}
        else if($hari_ini == 3){ return "RABU";}
        else if($hari_ini == 4){ return "KAMIS";}
        else if($hari_ini == 5){ return "JUMAT";}
        else if($hari_ini == 6){ return "SABTU";}
        else if($hari_ini == 7){ return "MINGGU";}
    }

    function get_detail_hari($id, $hari)
    {
        $cek_hari = $this->check_two_data('jaka_detail_hari','id_header','hari',$id,$hari);
        $get_hari = $this->get_two_data('jaka_detail_hari','id_header','hari',$id,$hari);
        if($cek_hari > 0){
            $jaka = '';
            foreach($get_hari->result() as $row){
                if($row->jaka == '24 Jam'){
                    $jaka = '24 JAM';
                } else if($row->jaka == 'Tutup'){
                    $jaka = 'TUTUP';
                } else if($row->jaka == '-'){
                    $jaka = '-';
                }else {
                    //$jaka = "Ada";
                    $pecah = explode('~', $row->jaka);
                    $sesi1 = $pecah[0];
                    $sesi2 = $pecah[1];
        
                    $pecah_sesi1 = explode('-', $sesi1);
                    $pecah_sesi2 = explode('-', $sesi2);
        
                    if($pecah_sesi1[0] == 0 && $pecah_sesi1[1] == 0 ){
                        $sesi1 = '';
                    } else {
                        $sesi1 = $pecah_sesi1[0].'-'.$pecah_sesi1[1];
                    }
        
                    if($pecah_sesi2[0] == 0  && $pecah_sesi2[1] == 0){
                        $sesi2 = '';
                    } else {
                        $sesi2 = $pecah_sesi2[0].'-'.$pecah_sesi2[1];
                    }
        
                    if($sesi1 ==  '' && $sesi2 == ''){
                        $jaka = '';
                    } else if($sesi1 != '' && $sesi2 == ''){
                        $jaka = trim($sesi1);
                    } else if($sesi1 == '' && $sesi2 != ''){
                        $jaka = trim($sesi2);
                    } else {
                        $jaka = trim($sesi1).'<br/>'.trim($sesi2);
                    }
                }
            }
            return $jaka;
        } else {
            return "-";
        }
    }
      
    public function get_data_jam($data, $zona)
    {
        if($data == '24 Jam'){
            return '<div style="text-align:center;color:#049bc9;" class="statfont">24 JAM</div>';
        } else if($data == 'Tutup'){
            return '<div style="text-align:center;color:#ad0000;" class="statfont">TUTUP</div>';
        } else {
            $pecah = explode('~', $data);
            $sesi1 = $pecah[0];
            $sesi2 = $pecah[1];

            $pecah_sesi1 = explode('-', $sesi1);
            $pecah_sesi2 = explode('-', $sesi2);

            if($pecah_sesi1[0] == 0 && $pecah_sesi1[1] == 0 ){
                $sesi1 = '';
            } else {
                $sesi1 = $pecah_sesi1[0].'-'.$pecah_sesi1[1];
            }

            if($pecah_sesi2[0] == 0  && $pecah_sesi2[1] == 0){
                $sesi2 = '';
            } else {
                $sesi2 = $pecah_sesi2[0].'-'.$pecah_sesi2[1];
            }

            if($sesi1 ==  '' && $sesi2 == ''){
                return '<div style="text-align:center;" class="statfont"></div>';
            } else if($sesi1 != '' && $sesi2 == ''){
                return '<div style="text-align:center;color:#00ad42;" class="statfont">'.$sesi1.'</div><div class="label">'.$zona.'</div>';
            } else if($sesi1 == '' && $sesi2 != ''){
                return '<div style="text-align:center;color:#d3a10a;" class="statfont">'.$sesi2.'</div><div class="label">'.$zona.'</div>';
            } else {
                return '<div style="text-align:center;color:#000000;" class="statfont">'.trim($sesi1).'<br/>'.trim($sesi2).'</div><div class="label">'.$zona.'</div>';
            }
        }
    }

    public function get_data_jam_info($data, $zona)
    {
        if($data == '24 Jam'){
            return 'Jam Buka : 24 JAM';
        } else if($data == 'Tutup'){
            return 'Jam Buka : TUTUP';
        } else {
            $pecah = explode('~', $data);
            $sesi1 = $pecah[0];
            $sesi2 = $pecah[1];

            $pecah_sesi1 = explode('-', $sesi1);
            $pecah_sesi2 = explode('-', $sesi2);

            if($pecah_sesi1[0] == 0 && $pecah_sesi1[1] == 0 ){
                $sesi1 = '';
            } else {
                $sesi1 = $pecah_sesi1[0].'-'.$pecah_sesi1[1];
            }

            if($pecah_sesi2[0] == 0  && $pecah_sesi2[1] == 0){
                $sesi2 = '';
            } else {
                $sesi2 = $pecah_sesi2[0].'-'.$pecah_sesi2[1];
            }

            if($sesi1 ==  '' && $sesi2 == ''){
                return '-';
            } else if($sesi1 != '' && $sesi2 == ''){
                return 'Jam Buka : '.$sesi1.' '.$zona;
            } else if($sesi1 == '' && $sesi2 != ''){
                return 'Jam Buka : '.$sesi2.' '.$zona;
            } else {
                return 'Jam Buka : '.trim($sesi1).', '.trim($sesi2).' '.$zona;
            }
        }
    }

    function get_data_jam_search($data, $zona)
    {
        if($data == '24 Jam'){
            return '<div style="background:#4a7c00;padding-top:23px;text-align:center !important;" class="image">
                        <div style="margin:0 auto;margin-left:5px;min-height:90px;" class="ui statistics">
                          <div class="statistic">
                            <div class="label">
                              Today
                            </div>
                            <div style="color:white;font-size:25pt !important;margin-left:8px;padding-top:7px;" class="text">
                              24 JAM
                            </div>
                          </div>
                        </div>
                    </div>';
        } else if($data == 'Tutup'){
            return ' <div style="background:red;padding-top:23px;text-align:center !important;" class="image">
                        <div style="margin:0 auto;margin-left:5px;min-height:90px;" class="ui statistics">
                          <div class="statistic">
                            <div class="label">
                              Today
                            </div>
                            <div style="font-size:25pt !important;padding-top:7px;margin-left:8px;color:white;" class="text">
                              TUTUP
                            </div>
                          </div>
                        </div>
                    </div>';
        } else {
            $pecah = explode('~', $data);
            $sesi1 = $pecah[0];
            $sesi2 = $pecah[1];

            $pecah_sesi1 = explode('-', $sesi1);
            $pecah_sesi2 = explode('-', $sesi2);

            if($pecah_sesi1[0] == 0 && $pecah_sesi1[1] == 0 ){
                $sesi1 = '';
            } else {
                $sesi1 = $pecah_sesi1[0].'-'.$pecah_sesi1[1];
            }

            if($pecah_sesi2[0] == 0  && $pecah_sesi2[1] == 0){
                $sesi2 = '';
            } else {
                $sesi2 = $pecah_sesi2[0].'-'.$pecah_sesi2[1];
            }

            if($sesi1 ==  '' && $sesi2 == ''){
                return '<div style="background:black;padding-top:23px;text-align:center !important;" class="image">
                        <div style="margin:0 auto;margin-left:40px;min-height:90px;" class="ui statistics">
                          <div class="statistic">
                            <div style="color:white;" class="label">
                              Today
                            </div>
                            <div style="font-size:25pt !important;padding-top:7px;margin-left:8px;color:white;" class="text">
                              -
                            </div>
                          </div>
                        </div>
                    </div>';
            } else if($sesi1 != '' && $sesi2 == ''){
                return '<div style="border:solid 1px #d8d6d6;padding-top:23px;text-align:center !important;" class="image">
                          <div style="margin:0 auto;margin-left:5px;min-height:90px;" class="ui statistics">
                              <div class="statistic">
                                <div class="label">Today</div>
                                <div style="font-size:16pt !important;" class="text">'.trim($sesi1).'</div>
                                <div class="label">'.$zona.'</div>
                              </div>
                          </div>
                        </div>';
            } else if($sesi1 == '' && $sesi2 != ''){
                return '<div style="border:solid 1px #d8d6d6;padding-top:23px;text-align:center !important;" class="image">
                          <div style="margin:0 auto;margin-left:5px;min-height:90px;" class="ui statistics">
                              <div class="statistic">
                                <div class="label">Today</div>
                                <div style="font-size:16pt !important;" class="text">'.trim($sesi2).'</div>
                                <div class="label">'.$zona.'</div>
                              </div>
                          </div>
                        </div>';
            } else {
                return '<div style="border:solid 1px #d8d6d6;" class="image box-jam">
                          <div class="ui statistics box-jam-content">
                              <div class="statistic">
                                <div style="color:#969696;" class="label">Today</div>
                                <div style="font-size:16pt !important;" class="text">'.trim($sesi1).'<br/>'.trim($sesi2).'</div>
                                <div style="color:#969696;" class="label">'.$zona.'</div>
                              </div>
                          </div>
                        </div>';
            }
        }
    }

    public function get_data_tanggal($jaka_tgl)
    {
        if($jaka_tgl == '24 Jam'){
            $jaka = '24 JAM';
        } else if($jaka_tgl == 'Tutup'){
            $jaka = 'TUTUP';
        } else {
            //$jaka = "Ada";
            $pecah = explode('~', $jaka_tgl);
            $sesi1 = $pecah[0];
            $sesi2 = $pecah[1];

            $pecah_sesi1 = explode('-', $sesi1);
            $pecah_sesi2 = explode('-', $sesi2);

            if($pecah_sesi1[0] == 0 && $pecah_sesi1[1] == 0 ){
                $sesi1 = '';
            } else {
                $sesi1 = $pecah_sesi1[0].'-'.$pecah_sesi1[1];
            }

            if($pecah_sesi2[0] == 0  && $pecah_sesi2[1] == 0){
                $sesi2 = '';
            } else {
                $sesi2 = $pecah_sesi2[0].'-'.$pecah_sesi2[1];
            }

            if($sesi1 ==  '' && $sesi2 == ''){
                $jaka = '';
            } else if($sesi1 != '' && $sesi2 == ''){
                $jaka = trim($sesi1);
            } else if($sesi1 == '' && $sesi2 != ''){
                $jaka = trim($sesi2);
            } else {
                $jaka = trim($sesi1).'<br/>'.trim($sesi2);
            }
        }
        return $jaka;
    }
}
