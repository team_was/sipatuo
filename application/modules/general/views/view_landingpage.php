<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sipatuo - Hadir membantu anda.</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
	<?php
		$multiple_css = array('all.css','bootstrap.min.css','offcanvas.css');
		echo assets_css($multiple_css);
	?>
</head>
<body class="bg-light">
	<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <?php $this->load->view('common/navbar_title_view'); ?>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item active mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>"><i class="fas fa-smile-beam"></i> <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>campaign">Campaign</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>pendonor">Pendonor</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>events">Kegiatan</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>signup"><i class="fas fa-user-plus"></i></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a alt="Login" title="Login" class="nav-link" href="<?php echo base_url(); ?>signin"><i class="fas fa-door-open"></i></a>
          </li>
        </ul>
      </div>
  </nav>

		<!-- <div class="nav-scroller bg-white box-shadow">
		  <nav class="nav nav-underline ml-lg-3">
		    <a class="nav-link active" href="#">Dashboard</a>
		    <a class="nav-link" href="#">
		      Friends
		      <span class="badge badge-pill bg-light align-text-bottom">27</span>
		    </a>
		    <a class="nav-link" href="#">Explore</a>
		    <a class="nav-link" href="#">Suggestions</a>
		    <a class="nav-link" href="#">Link</a>
		    <a class="nav-link" href="#">Link</a>
		    <a class="nav-link" href="#">Link</a>
		    <a class="nav-link" href="#">Link</a>
		    <a class="nav-link" href="#">Link</a>
		  </nav>
		</div> -->

    <main role="main" class="container-fluid">
      <div class="row baris-1">
          <div class="col-lg-9 col-md-8 box-l-row-1">
            <div class="type-wrap type-style">
              <div id="typed-strings">
                <span>Manusia terbaik itu yang banyak<br/>memberi <strong>manfaat</strong> untuk sesama.</span>
                <p><strong>Mendonorkan <em>darah</em></strong> adalah<br/>salah satunya.</p>
                <p>Sekantong darah anda dapat<br/>menyelamatkan <strong style="color:red;">nyawa</strong> mereka.</p>
                <p>Sipatuo berusaha membantu anda<br/>menemukan pendonor <strong>secara cepat</strong>.</p>
              </div>
              <span id="typed" style="white-space:pre;"></span>
            </div>
          </div>
          <div class="col-lg-3 col-md-2 box-r-row-1">
            <a style="width:170px;border-radius: 25px;" href="<?php echo base_url(); ?>campaign/create" class="btn btn-outline-light btn-lg-lg mb-3">Buat Campaign</a><br/>
            <a style="width:170px;border-radius: 25px;" href="<?php echo base_url(); ?>pendonor" class="btn btn-outline-light btn-lg-lg mb-3">Cari Pendonor</a>
          </div>
      </div>
      <!-- <div class="row justify-content-center baris-2">
      
      </div> -->

      <div style="min-height:600px;" class="row justify-content-center baris-2">
        <?php $dataurgent =1; ?>
        <div class="col-lg-9 col-12">
          <div class="row justify-content-center pt-4">
            <div style="text-align: center;" class="col-lg-12 d-none d-md-block d-lg-block">
              <p style="color:#c80d0d;text-shadow: 2px 2px 12px white;" class="display-4 mb-5 mt-3"><strong>#urgent</strong>Needs</p>
            </div>
            

            <div class="col-lg-6 mb-lg-5 text-center">
              <img class="urgent-img" src="<?php echo base_url(); ?>assets/img/urgentneeds-7.png" alt="">
            </div>
            <div class="col-lg-6 pt-4 desc-camp">
              <h1 class="">Campaign</h1>
              <p class="lead camp-det ml-3">
                Campaign bertujuan untuk membantu anda menemukan pendonor secara cepat dan tepat sesuai dengan kriteria darah
                yang anda butuhkan. Sipatuo tim tergerak untuk membuat fitur ini karena sering melihat rekan-rekan mengirimkan pesan singkat / chat / postingan untuk mencari pendonor dimana kondisi pasien dalam keadaan urgent.
              </p>
              <a style="border-radius:20px;" href="<?php echo base_url(); ?>campaign" class="btn btn-danger ml-3 mb-4 px-4"><i style="color:white;" class="fas fa-plus-circle mr-2"></i> Buat Campaign</a><br/>
            </div>

            <div class="col-lg-12 mt-4 d-lg-none d-md-none">
              <div class="float-right"><a style="font-size:1.15em;" href="#" class="text-success mr-1"><i class="fas fa-plus-circle"></i></a> <a style="font-size:1.15em;" href="#" class="text-dark"><i class="fas fa-search"></i></a></div>
              <h6 style="" class="border-bottom border-gray pb-3 mb-0">Urgent Needs / Campaigns</h6>
            </div>

            <?php if($dataurgent > 0){ ?>
            <div class="col-lg-4 col-md-4 mb-lg-4 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <!-- <img data-src="holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1" alt="" class="mr-2 rounded"> -->
                <div style="width: 32px;height:32px;background:black;color:white;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>YS</strong></div>
                <div style="width: 32px;height:32px;background:#c4c4c4;color:white;text-align: center;padding-top:3px;position: absolute;top:51px;" class="mr-2 rounded"><strong>Tr</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Butuh : Trombosit (10 Kantong)</strong>
                  <strong class="d-block text-gray-dark mb-2">Yunita Sinegar</strong>
                  Donec id elit non mi porta gravida at eget metus.
                  
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Status : Open</span>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="fas fa-comments"></i> 0</a>
                </p>
              </div>
              <div class="box-urgent d-none d-md-block d-lg-block">
                <div class="display-4 urgent-blood">A+</div>
                <div class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong></div>
                <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih." (area Jawa Barat)</p>
                <p class="urgent-user text-danger">Yuanita Siregar</p>
                <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
                <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
                <span class="urgent-deadline d-md-none d-lg-block">Deadline : 24 Juli 2018</span>
                <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>
                <span class="urgent-status d-md-none d-lg-block">Status : <strong style="color:#adabab;">Completed</strong></span>

                <div style="text-align:right;" class="box-info justify-content-end d-none d-md-block d-lg-block">
                  <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-secondary btn-sm urgent-info ml-1"><i class="fas fa-comments"></i> 0</a>
                  <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-warning btn-sm urgent-info ml-1"><i class="fas fa-info-circle mr-1"></i> Detail Kontak</a>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-lg-4 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <!-- <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded"> -->
                <div style="width: 32px;height:32px;background:#fc5a5a;color:white;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>AW</strong></div>
                <div style="width: 32px;height:32px;background:#c4c4c4;color:white;text-align: center;padding-top:3px;position: absolute;top:51px;" class="mr-2 rounded"><strong>A+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Butuh : A+ (7 Kantong)</strong>
                  <strong class="d-block text-gray-dark">Andi Waya Meraja</strong>
                  Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Status : Open</span>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                  <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-secondary btn-sm urgent-info ml-1"><i class="fas fa-comments"></i> 0</a>
                </p>
              </div>
              <div class="box-urgent d-none d-md-block d-lg-block">
                <div class="display-4 urgent-blood">AB+</div>
                <div class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong> <span class="blink_me" style="font-size:0.70em;">(Butuh 2 Kantong lagi)</span></div>
                <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih."</p>
                <p class="urgent-user text-danger">Yuanita Siregar</p>
                <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
                <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
                <span class="urgent-deadline">Deadline : 24 Juli 2018</span>
                <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>
                <span class="urgent-status d-md-none d-lg-block">Status : <strong style="color:#1ecc49;">Open</strong></span>

                <div style="text-align:right;" class="box-info justify-content-end d-none d-md-block d-lg-block">
                  <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-secondary btn-sm urgent-info ml-1"><i class="fas fa-comments"></i> 0</a>
                  <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-warning btn-sm urgent-info ml-1"><i class="fas fa-info-circle mr-1"></i> Detail Kontak</a>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 mb-lg-4 pb-1 box-main">
              <div class="media text-muted pt-3 d-lg-none d-md-none">
                <!-- <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded"> -->
                <div style="width: 32px;height:32px;background:#fc5a5a;color:white;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>LN</strong></div>
                <div style="width: 32px;height:32px;background:#c4c4c4;color:white;text-align: center;padding-top:3px;position: absolute;top:51px;" class="mr-2 rounded"><strong style="font-size:0.8em;">AB+</strong></div>
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark">Butuh : A+ (7 Kantong)</strong>
                  <strong class="d-block text-gray-dark">Liliana Nana</strong>
                  Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                  <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                  <span style="font-size:0.95em;" class="d-block text-gray-dark">Status : Open</span>
                  <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                  <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-secondary btn-sm urgent-info ml-1"><i class="fas fa-comments"></i> 0</a>
                </p>
              </div>
              <div class="box-urgent d-none d-md-block d-lg-block">
                <div class="display-4 urgent-blood">B+</div>
                <div class="display-4 mb-3 urgent-bag"><strong>10 Kantong</strong></div>
                <p class="urgent-desc">"Keluarga saya akan melakukan operasi dan membutuhkan darah, mohon bantuannya, terima kasih." (area Makassar)</p>
                <p class="urgent-user text-danger">Yuanita Siregar</p>
                <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none">Lokasi : Kalimantan Selatan</p>
                <p style="margin-bottom:0.5px;" class="urgent-user d-none d-md-block d-lg-none"><a href="#">Lihat Detail</a></p>
                <span class="urgent-deadline">Deadline : 24 Juli 2018</span>
                <span class="urgent-time"><i class="fas fa-clock mr-1"></i> 3 jam yang lalu</span>
                <span class="urgent-status d-md-none d-lg-block">Status : <strong style="color:#adabab;">Completed</strong></span>

                <div style="text-align:right;" class="box-info justify-content-end d-none d-md-block d-lg-block">
                  <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-secondary btn-sm urgent-info ml-1"><i class="fas fa-comments"></i> 0</a>
                  <a href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-warning btn-sm urgent-info ml-1"><i class="fas fa-info-circle mr-1"></i> Detail Kontak</a>
                </div>
              </div>
            </div>
            <div style="text-align: center;" class="col-lg-12 mb-3 mt-3 d-none d-lg-block">
              <a style="border-radius:20px;" href="<?php echo base_url(); ?>campaign" class="btn btn-dark px-4"><i style="color:white;" class="fas fa-globe-asia mr-2"></i> Lihat Semua</a>
            </div>
            <?php } else { ?>
              
                

            <?php } ?>
          </div>
          
        </div>
          
        
      </div>
      
      <div class="row justify-content-center box-cari-donor">
          <div class="col-lg-12 pt-lg-3 mt-5 cari-pendonor">
            <h1 class="display-4 search-text"><strong>#cari</strong>Pendonor</h1>
          </div>
          <div style="margin-top:0px;" class="col-lg-7 mx-auto mt-1">
            <form>
              <div class="form-row">
                <div class="form-group col-md-4">
                  <input style="border-radius: 20px;border:none;" type="text" class="form-control form-control-sm px-3" id="inputEmail4" placeholder="Masukkan keyword ...">
                </div>
                <div class="form-group col-md-4 col-6">
                  <select style="border-radius: 20px;border:none;" id="inputState" class="form-control form-control-sm px-3">
                    <option selected>Lokasi</option>
                    <option>Makassar</option>
                    <option>Jakarta</option>
                    <option>Serui</option>
                  </select>
                </div>
                <div class="form-group col-md-2 col-6">
                  <select style="border-radius: 20px;border:none;" id="inputState" class="form-control form-control-sm px-3">
                    <option selected>Goldar</option>
                    <option>A</option>
                    <option>B</option>
                    <option>O</option>
                    <option>AB</option>
                  </select>
                </div>
                <div class="form-group col-md-2">
                  <a style="border-radius:20px;width:100%;" href="<?php echo base_url(); ?>pendonor" class="btn btn-danger btn-sm px-3"><i class="fas fa-search"></i> Search</a>
                </div>
              </div>
            </form>
          </div>
          <div class="col-lg-12 mb-5">
            <div class="row justify-content-center">
              <div class="col-lg-2 col-md-3">
                <div class="d-none d-md-block d-lg-block" style="border-radius:15px;width:100%; background:rgba(255,255,255,0.7);height:200px;box-shadow: 3px 4px 9px #515151;text-align: center;">
                  <label class="display-4 pt-4" style="color: black;font-size:3em;"><a class="text-danger" href="<?php echo base_url(); ?>">A</a></label><br/>
                  <label class="display-4" style="color: black;font-size:2em;font-weight: 600;">0</label><br/>
                  <p class="lead">Pendonor</p>
                  <span style="position:absolute;bottom:7px;left:25px;"><i style="color:#f9e8e8;" class="fas fa-tint"></i></span>
                </div>
                <div class="media text-muted pt-3 d-lg-none d-md-none">
                  <div style="width: 32px;height:32px;background:#01c157;color:white;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>A</strong></div>
                  <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <div class="d-flex justify-content-between align-items-center w-100">
                      <strong class="text-light">Goldar : A</strong>
                      <a style="font-size:0.9em;" class="text-light" href="#">Lihat semua</a>
                    </div>
                    <span style="color:#01c157" class="d-block">0 Pendonor</span>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 col-md-3">
                <div class="d-none d-md-block d-lg-block" style="border-radius:15px;width:100%; background:rgba(255,255,255,0.7);height:200px;box-shadow: 3px 4px 9px #515151;text-align: center;">
                  <label class="display-4 pt-4" style="color: black;font-size:3em;"><a class="text-danger" href="<?php echo base_url(); ?>">B</a></label><br/>
                  <label class="display-4" style="color: black;font-size:2em;font-weight: 600;">0</label><br/>
                  <p class="lead">Pendonor</p>
                  <span style="position:absolute;bottom:7px;left:25px;"><i style="color:#f9e8e8;" class="fas fa-tint"></i></span>
                </div>
                <div class="media text-muted pt-3 d-lg-none d-md-none">
                  <div style="width: 32px;height:32px;background:#007bff;color:white;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>B</strong></div>
                  <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <div class="d-flex justify-content-between align-items-center w-100">
                      <strong class="text-light">Goldar : B</strong>
                      <a style="font-size:0.9em;" class="text-light" href="#">Lihat semua</a>
                    </div>
                    <span style="color:#85bcf7" class="d-block">0 Pendonor</span>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 col-md-3">
                <div class="d-none d-md-block d-lg-block" style="border-radius:15px;width:100%; background:rgba(255,255,255,0.7);height:200px;box-shadow: 3px 4px 9px #515151;text-align: center;">
                  <label class="display-4 pt-4" style="color: black;font-size:3em;"><a class="text-danger" href="<?php echo base_url(); ?>">AB</a></label><br/>
                  <label class="display-4" style="color: black;font-size:2em;font-weight: 600;">0</label><br/>
                  <p class="lead">Pendonor</p>
                  <span style="position:absolute;bottom:7px;left:25px;"><i style="color:#f9e8e8;" class="fas fa-tint"></i></span>
                </div>
                <div class="media text-muted pt-3 d-lg-none d-md-none">
                  <div style="width: 32px;height:32px;background:#c12801;color:white;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>AB</strong></div>
                  <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <div class="d-flex justify-content-between align-items-center w-100">
                      <strong class="text-light">Goldar : AB</strong>
                      <a style="font-size:0.9em;" class="text-light" href="#">Lihat semua</a>
                    </div>
                    <span style="color:#fb9277" class="d-block">0 Pendonor</span>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 col-md-3">
                <div class="d-none d-md-block d-lg-block" style="border-radius:15px;width:100%; background:rgba(255,255,255,0.7);height:200px;box-shadow: 3px 4px 9px #515151;text-align: center;">
                  <label class="display-4 pt-4" style="color: black;font-size:3em;"><a class="text-danger" href="<?php echo base_url(); ?>">O</a></label><br/>
                  <label class="display-4" style="color: black;font-size:2em;font-weight: 600;">0</label><br/>
                  <p class="lead">Pendonor</p>
                  <span style="position:absolute;bottom:7px;left:25px;"><i style="color:#f9e8e8;" class="fas fa-tint"></i></span>
                </div>
                <div class="media text-muted pt-3 d-lg-none d-md-none">
                  <div style="width: 32px;height:32px;background:#c8cc00;color:white;text-align: center;padding-top:3px;" class="mr-2 rounded"><strong>O</strong></div>
                  <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <div class="d-flex justify-content-between align-items-center w-100">
                      <strong class="text-light">Goldar : O</strong>
                      <a style="font-size:0.9em;" class="text-light" href="#">Lihat semua</a>
                    </div>
                    <span style="color:#c8cc00" class="d-block">0 Pendonor</span>
                  </div>
                </div>
              </div>
              
            </div>
            <div class="col-lg-12 text-center mt-5">
                <a style="border-radius:20px;" href="<?php echo base_url(); ?>campaign" class="btn btn-light ml-3 mb-2 px-4"><i style="color:black;" class="far fa-user mr-2"></i> Lihat Semua Pendonor</a><br/>
              </div>
          </div>
      </div>


      <div class="row justify-content-center bg-light box-kegiatan-donor">
        <!-- <div> -->

          <!-- <div class="my-3 p-3 bg-light rounded d-lg-none d-md-none">
            <h6 class="border-bottom border-gray pb-2 mb-0">Kegiatan Donor</h6>
            <div class="media text-muted pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-dark">Bersama membangun negeri melalui donor darah terpadu.</strong>
                <strong style="font-weight:400;" class="d-block text-gray-dark mb-2">BPJS Makale</strong>
                This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                <span class="d-block text-gray-dark pt-2">Tanggal Kegiatan : 24 Juli 2018</span>
                <a href="#" class="d-block text-gray-dark">Lihat Detail</a>
              </p>
            </div>
            <div class="media text-muted pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=53019b&fg=53019b&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-dark">Donor darah untuk mereka yang membutuhkan.</strong>
                <strong style="font-weight:400;" class="d-block text-gray-dark mb-2">PMI Makassar</strong>
                This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                <span class="d-block text-gray-dark pt-2">Tanggal Kegiatan : 24 Juli 2018</span>
                <a href="#" class="d-block text-gray-dark">Lihat Detail</a>
              </p>
            </div>
            <div class="media text-muted pt-3">
              <img data-src="holder.js/32x32?theme=thumb&bg=c1011a&fg=c1011a&size=1" alt="" class="mr-2 rounded">
              <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <strong class="d-block text-gray-dark">@username</strong>
                Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
              </p>
            </div>
            <small class="d-block text-right mt-3 mb-3">
              <a href="#">Lihat Semua Kegiatan</a>
            </small>
          </div> -->

          <div class="col-lg-10">
            <div class="row justify-content-center pt-4">
              <div style="text-align: center;" class="col-lg-12 mt-5 mb-5 d-none d-md-block d-lg-block">
                <h1 style="font-size:3em;color:black;" class="display-4"><strong>#kegiatan</strong>Donor</h1>
              </div>
              <div class="col-lg-12 d-lg-none d-md-none">
                <div class="float-right"><a style="font-size:1.15em;" href="#" class="text-success mr-1"><i class="fas fa-plus-circle"></i></a> <a style="font-size:1.15em;" href="#" class="text-dark"><i class="fas fa-search"></i></a></div>
                <h6 style="" class="border-bottom border-gray pb-3 mb-0">Daftar Kegiatan Donor</h6>
              </div>

              <div class="col-lg-3 col-md-4 mb-lg-5">
                  <div class="media text-muted pt-3 d-lg-none d-md-none">
                    <img style="border:solid 1px #efefef" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-2.jpg" alt="" class="mr-2 rounded">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                      <strong class="d-block text-gray-dark"><a href="<?php echo base_url(); ?>events/detail/ev123nt">Tema : Card title that wraps to a new line latihan melakukan</a></strong>
                      <span style="color:#ff9e9e" class="d-block mb-2">Penyelenggara : BPJS Kesehatan</span>
                      This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                      <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Alamat : Jalan Perintis Kemerdekaan 10, Rumah sakit Universitas Hasanuddin (Tampilkan dalam maps)</span>
                      <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu : 24 Juli 2018</span>
                      <span style="font-size:0.95em;" class="d-block text-gray-dark">Status : Open</span>
                      <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-dark ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    </p>
                  </div>
                  <div style="" class="card d-none d-md-block d-lg-block">
                    <img class="card-img-top fade-btm" src="<?php echo base_url(); ?>assets/img/Donor-Darah.jpg" alt="Card image cap">
                    <div class="card-body">
                      <h5 style="font-size:1.20em;font-weight: 400;margin-top:-20px;" class="card-title display-4"><a class="text-dark" href="<?php echo base_url(); ?>events/detail/ev123nt">Card title that wraps to a new line</a></h5>
                      <div class="mb-1">
                        <i class="far fa-clock mr-1 text-info"></i>
                        <i class="fas fa-map-marker-alt mr-1 text-danger"></i>
                        <a href="#"><i class="far fa-comment-dots text-dark"></i><small> 0</small></a>
                      </div>
                      <p style="font-size:0.9em;" class="card-text lead mb-3">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
                      <span class="btn btn-dark btn-sm urgent-info px-3 mb-2"><i class="fas fa-info-circle"></i> Detail Kegiatan</span>
                      <span style="position: absolute;top:5px;right:5px;" class="btn btn-success btn-sm urgent-info mb-2">Open</span>
                    </div>
                  </div>
              </div>
              <div class="col-lg-3 col-md-4 mb-lg-5">
                  <div class="media text-muted pt-3 d-lg-none d-md-none">
                    <img style="border:solid 1px #efefef" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-5.png" alt="" class="mr-2 rounded">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                      <strong class="d-block text-gray-dark">Tema : Card title that wraps to a new line latihan melakukan</strong>
                      <span style="color:#ff9e9e" class="d-block mb-2">Penyelenggara : BPJS Kesehatan</span>
                      -
                      <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Alamat : Jalan Perintis Kemerdekaan 10, Rumah sakit Universitas Hasanuddin (Tampilkan dalam maps)</span>
                      <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu : 24 Juli 2018</span>
                      <span style="font-size:0.95em;" class="d-block text-gray-dark">Status : Open</span>
                      <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-dark ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    </p>
                  </div>
                  <div style="" class="card d-none d-md-block d-lg-block">
                    <img class="card-img-top fade-btm" src="<?php echo base_url(); ?>assets/img/Donor-Darah-2.jpg" alt="Card image cap">
                    <div class="card-body">
                      <h5 style="font-size:1.20em;font-weight: 400;margin-top:-20px;" class="card-title display-4"><a class="text-dark" href="<?php echo base_url(); ?>events/detail/ev123nt">Card title that wraps to a new line</a></h5>
                      <div class="mb-1">
                        <i class="far fa-clock mr-1 text-info"></i>
                        <i class="fas fa-map-marker-alt mr-1 text-danger"></i>
                        <a href="#"><i class="far fa-comment-dots text-dark"></i><small> 0</small></a>
                      </div>
                      <p style="font-size:0.9em;" class="card-text lead mb-3">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
                      <span class="btn btn-dark btn-sm urgent-info px-3 mb-2"><i class="fas fa-info-circle"></i> Detail Kegiatan</span>
                      <span style="position: absolute;top:5px;right:5px;" class="btn btn-warning btn-sm urgent-info py-0 mb-2">Completed</span>
                    </div>
                  </div>
              </div>
              <div class="col-lg-3 col-md-4 mb-lg-5">
                  <div class="media text-muted pt-3 d-lg-none d-md-none">
                    <img style="border:solid 1px #efefef" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-5.png" alt="" class="mr-2 rounded">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                      <strong class="d-block text-gray-dark">Tema : Card title that wraps to a new line latihan melakukan</strong>
                      <span style="color:#ff9e9e" class="d-block mb-2">Penyelenggara : BPJS Kesehatan</span>
                      -
                      <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Alamat : Jalan Perintis Kemerdekaan 10, Rumah sakit Universitas Hasanuddin (Tampilkan dalam maps)</span>
                      <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu : 24 Juli 2018</span>
                      <span style="font-size:0.95em;" class="d-block text-gray-dark">Status : Open</span>
                      <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-dark ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    </p>
                  </div>
                  <div style="" class="card d-none d-md-block d-lg-block">
                    <img class="card-img-top fade-btm" src="<?php echo base_url(); ?>assets/img/Donor-Darah-2.jpg" alt="Card image cap">
                    <div class="card-body">
                      <h5 style="font-size:1.20em;font-weight: 400;margin-top:-20px;" class="card-title display-4"><a class="text-dark" href="<?php echo base_url(); ?>events/detail/ev123nt">Card title that wraps to a new line</a></h5>
                      <div class="mb-1">
                        <i class="far fa-clock mr-1 text-info"></i>
                        <i class="fas fa-map-marker-alt mr-1 text-danger"></i>
                        <a href="#"><i class="far fa-comment-dots text-dark"></i><small> 0</small></a>
                      </div>
                      <p style="font-size:0.9em;" class="card-text lead mb-3">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
                      <span class="btn btn-dark btn-sm urgent-info px-3 mb-2"><i class="fas fa-info-circle"></i> Detail Kegiatan</span>
                      <span style="position: absolute;top:5px;right:5px;" class="btn btn-warning btn-sm urgent-info py-0 mb-2">Completed</span>
                    </div>
                  </div>
              </div>
              <div class="col-lg-3 col-md-4 mb-lg-5">
                  <div class="media text-muted pt-3 d-lg-none d-md-none">
                    <img style="border:solid 1px #efefef" width="60px" height="60px" src="<?php echo base_url(); ?>assets/img/user-5.png" alt="" class="mr-2 rounded">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                      <strong class="d-block text-gray-dark">Tema : Card title that wraps to a new line latihan melakukan</strong>
                      <span style="color:#ff9e9e" class="d-block mb-2">Penyelenggara : BPJS Kesehatan</span>
                      -
                      <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Alamat : Jalan Perintis Kemerdekaan 10, Rumah sakit Universitas Hasanuddin (Tampilkan dalam maps)</span>
                      <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu : 24 Juli 2018</span>
                      <span style="font-size:0.95em;" class="d-block text-gray-dark">Status : Open</span>
                      <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-dark ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    </p>
                  </div>
                  <div style="" class="card d-none d-md-block d-lg-block">
                    <img class="card-img-top fade-btm" src="<?php echo base_url(); ?>assets/img/Donor-Darah-2.jpg" alt="Card image cap">
                    <div class="card-body">
                      <h5 style="font-size:1.20em;font-weight: 400;margin-top:-20px;" class="card-title display-4"><a class="text-dark" href="<?php echo base_url(); ?>events/detail/ev123nt">Card title that wraps to a new line</a></h5>
                      <div class="mb-1">
                        <i class="far fa-clock mr-1 text-info"></i>
                        <i class="fas fa-map-marker-alt mr-1 text-danger"></i>
                        <a href="#"><i class="far fa-comment-dots text-dark"></i><small> 0</small></a>
                      </div>
                      <p style="font-size:0.9em;" class="card-text lead mb-3">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
                      <span class="btn btn-dark btn-sm urgent-info px-3 mb-2"><i class="fas fa-info-circle"></i> Detail Kegiatan</span>
                      <span style="position: absolute;top:5px;right:5px;" class="btn btn-warning btn-sm urgent-info py-0 mb-2">Completed</span>
                    </div>
                  </div>
              </div>

            </div>
          </div>
          
          <div style="text-align: center;" class="col-lg-12 mb-5 mt-2 d-none d-lg-block">
            <a style="border-radius:20px;" href="<?php echo base_url(); ?>events" class="btn btn-danger px-5"><i style="color:white;" class="fas fa-globe-asia mr-2"></i> Lihat Semua</a>
          </div>
          <div style="text-align: center;" class="col-lg-12 mb-5 mt-4 d-lg-none d-md-none">
            <a style="border-radius:20px;" href="<?php echo base_url(); ?>events" class="btn btn-sm btn-danger px-5"><i style="color:white;" class="fas fa-globe-asia mr-2"></i> Lihat Semua</a>
          </div>
        <!-- </div> -->
      </div>
      <div class="row justify-content-center box-gabung-sipper">
        <div class="col-lg-12 mt-5 text-gabung-donor">
          <h1 style="font-size:2em;color:black;" class="display-4"><strong>Ayo </strong>Bergabung bersama kami</h1>
          <h1 style="font-size:1em;color:#153b77;" class="display-4">Gabung, temukan, minta bantuan dan bantu teman yang membutuhkan bantuan anda.</h1>
        </div>
        <div style="text-align: center;" class="col-lg-6">
          <a style="border-radius:20px;" href="<?php echo base_url(); ?>signup" class="btn btn-outline-warning px-lg-5 btn-lg-lg">Daftar</a> <span class="px-lg-2">atau</span>
          <a style="border-radius:20px;" href="<?php echo base_url(); ?>signin" class="btn btn-outline-info px-lg-5 btn-lg-lg">Login</a>
        </div>
      </div>
      <div style="" class="row justify-content-center box-kata-mereka pb-5">

          <div style="text-align: left;height:20px;" class="col-lg-10 mt-5 d-none d-md-block d-lg-block">
            <h1 style="font-size:3em;color:white;" class="display-4"><strong>#kata</strong>Mereka</h1>
          </div>

        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Carousel indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>   
          <!-- Wrapper for carousel items -->
          <div class="carousel-inner">
            <div class="item carousel-item active">
              <div class="img-box"><img src="<?php echo base_url(); ?>assets/img/user.png" alt=""></div>
              <p class="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
              <p class="overview"><b>Paula Wilson</b>, Media Analyst</p>
            </div>
            <div class="item carousel-item">
              <div class="img-box"><img src="<?php echo base_url(); ?>assets/img/user.png" alt=""></div>
              <p class="testimonial">Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget nisi a mi suscipit tincidunt. Utmtc tempus dictum risus. Pellentesque viverra sagittis quam at mattis. Suspendisse potenti. Aliquam sit amet gravida nibh, facilisis gravida odio.</p>
              <p class="overview"><b>Antonio Moreno</b>, Web Developer</p>
            </div>
            <div class="item carousel-item">
              <div class="img-box"><img src="<?php echo base_url(); ?>assets/img/images.png" alt=""></div>
              <p class="testimonial">Phasellus vitae suscipit justo. Mauris pharetra feugiat ante id lacinia. Etiam faucibus mauris id tempor egestas. Duis luctus turpis at accumsan tincidunt. Phasellus risus risus, volutpat vel tellus ac, tincidunt fringilla massa. Etiam hendrerit dolor eget rutrum.</p>
              <p class="overview"><b>Michael Holz</b>, Seo Analyst</p>
            </div>
          </div>
          <!-- Carousel controls -->
          <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
            <i class="fa fa-angle-left"></i>
          </a>
          <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
            <i class="fa fa-angle-right"></i>
          </a>
        </div>
          
      </div>
    </main>

    <?php $this->load->view('common/footer_view'); ?>
<?php
	$multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js');
	echo assets_js($multiple_js);
?>
<script type="text/javascript">
  document.addEventListener('DOMContentLoaded', function() {
    var typed = new Typed('#typed', {
      stringsElement: '#typed-strings',
      typeSpeed: 130,
      backSpeed: 30,
      backDelay:3000,
      startDelay: 1000,
      loop: true,
      loopCount: Infinity,
      onComplete: function(self) { prettyLog('onComplete ' + self) },
      preStringTyped: function(pos, self) { prettyLog('preStringTyped ' + pos + ' ' + self); },
      onStringTyped: function(pos, self) { prettyLog('onStringTyped ' + pos + ' ' + self) },
      onLastStringBackspaced: function(self) { prettyLog('onLastStringBackspaced ' + self) },
      onTypingPaused: function(pos, self) { prettyLog('onTypingPaused ' + pos + ' ' + self) },
      onTypingResumed: function(pos, self) { prettyLog('onTypingResumed ' + pos + ' ' + self) },
      onReset: function(self) { prettyLog('onReset ' + self) },
      onStop: function(pos, self) { prettyLog('onStop ' + pos + ' ' + self) },
      onStart: function(pos, self) { prettyLog('onStart ' + pos + ' ' + self) },
      onDestroy: function(self) { prettyLog('onDestroy ' + self) }
    });
  });

  function prettyLog(str) {
    console.log('%c ' + str, 'color: green; font-weight: bold;');
  }

  function toggleLoop(typed) {
    if (typed.loop) {
      typed.loop = false;
    } else {
      typed.loop = true;
    }
  }
</script>
</body>
</html>