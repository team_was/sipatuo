<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sipatuo - Hadir membantu anda.</title>
    <link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
    <?php
      $multiple_css = array('all.css','bootstrap.min.css','startpage.css');
      echo assets_css($multiple_css);
    ?>
  </head>
  <body class="bg-light">
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      <?php $this->load->view('common/navbar_title_view'); ?>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>"><i class="fas fa-smile-beam"></i> <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>campaign">Campaign</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>pendonor">Pendonor</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>events">Kegiatan</a>
          </li>
          <li class="nav-item mr-lg-4">
            <a class="nav-link" href="<?php echo base_url(); ?>signup"><i class="fas fa-user-plus"></i></a>
          </li>
          <li class="nav-item mr-lg-4">
            <a alt="Login" title="Login" class="nav-link" href="<?php echo base_url(); ?>signin"><i class="fas fa-door-open"></i></a>
          </li>
        </ul>
      </div>
  </nav>

    

    <main role="main" class="container-fluid">
      
      <div style="min-height:800px;" class="row justify-content-center">

        <div class="col-lg-9">
          <div class="row justify-content-center pt-4">
            <div style="text-align: center;" class="col-lg-12 d-none d-md-block d-lg-block">
              <p style="color:#c80d0d;text-shadow: 2px 2px 12px white;font-size:2.5em;" class="display-4 mb-3 mt-3"><strong>#privacy</strong>Policy</p>
            </div>
            <div class="col-lg-12 mb-5">
              <div class="my-3 p-5 bg-white rounded shadow-sm">
                <h2 class="display-4" style="font-size:2em;">Umum</h2>
                
                <p class="text-muted">Jika Anda memerlukan informasi lebih lanjut atau memiliki pertanyaan lain tentang kebijakan privasi pada sipatuo.com, silahkan menghubungi kami via e-mail di imronahmad.sadar@gmail.com.</p>

                <p class="text-muted">Pada sipatuo.com, privasi para pengunjung website ini sangat penting. Dokumen kebijakan privasi ini mengurai jenis informasi pribadi yang diterima dan dikumpulkan oleh sipatuo.com dan bagaimana informasi pribadi tersebut digunakan.</p>
                
                <h2 class="display-4" style="font-size:2em;">Log Files</h2>

                <p class="text-muted">Seperti kebanyakan situs Web lain, sipatuo.com juga menggunakan log files. Informasi dalam log files meliputi alamat internet protocol (IP), jenis browser, Internet Service Provider (ISP), jejak tanggal / waktu, refferering atau exit pages dan klik-klik yang dilakukan pengunjung untuk menganalisis kecendrungan, mengelola situs, melacak aktifitas pengguna pada situs, dan untuk mengumpulkan informasi demografis. Alamat IP dan informasi lainnya tersebut tidak terkait dengan informasi yang bersifat pribadi.</p>

                <h2 class="display-4" style="font-size:2em;">Cookies dan Web Beacons</h2>

                <p class="text-muted">sipatuo.com tidak menggunakan cookies untuk menyimpan informasi tentang preferensi pengunjung, merekam informasi pengguna tertentu pada halaman yang dikunjungi, menyesuaikan halaman Web content based berdasarkan tipe browser yang digunakan pengunjung atau informasi lainnya yang pengunjung kirimkan melalui browser yang digunakan.</p>

                <h2 class="display-4" style="font-size:2em;">DoubleClick DART Cookie</h2>

                <p class="text-muted">.:: Google, sebagai vendor pihak ketiga, menggunakan cookies untuk menayangkan iklan di sipatuo.com. .:: Penggunaan DART cookie oleh Google memungkinkan Google dapat menampilkan iklan yang sesuai kepada pengguna berdasarkan kunjungan di sipatuo.com dan situs lainnya di Internet. .:: Pengguna dapat membatalkan penggunaan DART cookie dengan mengunjungi kebijakan privasi jaringan iklan dan konten Google di URL berikut : <a href="http://www.google.com/policies/technologies/ads/">http://www.google.com/policies/technologies/ads/</a></p>

                <p class="text-muted">Jika Anda ingin menonaktifkan cookies, Anda dapat melakukannya melalui pilihan-pilihan di browser Anda. Informasi lebih lanjut tentang manajemen cookie dengan browser web tertentu dapat ditemukan di situs web masing-masing browser.</p>

                <p class="text-muted">sipatuo.com tunduk dan patuh pada aturan dibawah google. Untuk membaca aturan Google, silahkan kunjungi : <a href="http://www.google.com/policies/privacy/">http://www.google.com/policies/privacy/</a></p>

              </div>
            </div>
          </div>
        </div>
        
      </div>
    </main>

    <?php $this->load->view('common/footer_view'); ?>

    <?php
      $multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', '', 'typed.min.js');
      echo assets_js($multiple_js);
    ?>
  </body>
</html>