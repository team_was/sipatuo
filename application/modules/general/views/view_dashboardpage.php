<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sipatuo - Hadir membantu anda.</title>
    <link rel="icon" href="<?php echo base_url(); ?>/assets/img/fav_red.ico">
    <?php
      $multiple_css = array('all.css','bootstrap.min.css','offcanvas.css');
      echo assets_css($multiple_css);
    ?>
    <style type="text/css">
      .fade-btm {
        -webkit-mask-image:-webkit-gradient(linear, left top, left bottom, from(rgba(0,0,0,1)), to(rgba(0,0,0,0)));
        mask-image: linear-gradient(to bottom, rgba(0,0,0,1), rgba(0,0,0,0));
      }
    </style>
  </head>
  <body class="bg-light">
    <nav style="" class="navbar shadow-sm navbar-expand-lg fixed-top navbar-dark bg-light">
      <?php $this->load->view('common/navbar_title_admin_view'); ?>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-md-auto d-none d-flex d-md-flex">
          <li class="nav-item active mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>home"><i class="fas fa-home"></i></a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;" class="nav-link dropdown-toggle" id="userdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url() ?>profil"><i class="far fa-list-alt"></i></a>
            <div style="margin-top:-17px;margin-right:130px;" class="dropdown-menu dropdown-menu-right" aria-labelledby="userdropdown">
              <h6 class="dropdown-header">General</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>campaign"><i class="fas fa-bullhorn mr-2"></i>Semua Campaign</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>events"><i class="far fa-calendar-alt mr-2"></i>Semua Kegiatan</a></span>
              <div class="dropdown-divider"></div>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>pendonor"><i class="fas fa-users mr-2"></i>Semua Pendonor</a></span>
            </div>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>notification"><i class="far fa-bell"></i>
              <label class="bg-danger count-new-notif-1"><strong>4</strong></label>
            </a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;width: 50px;text-align: center;" class="nav-link" href="<?php echo base_url(); ?>message"><i class="far fa-envelope"></i>
              <label class="bg-danger count-new-msg-1"><strong>9</strong></label>
            </a>
          </li>
          <li class="nav-item mr-lg-2">
            <a style="font-size:1.3em;color:#636363;" class="nav-link dropdown-toggle" id="userdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url() ?>profil"><i class="far fa-user-circle"></i></a>
            <div style="margin-top:-17px;margin-right:65px;" class="dropdown-menu dropdown-menu-right" aria-labelledby="userdropdown">
              <h6 class="dropdown-header">Personal</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/campaign"><i class="fas fa-bullhorn mr-2"></i>Campaign Anda</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/event"><i class="far fa-calendar-alt mr-2"></i>Kegiatan Anda</a></span>
              <div class="dropdown-divider"></div>
              <h6 class="dropdown-header">Profil</h6>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil/password"><i class="fas fa-fingerprint mr-2"></i>Ganti Password</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>profil"><i class="far fa-user mr-2"></i>Profil</a></span>
              <span><a style="font-size:0.88em;" class="dropdown-item" href="<?php echo base_url(); ?>signout"><i class="fas fa-power-off mr-2"></i>Keluar</a></span>
            </div>
          </li>
          <li class="nav-item">
            <a style="font-size:1.3em;color:#e16e12;" class="nav-link" href="<?php echo base_url(); ?>signout"><i class="fas fa-power-off"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <!-- <div class="nav-scroller bg-white shadow-sm">
      <nav class="nav nav-underline ml-lg-5">
        <a class="nav-link active" href="#">Dashboard</a>
        <a class="nav-link" href="#">
          Notifikasi
          <span class="badge badge-pill bg-light align-text-bottom">27</span>
        </a>
        <a class="nav-link" href="#">Pesan</a>
        <a class="nav-link" href="#">Undang Teman</a>
        <a class="nav-link" href="#">Kegiatan</a>
        <a class="nav-link" href="#">Pendonor</a>
        <a class="nav-link" href="#">Keluar</a> -->
        <!-- <a class="nav-link" href="#">Link</a>
        <a class="nav-link" href="#">Link</a> -->
      <!-- </nav>
    </div> -->

    <main role="main" class="container-fluid">
      
      <div style="min-height: 200px;" class="row justify-content-center bg-white">
        <div class="col-lg-10">
          <div class="row justify-content-center mt-5">
            <div class="col-lg-2">
              <div style="width: 100%;height:110px;font-size: 0.75em;background:;border-radius: 10px;" class="p-2 bg-info">
                <label style="position: absolute;top:5px;left:23px;" class="text-white">Notifikasi</label>
                  <h3 style="text-align:center;padding-top:23px;font-size: 3.5em;color:#dff3ff;" class="timer display-4 count-number mb-0 pb-0" data-to="0" data-speed="1500"></h3>
                  <a class="text-light" href="<?php echo base_url(); ?>notification"><i style="position: absolute;bottom:8px;right:23px;font-size:0.85em;" class="fas fa-external-link-alt"></i></a>
              </div>
            </div>
            <div class="col-lg-2">
              <div style="width: 100%;height:110px;font-size: 0.75em;background:;border-radius: 10px;" class="p-2 bg-danger">
                <label style="position: absolute;top:5px;left:23px;" class="text-white">Pesan</label>
                <h3 style="text-align:center;padding-top:23px;font-size: 3.5em;color:#ffdfdf;" class="timer display-4 count-number mb-0 pb-0" data-to="0" data-speed="1500"></h3>
                <a class="text-light" href="<?php echo base_url(); ?>message"><i style="position: absolute;bottom:8px;right:23px;font-size:0.85em;" class="fas fa-external-link-alt"></i></a>
              </div>
            </div>
            <div class="col-lg-2 ">
              <div style="width: 100%;height:110px;font-size: 0.75em;background:;border-radius: 10px;" class="p-2 bg-success">
                <label style="position: absolute;top:5px;left:23px;" class="text-white">Campaign Anda</label>
                <h3 style="text-align:center;padding-top:23px;font-size: 3.5em;color:#d0ffc7;" class="timer display-4 count-number mb-0 pb-0" data-to="0" data-speed="1500"></h3>
                <a class="text-light" href="<?php echo base_url(); ?>profil/campaign"><i style="position: absolute;bottom:8px;right:23px;font-size:0.85em;" class="fas fa-external-link-alt"></i></a>
              </div>
            </div>
            <div class="col-lg-2 ">
              <div style="width: 100%;height:110px;font-size: 0.75em;background:;border-radius: 10px;" class="p-2 bg-warning">
                <label style="position: absolute;top:5px;left:23px;" class="text-white">Event Anda</label>
                <h3 style="text-align:center;padding-top:23px;font-size: 3.5em;" class="timer display-4 count-number mb-0 pb-0" data-to="0" data-speed="1500"></h3>
                <a class="text-light" href="<?php echo base_url(); ?>profil/event"><i style="position: absolute;bottom:8px;right:23px;font-size:0.85em;" class="fas fa-external-link-alt"></i></a>
              </div>
            </div>
            <div class="col-lg-4 ">
              <div style="width: 100%;height:110px;font-size: 0.75em;" class="p-2 bg-dark">
                <label style="position: absolute;top:5px;left:23px;" class="text-white">Point</label>
                <h3 style="text-align:center;padding-top:15px;font-size: 3.5em;" class="timer display-4 count-number mb-0 pb-0 text-light" data-to="0" data-speed="1500"></h3>
                <small style="font-size:0.75em;padding-top:13px;" id="hpHelp" class="form-text text-light ml-2">Point akan bertambah sesuai keaktifan anda dalam melakukan donor darah.</small>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row justify-content-center bg-white pb-3">
        <div class="col-lg-10 mb-5">
          <div class="row justify-content-center">
            <div class="col-lg-6">
              <div style="min-height:400px;border:solid 1px #e8e8e8 !important;" class="mt-0 p-3 bg-white rounded shadow-sm">
                <a style="font-size:0.75em;" href="#" class="float-right">See all notifications</a>
                <h6 class="border-gray pb-2 mb-0">Your Notifications List</h6>
                <div style="padding: 0px 1rem 0px 2.25rem;" class="list-group-item list-group-item-action flex-column align-items-start unread">
                  <div class="media text-muted pt-3">
                    <i style="font-size: 1.5em;" class="far fa-comment mr-2"></i>
                    <span style="position: absolute;top:5px;left:7px;font-size:0.68em;"><i class="fas fa-circle"></i></span>
                    <p style="font-size:0.75em;" class="media-body pb-3 mb-0 small lh-125 border-gray">
                      <span style="position: absolute;top:10px;right:10px;font-size: 0.85em;">3 days ago</span>
                      <strong class="d-block text-gray-dark">Campaign</strong>
                      <a href="#">Ahmad Imron</a> memberikan komentar pada <a href="#">campaign</a> yang telah anda buat.
                    </p>
                  </div>
                </div>
                <div style="padding: 0px 1rem 0px 2.25rem;" class="list-group-item list-group-item-action flex-column align-items-start unread">
                  <div class="media text-muted pt-2">
                    <i style="font-size: 1.5em;" class="far fa-heart mr-2"></i>
                    <span style="position: absolute;top:5px;left:7px;font-size:0.68em;"><i class="fas fa-circle"></i></span>
                    <p style="font-size:0.75em;" class="media-body pb-3 mb-0 small lh-125 border-gray">
                      <span style="position: absolute;top:10px;right:10px;font-size: 0.85em;">3 minutes ago</span>
                      <strong class="d-block text-gray-dark">Campaign</strong>
                      <a href="#">No Name</a> memberikan simpati kepada <a href="#">Campaign (Butuh golongan darah B)</a> anda.
                    </p>
                  </div>
                </div>
                <div style="padding: 0px 1rem 0px 2.25rem;" class="list-group-item list-group-item-action flex-column align-items-start">
                  <div class="media text-muted pt-3">
                    <i style="font-size: 1.5em;" class="far fa-calendar-alt mr-2"></i>
                    <p style="font-size:0.75em;" class="media-body pb-3 mb-0 small lh-125 border-gray">
                      <span style="position: absolute;top:10px;right:10px;font-size: 0.85em;">3 minutes ago</span>
                      <strong class="d-block text-gray-dark">Events</strong>
                      Sandiaga akan mengikuti <a href="#">Event (Bersama membangun negeri melalui donor darah)</a> yang anda buat.
                    </p>
                  </div>
                </div>
                <div style="padding: 0px 1rem 0px 2.25rem;" class="list-group-item list-group-item-action flex-column align-items-start">
                  <div class="media text-muted pt-3">
                    <i style="font-size: 1.5em;" class="far fa-calendar-alt mr-2"></i>
                    <p style="font-size:0.75em;" class="media-body pb-3 mb-0 small lh-125 border-gray">
                      <span style="position: absolute;top:10px;right:10px;font-size: 0.85em;">3 minutes ago</span>
                      <strong class="d-block text-gray-dark">Events</strong>
                      Pramuidtyo akan mengikuti <a href="#">Event (Bersama membangun negeri melalui donor darah)</a> yang anda buat.
                    </p>
                  </div>
                </div>
                <small class="d-block text-right mt-3">
                  <a href="#">See all notifications</a>
                </small>
              </div>
            </div>
            <div class="col-lg-6">
              <div style="min-height:400px;border:solid 1px #e8e8e8 !important;" class="p-3 bg-white rounded shadow-sm">
                <a style="font-size:0.75em;" href="#" class="float-right">See all messages</a>
                <h6 class="border-gray pb-2 mb-0">Your Messages List</h6>
                <a href="#" style="padding: 0px 1rem 0px 2rem;" class="list-group-item list-group-item-action flex-column align-items-start unseen">
                  <div class="media text-muted pt-3">
                    <i style="font-size: 1.5em;" class="fas fa-envelope mr-2"></i>
                    <span style="position: absolute;top:5px;left:7px;font-size:1em;"><label class="badge badge-pill badge-success">1.5k</label></span>
                    <p style="font-size:0.75em;" class="media-body pb-3 mb-0 small lh-125 border-gray">
                      <span style="position: absolute;top:10px;right:10px;font-size: 0.85em;">23 minutes ago</span>
                      <strong class="d-block text-gray-dark">Ahmad Imron</strong>
                      Pesan : Siapa yang suka pada kerinduan yang mendalam, sebagai hasil dari siapa saja yang mau
                    </p>
                  </div>
                </a>
                <a href="#" style="padding: 0px 1rem 0px 2rem;" class="list-group-item list-group-item-action flex-column align-items-start unseen">
                  <div class="media text-muted pt-3">
                    <i style="font-size: 1.5em;" class="fas fa-envelope mr-2"></i>
                    <span style="position: absolute;top:5px;left:7px;font-size:1em;"><label class="badge badge-pill badge-success">10</label></span>
                    <p style="font-size:0.75em;" class="media-body pb-3 mb-0 small lh-125 border-gray">
                      <span style="position: absolute;top:10px;right:10px;font-size: 0.85em;">33 hours ago</span>
                      <strong class="d-block text-gray-dark">Suryadarmadi</strong>
                      Pesan : Ingat untuk datang ke rumah
                    </p>
                  </div>
                </a>
                <a href="#" style="padding: 0px 1rem 0px 2rem;" class="list-group-item list-group-item-action flex-column align-items-start">
                  <div class="media text-muted pt-3">
                    <i style="font-size: 1.5em;" class="far fa-envelope-open mr-3"></i>
                    <p style="font-size:0.75em;" class="media-body pb-3 mb-0 small lh-125 border-gray">
                      <span style="position: absolute;top:10px;right:10px;font-size: 0.85em;">33 hours ago</span>
                      <strong class="d-block text-gray-dark">jjokwidod</strong>
                      Pesan : Hanya untuk tes data yang masuk atau belum sodara lama
                    </p>
                  </div>
                </a>
                <a href="#" style="padding: 0px 1rem 0px 2rem;" class="list-group-item list-group-item-action flex-column align-items-start">
                  <div class="media text-muted pt-3">
                    <i style="font-size: 1.5em;" class="far fa-envelope-open mr-3"></i>
                    <p style="font-size:0.75em;" class="media-body pb-3 mb-0 small lh-125 border-gray">
                      <span style="position: absolute;top:10px;right:10px;font-size: 0.85em;">33 hours ago</span>
                      <strong class="d-block text-gray-dark">santodagi</strong>
                      Pesan : Ping
                    </p>
                  </div>
                </a>
                <small class="d-block text-right mt-3">
                  <a href="#">See all messages</a>
                </small>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div style="min-height:400px;" class="row justify-content-center bg-dark pb-3">
        <div class="col-lg-10">
          <p style="color:#fff;font-size:2.5em;text-align: center;" class="display-4 mb-5 mt-5 pt-5"><strong>#Look</strong>Around</p>
        </div>
        <div class="col-lg-10 mb-5">
          <div class="row">
            <div class="col-lg-6">
              <div style="min-height:650px;background: rgba(255,255,255,1);" class="my-3 p-3 rounded">
                <a style="font-size:0.75em;" href="#" class="float-right">Lihat Semua Campaign</a>
                <h6 class="border-bottom border-gray pb-2 mb-0">Campaign</h6>
                <div class="media text-secondary pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=0ccadb&fg=0ccadb&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Butuh : A+ (3 Kantong)</strong>
                    <span style="color:#a23;" class="d-block mb-2">Request by : Yunita Sinegar </span>
                    Mohon bantuannya, keluarga kami akan melakukan operasi, silahkan hubungi nomor : 08124206668, Terima kasih.
                    
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Status : Open</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-dark ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="far fa-comment-dots"></i> 0</a>
                  </p>
                </div>
                <div class="media text-secondary pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Butuh : Trombosit (10 Kantong)</strong>
                    <span style="color:#a23;" class="d-block mb-2">Request by : Ringki datas </span>
                    Donec id elit non mi porta gravida at eget metus.
                    
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Status : Open</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-dark ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="far fa-comment-dots"></i> 0</a>
                  </p>
                </div>
                <div class="media text-secondary pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=6f42c1&fg=6f42c1&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Butuh : AB+ (10 Kantong)</strong>
                    <span style="color:#e23;" class="d-block mb-2">Request by : Ananda Riswa </span>
                    Donec id elit non mi porta gravida at eget metus.
                    
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Status : Open</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-dark ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="far fa-comment-dots"></i> 0</a>
                  </p>
                </div>
                <div class="media text-secondary pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=dbbb0c&fg=dbbb0c&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Butuh : B+ (20 Kantong)</strong>
                    <span style="color:#e23;" class="d-block mb-2">Request by : Indrawan Silha </span>
                    Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo.
                    
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Deadline : 24 Juli 2018</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Status : Open</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-dark ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="far fa-comment-dots"></i> 0</a>
                  </p>
                </div>
                <small class="d-block text-right mt-3 mb-3">
                  <a class="text-secondary" href="#">Lihat Semua Kebutuhan</a>
                </small>
              </div>
            </div>
            <div class="col-lg-6">
              <div style="min-height:650px;" class="my-3 p-3 bg-white rounded">
                <a style="font-size:0.75em;" href="#" class="float-right">Lihat Semua Kegiatan</a>
                <h6 class="border-bottom border-gray pb-2 mb-0">Kegiatan donor</h6>
                <div class="media text-secondary pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=e83e8c&fg=e83e8c&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Bersama membangun bangsa yang peduli sesama.</strong>
                    <span style="color:#a51;" class="d-block mb-2">Organized by : BPJS Kesehatan </span>
                    Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo.
                    
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Makassar</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu kegiatan : 24 Juli 2018</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>events/detail/ev123nt" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="far fa-comment-dots"></i> 0</a>
                  </p>
                </div>
                <div class="media text-secondary pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=0ccadb&fg=0ccadb&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Bakti sosial donor darah</strong>
                    <span style="color:#a51;" class="d-block mb-2">Organized by : PLN </span>
                    Donec id elit non mi porta gravida at eget metus.
                    
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Jakarta Selatan</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu kegiatan : 24 Agustus 2018</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>events/detail/ev123nt" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="far fa-comment-dots"></i> 0</a>
                  </p>
                </div>
                <div class="media text-secondary pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=6f42c1&fg=6f42c1&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Bakti sosial donor darah</strong>
                    <span style="color:#a51;" class="d-block mb-2">Organized by : PLN</span>
                    Donec id elit non mi porta gravida at eget metus.
                    
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Jakarta Selatan</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu kegiatan : 24 Agustus 2018</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>events/detail/ev123nt" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="far fa-comment-dots"></i> 0</a>
                  </p>
                </div>
                <div class="media text-secondary pt-3">
                  <img data-src="holder.js/32x32?theme=thumb&bg=dbbb0c&fg=dbbb0c&size=1" alt="" class="mr-2 rounded">
                  <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-dark">Bakti sosial donor darah</strong>
                    <span style="color:#a51;" class="d-block mb-2">Organized by : Pertamina </span>
                    Donec id elit non mi porta gravida at eget metus.
                    
                    <span style="font-size:0.95em;" class="d-block text-gray-dark pt-2">Lokasi : Jakarta Selatan</span>
                    <span style="font-size:0.95em;" class="d-block text-gray-dark">Waktu kegiatan : 24 Agustus 2018</span>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>events/detail/ev123nt" class="btn btn-sm btn-warning ml-0 mt-2"><i class="fas fa-info-circle mr-1"></i> Lihat Detail</a>
                    <a style="border-radius:25px;font-size:0.8em;" href="<?php echo base_url(); ?>campaign/detail/ff1234512" class="btn btn-sm btn-secondary ml-0 mt-2"><i class="far fa-comment-dots"></i> 0</a>
                  </p>
                </div>
                <small class="d-block text-right mt-3 mb-3">
                  <a class="text-secondary" href="<?php echo base_url(); ?>events">Lihat Semua Kegiatan</a>
                </small>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div style="min-height:400px;" class="row justify-content-center bg-white">
        <div class="col-lg-10">
          <p style="color:#000;font-size:2.5em;text-align: center;" class="display-4 mb-5 mt-5 pt-5"><strong>#Pendonor</strong>Around</p>
        </div>
        <div style="" class="col-lg-10 pb-2">
          <div class="row justify-content-center">
            <div class="col-lg-9 pb-5">
              <canvas id="chart-area"></canvas>
            </div>
            <div class="col-lg-9 pb-3">
              <form>
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <input style="border-radius: 20px;" type="text" class="form-control form-control-sm" id="inputEmail4" placeholder="Masukkan Keyword...">
                  </div>
                  <div class="form-group col-md-4">
                    <select style="border-radius: 20px;" id="inputState" class="form-control form-control-sm">
                      <option selected>Lokasi</option>
                      <option>Makassar</option>
                      <option>Jakarta</option>
                      <option>Serui</option>
                    </select>
                  </div>
                  <div class="form-group col-md-2">
                    <select style="border-radius: 20px;" id="inputState" class="form-control form-control-sm">
                      <option selected>Goldar</option>
                      <option>A</option>
                      <option>B</option>
                      <option>O</option>
                      <option>AB</option>
                    </select>
                  </div>
                  <div style="text-align: center;" class="form-group col-md-2">
                    <button style="border-radius:25px;width: 100%;" type="button" class="btn btn-dark btn-sm"><i class="fas fa-search"></i> Search</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- <div class="col-lg-10 ml-4">
          <div class="row">
            <p>Recently Added :</p>
          </div>
        </div> -->
        <div class="col-lg-10 mb-5 pb-3">
          <div class="row">
            <div class="col-lg-2">
              <div class="p-2" style="border-radius:7px;width: 100%;min-height:205px;background:;text-align: center;border:solid 1px #eee;">
                <img style="width:100px;height:100px;" class="rounded-circle mt-3 mb-1" src="<?php echo base_url(); ?>assets/img/user-2.jpg" alt="Card image cap">
                <p class="mb-1" style="font-size:0.70em;line-height: 15px;"><a href="<?php echo base_url(); ?>pendonor/detail">Ahmad Imron Sadar Bolehlah</a></p>
                <span style="border-radius:25px;font-size:0.7em;" class="btn btn-danger py-0 mb-2">Goldar : A+</span>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="p-2" style="border-radius:7px;width: 100%;min-height:205px;background:;text-align: center;border:solid 1px #eee;">
                <img style="width:100px;height:100px;" class="rounded-circle mt-3 mb-1" src="<?php echo base_url(); ?>assets/img/user-3.png" alt="Card image cap">
                <p class="mb-1" style="font-size:0.70em;line-height: 15px;">Ahmad Imron Sadar Sekolah</p>
                <span style="border-radius:25px;font-size:0.7em;" class="btn btn-danger py-0 mb-2">Goldar : A+</span>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="p-2" style="border-radius:7px;width: 100%;min-height:205px;background:;text-align: center;border:solid 1px #eee;">
                 <img style="width:100px;height:100px;" class="rounded-circle mt-3 mb-1" src="<?php echo base_url(); ?>assets/img/user-4.png" alt="Card image cap">
                <p class="mb-1" style="font-size:0.70em;line-height: 15px;">Suryadarmadi</p>
                <span style="border-radius:25px;font-size:0.7em;" class="btn btn-danger py-0 mb-2">Goldar : A+</span>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="p-2" style="border-radius:7px;width: 100%;min-height:205px;background:;text-align: center;border:solid 1px #eee;">
                <img style="width:100px;height:100px;" class="rounded-circle mt-3 mb-1" src="<?php echo base_url(); ?>assets/img/user-5.png" alt="Card image cap">
                <p class="mb-1" style="font-size:0.70em;line-height: 15px;">Wahyudi</p>
                <span style="border-radius:25px;font-size:0.7em;" class="btn btn-danger py-0 mb-2">Goldar : A+</span>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="p-2" style="border-radius:7px;width: 100%;min-height:205px;background:;text-align: center;border:solid 1px #eee;">
                <img style="width:100px;height:100px;" class="rounded-circle mt-3 mb-1" src="<?php echo base_url(); ?>assets/img/user-1.png" alt="Card image cap">
                <p class="mb-1" style="font-size:0.70em;line-height: 15px;">Imron</p>
                <span style="border-radius:25px;font-size:0.7em;" class="btn btn-danger py-0 mb-2">Goldar : A+</span>
              </div>
            </div>
            <div class="col-lg-2">
              <div class="p-2" style="border-radius:7px;width: 100%;min-height:205px;background:;text-align: center;border:solid 1px #eee;">
                <!-- <img style="margin-bottom:5px;" width="90%" height="100px" src="<?php echo base_url(); ?>assets/img/user-1.png"> -->
                <p class="mt-5 pt-3" style="font-size:1.30em;line-height: 27px;"><a href="<?php echo base_url(); ?>pendonor">Lihat Semua Pendonor</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <?php $this->load->view('common/invitation_view'); ?>

    </main>

    <?php $this->load->view('common/footer_view'); ?>

    <?php
      $multiple_js = array('jquery.min.js', 'all.js', 'popper.min.js','bootstrap.min.js', 'holder.min.js', 'offcanvas.js', 'typed.min.js', 'chart.bundle.js', 'utils.js');
      echo assets_js($multiple_js);
    ?>
    <script type="text/javascript">
      var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
      };

      var config = {
        type: 'pie',
        data: {
          datasets: [{
            data: [
              21,
              10,
              25,
              30,
              10,
            ],
            backgroundColor: [
              window.chartColors.red,
              window.chartColors.orange,
              window.chartColors.purple,
              window.chartColors.green,
              window.chartColors.blue,
            ],
            label: 'Pendonor'
          }],
          labels: [
            'Goldar A',
            'Goldar B',
            'Goldar AB',
            'Goldar O',
            'Tidak Tahu Goldar'
          ]
        },
        options: {
          responsive: true,
          tooltips: {
              callbacks: {
                  label: function(tooltipItem, data) {
                      return data.labels[tooltipItem.index] + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + ' Pendonor';
                  }
              }
          }
        }
      };

      window.onload = function() {
        var ctx = document.getElementById('chart-area').getContext('2d');
        window.myPie = new Chart(ctx, config);
      };

      document.getElementById('randomizeData').addEventListener('click', function() {
        config.data.datasets.forEach(function(dataset) {
          dataset.data = dataset.data.map(function() {
            return randomScalingFactor();
          });
        });

        window.myPie.update();
      });

      var colorNames = Object.keys(window.chartColors);
      document.getElementById('addDataset').addEventListener('click', function() {
        var newDataset = {
          backgroundColor: [],
          data: [],
          label: 'New dataset ' + config.data.datasets.length,
        };

        for (var index = 0; index < config.data.labels.length; ++index) {
          newDataset.data.push(randomScalingFactor());

          var colorName = colorNames[index % colorNames.length];
          var newColor = window.chartColors[colorName];
          newDataset.backgroundColor.push(newColor);
        }

        config.data.datasets.push(newDataset);
        window.myPie.update();
      });

      document.getElementById('removeDataset').addEventListener('click', function() {
        config.data.datasets.splice(0, 1);
        window.myPie.update();
      });
    </script>
  </body>
</html>