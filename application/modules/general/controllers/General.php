<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends MX_Controller {
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('view_landingpage');
	}

	function dashboard()
	{
		$this->load->view('view_dashboardpage');
	}

	function faq()
	{
		$this->load->view('view_faqpage');
	}

	function privacy()
	{
		$this->load->view('view_privacypage');
	}
}